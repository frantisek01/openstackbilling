import datetime
import pytz
from django.core.serializers.json import DjangoJSONEncoder
from django.contrib import admin
from django.urls import reverse
from django.urls import path
from django.utils.safestring import mark_safe
from django.template.response import TemplateResponse
from .models import (BillCodeProjectRelation, BillCode, Project, Server, ServerEvent, Flavor,HypervisorEvent,
                     Volume, VolumeEvent, VolumeType, Image, ImageEvent, Backup, BackupEvent, MonthlyReport, BillCodeProjectRelation, Hypervisor,
                     Aggregate, Report)
from .bill_sum import bill_sum
from .excelwriter import excelwriter
from django.http import HttpResponse
import os
from tempfile import NamedTemporaryFile

admin.site.site_header = 'Openstack Billing Admin'
admin.site.site_title = 'Openstack Billing Admin'

@admin.register(MonthlyReport)
class MonthlyReportAdmin(admin.ModelAdmin):
    list_display = ("id", "created", "start_date", "end_date")
    fields = ("id", "created", "start_date", "end_date", "data")

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "terp", "enabled",
                    "description", "openstack_id", "created")
    list_filter = ("enabled",)
    search_fields = ("id", "name", "enabled", "description",
                     "openstack_id", "created")
    fields = ("id", "name", "terp", "enabled",
              "description", "openstack_id", "created")

    def terp(self, obj):
        try:
            terp_id = BillCodeProjectRelation.objects.get(project=obj)
        except BillCodeProjectRelation.MultipleObjectsReturned:
            terp_id = BillCodeProjectRelation.objects.filter(
                project=obj).latest("activated")
        terp = BillCode.objects.get(id=terp_id.billcode_id)
        return mark_safe(terp.code)

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(BillCode)
class BillCodeAdmin(admin.ModelAdmin):
    list_display = ("id", "code", "active", "projects")
    list_filter = ("active",)
    search_fields = ("id", "code", "active")
    fields = ("id", "code", "active")

    def projects(self, obj):
        rel = BillCodeProjectRelation.objects.filter(
            billcode=obj).values("project")
        pr = Project.objects.filter(id__in=rel).filter(enabled=True)
        display_text = ", ".join([
            "<a href={}>{}</a>".format(
                reverse('admin:{}_{}_change'.format(obj._meta.app_label, "project"),
                        args=(child.pk,)),
                child.name)
            for child in pr
        ])
        if display_text:
            return mark_safe(display_text)
        return "-"

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(BillCodeProjectRelation)
class BillCodeProjectRelationAdmin(admin.ModelAdmin):
    list_display = ("id", "project", "billcode",
                    "activated", "deactivated")
    list_filter = ("activated", "deactivated")

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Hypervisor)
class HypervisorAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "vcpu", "ram_gb", "disk_gb", "openstack_id")    
    fields = ("id", "name", "vcpu", "ram_gb", "disk_gb", "openstack_id")
    search_fields = ("id", "name", "vcpu", "ram_gb", "disk_gb", "openstack_id")

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

@admin.register(HypervisorEvent)
class HypervisorEventAdmin(admin.ModelAdmin):
    list_display = ("id", "hypervisor", "hypervisor_id", "name","os_state",
                    "created",  "bill_state", "vcpu", "ram_gb", "disk_gb")
    list_filter = ("name", "os_state","bill_state")
    fields = ("id", "hypervisor", "hypervisor_id", "name","os_state",
              "created", "bill_state", "vcpu", "ram_gb", "disk_gb" )
    search_fields = ("id", "name", "created", "os_state",
                     "bill_state", "vcpu", "ram_gb", "disk_gb")

    def hypervisor(self, obj):
        return mark_safe("<a href={}>{}</a>".format(reverse('admin:{}_{}_change'.format(obj._meta.app_label, "hypervisor"), args=(obj.hypervisor,)), obj.hypervisor))

    def hypervisor_id(self, obj):
        return obj.hypervisor

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

@admin.register(Aggregate)
class AggregateAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "created", "deleted",
                    "aggregate_instance_extra_specs", "hypervisor_name", "openstack_id")
    fields = ("id", "name", "created", "deleted",
              "aggregate_instance_extra_specs", "hypervisor_name", "openstack_id")
    search_fields = ("id", "name", "created", "deleted",
                     "aggregate_instance_extra_specs", "hypervisor_name", "openstack_id")

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Server)
class ServerAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "status", "project",
                    "openstack_id", "server_events")
    list_filter = ("project",)
    fields = ("id", "name", "status", "project", "openstack_id", "server_events")
    search_fields = ("id", "name", "status", "openstack_id")

    def server_events(self, obj):
        display_text = ", ".join([
            "<a href={}>{}</a>".format(
                reverse('admin:{}_{}_change'.format(obj._meta.app_label, "serverevent"),
                        args=(child.pk,)),
                child.name)
            for child in obj.server_events.all()
        ])
        if display_text:
            return mark_safe(display_text)
        return "-"

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(ServerEvent)
class ServerEventAdmin(admin.ModelAdmin):
    list_display = ("id", "server", "servers_id", "flavor", "name","os_state",
                    "created",  "bill_state", "vcpu", "ram_gb", "disk_gb")
    list_filter = ("name", "os_state","bill_state")
    fields = ("id", "server", "servers_id", "flavor", "name","os_state",
              "created", "bill_state", "vcpu", "ram_gb", "disk_gb" )
    search_fields = ("id", "name", "created", "os_state",
                     "bill_state", "vcpu", "ram_gb", "disk_gb")

    def server(self, obj):
        return mark_safe("<a href={}>{}</a>".format(reverse('admin:{}_{}_change'.format(obj._meta.app_label, "server"), args=(obj.server_id,)), obj.server))

    def servers_id(self, obj):
        return obj.server_id

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Flavor)
class FlavorAdmin(admin.ModelAdmin):
    list_display = ("id", "vcpu", "ram_gb", "name", 'disk_gb', "openstack_id", 
                    "aggregate", "aggregate_instance_extra_specs", "servers")
    fields = ("id", "vcpu", "ram_gb", "name", 'disk_gb', "openstack_id", )
    search_fields = ("id", "vcpu", "ram_gb", "name", 'disk_gb', "openstack_id")

    def aggregate(self, obj):
        out = ["NONE"]
        for a in obj.aggregate_instance_extra_specs:
            if "filter_project" in a:
                out = []
                out.append(a)
        aggr = Aggregate.objects.filter(
            aggregate_instance_extra_specs__contains=out)
        display_text = ", ".join([
            "<a href={}>{}</a>".format(
                reverse('admin:{}_{}_change'.format(obj._meta.app_label, "aggregate"),
                        args=(child.id,)),
                child.name)
            for child in aggr
        ])
        if display_text:
            return mark_safe(display_text)
        return "-"

    def servers(self, obj):
        morte = ServerEvent.objects.filter(bill_state=False).distinct(
            'server').values('server_id')
        events = ServerEvent.objects.all().exclude(server__in=morte).filter(
            flavor=obj).order_by('server', 'created').distinct('server')
        display_text = ", ".join([
            "<a href={}>{}</a>".format(
                reverse('admin:{}_{}_change'.format(obj._meta.app_label, "server"),
                        args=(child.server_id,)),
                child.server)
            for child in events
        ])
        if display_text:
            return mark_safe(display_text)
        return "-"

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Volume)
class VolumeAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "project", "openstack_id", "status",
                    "attached_to", "image_os_distro", "volume_events")
    list_filter = ("project",)
    fields = ("id", "name", "project", "openstack_id", "status",
              "volume_events", "image_os_distro", "attached_to")
    search_fields = ("id", "name", "image_os_distro", "status", "openstack_id")

    def volume_events(self, obj):
        display_text = ", ".join([
            "<a href={}>{}</a>".format(
                reverse('admin:{}_{}_change'.format(obj._meta.app_label, "volumeevent"),
                        args=(child.pk,)),
                child.name)
            for child in obj.volume_events.all()
        ])
        if display_text:
            return mark_safe(display_text)
        return "-"

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(VolumeEvent)
class VolumeEventAdmin(admin.ModelAdmin):
    list_display = ("id", "volume", "volumes_id", "name", "created", "os_state",
                    "bill_state", "size_gb", "storages_type", "storages_type_id")
    list_filter = ("name", "os_state", "bill_state", "volume_type", )
    fields = ("id", "volume", "volumes_id", "name", "created", "os_state",
              "bill_state", "size_gb", "storages_type", "storages_type_id")
    search_fields = ("id", "name", "created", "os_state",
                     "bill_state", "size_gb")

    def volume(self, obj):
        return mark_safe("<a href={}>{}</a>".format(reverse('admin:{}_{}_change'.format(obj._meta.app_label, "volume"), args=(obj.volume_id,)), obj.volume))

    def storages_type(self, obj):
        return mark_safe("<a href={}>{}</a>".format(reverse('admin:{}_{}_change'.format(obj._meta.app_label, "volumetype"), args=(obj.volume_type_id,)), obj.volume_type))

    def storages_type_id(self, obj):
        return obj.volume_type_id

    def volumes_id(self, obj):
        return obj.volume_id

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(VolumeType)
class VolumeType(admin.ModelAdmin):
    list_display = ("id", "name", "is_public", "volumes")
    list_filter = ("is_public",)
    fields = ("id", "name", "is_public", "volumes")
    search_fields = ("id", "name", "is_public")

    def volumes(self, obj):
        morte = VolumeEvent.objects.filter(bill_state=False).distinct(
            'volume').values('volume_id')
        events = VolumeEvent.objects.all().exclude(volume__in=morte).filter(
            volume_type=obj).order_by('volume', 'created').distinct('volume')
        display_text = ", ".join([
            "<a href={}>{}</a>".format(
                reverse('admin:{}_{}_change'.format(obj._meta.app_label, "volume"),
                        args=(child.volume_id,)),
                child.volume)
            for child in events
        ])
        if display_text:
            return mark_safe(display_text)
        return "-"

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(Image)
class ImageAdmin(admin.ModelAdmin):
    list_display = ("id", "name","status", "project", "openstack_id", "image_events")
    list_filter = ("project",)
    fields = ("id", "name", "status", "project", "openstack_id", "image_events")
    search_fields = ("id", "name", "status", "openstack_id")

    def image_events(self, obj):
        display_text = ", ".join([
            "<a href={}>{}</a>".format(
                reverse('admin:{}_{}_change'.format(obj._meta.app_label, "imageevent"),
                        args=(child.pk,)),
                child.name)
            for child in obj.image_events.all()
        ])
        if display_text:
            return mark_safe(display_text)
        return "-"

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(ImageEvent)
class ImageEventAdmin(admin.ModelAdmin):
    list_display = ("id", "image", "images_id", "name", "created",
                    "os_state", "bill_state", "size_gb")
    list_filter = ("name", "os_state", "bill_state")
    fields = ("id", "image", "images_id", "name", "created",
              "os_state", "bill_state", "size_gb")
    search_fields = ("id", "name", "created", "os_state",
                     "bill_state", "size_gb")

    def image(self, obj):
        return mark_safe("<a href={}>{}</a>".format(reverse('admin:{}_{}_change'.format(obj._meta.app_label, "image"), args=(obj.image_id,)), obj.image))

    def images_id(self, obj):
        return obj.image_id

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

@admin.register(Backup)
class BackupAdmin(admin.ModelAdmin):
    list_display = ("id", "name","status", "project", "openstack_id", "backup_events")
    list_filter = ("project",)
    fields = ("id", "name", "status", "project", "openstack_id", "backup_events")
    search_fields = ("id", "name", "status", "openstack_id")

    def backup_events(self, obj):
        display_text = ", ".join([
            "<a href={}>{}</a>".format(
                reverse('admin:{}_{}_change'.format(obj._meta.app_label, "backupevent"),
                        args=(child.pk,)),
                child.name)
            for child in obj.backup_events.all()
        ])
        if display_text:
            return mark_safe(display_text)
        return "-"

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


@admin.register(BackupEvent)
class BackupEventAdmin(admin.ModelAdmin):
    list_display = ("id","back", "backups_id", "name", "created",
                    "os_state", "bill_state", "size_gb")
    list_filter = ("name", "os_state", "bill_state")
    fields = ("id","back", "backups_id", "name", "created",
              "os_state", "bill_state", "size_gb")
    search_fields = ("id", "name", "created", "os_state",
                     "bill_state", "size_gb")

    def back(self, obj):
        return mark_safe("<a href={}>{}</a>".format(reverse('admin:{}_{}_change'.format(obj._meta.app_label, "backup"), args=(obj.backup_id,)), obj.backup))

    def backups_id(self, obj):
        return obj.backup_id

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

@admin.register(Report)
class ReportAdmin(admin.ModelAdmin):
    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('', self.admin_site.admin_view(self.my_view))
        ]
        return my_urls + urls

    def my_view(self, request):
        if request.method == 'GET':
            now=datetime.datetime.now()
            start_time = now.replace(day=1, hour=0, minute=0, second=0, microsecond=0)
            finish_time=start_time.replace(day=15) + datetime.timedelta(days=30)
            finish_time=finish_time.replace(day=1, tzinfo=pytz.utc)
            context = dict(
                # Include common variables for rendering the admin template.
                self.admin_site.each_context(request),
                start_time=start_time.strftime("%d-%m-%Y"),
                end_time=finish_time.strftime("%d-%m-%Y"),
            )
            return TemplateResponse(request, "admin/excel.html", context)
        elif request.method == 'POST':
            start_time = datetime.datetime.strptime(request.POST.get('startDate')+'T00:00:00.0 +0000', '%d-%m-%YT%H:%M:%S.%f %z')
            finish_time = datetime.datetime.strptime(request.POST.get('endDate')+'T00:00:00.0 +0000', '%d-%m-%YT%H:%M:%S.%f %z')
            filename = "OpenStackBilling-" + os.environ.get('OPENSTACK_SITE') + "(" + start_time.strftime('%d-%m-%Y')  + " - " + finish_time.strftime('%d-%m-%Y') + ").xlsx"
            bill = bill_sum(start_time, finish_time)
            excel = excelwriter(bill)
            with NamedTemporaryFile() as tmp:
                excel.save(tmp.name)
                tmp.seek(0)
                stream = tmp.read()

                response = HttpResponse(stream, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',)
                response['Content-Disposition'] = 'attachment; filename='+filename

                return response

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
