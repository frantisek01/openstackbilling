from osbill.loader import Loader
from django.contrib.auth.models import User
import datetime
import pytz
from osbill.models import Project

class BillBackend:
    def authenticate(self, request, username=None, password=None):
        print("inside BillBackend")
        user = User.objects.get(username=username)
        if user.is_staff:
            try:
                projects = Project.objects.all()#filter(enabled=True)
                serialized_projects=[]
                for project in projects:
                    row={'id': project.openstack_id, 'name': project.name, 'description': project.description, 'enabled': project.enabled}
                    serialized_projects.append(row)
                request.session['projects'] = serialized_projects
                request.session.set_expiry(3000)

                today = datetime.datetime.now().replace(minute=0, hour=0, second=0, microsecond=0)
                default_end_time = today.replace(day=1)
                previous_month = default_end_time - datetime.timedelta(days=1)
                default_start_time = previous_month.replace(day=1)
                str_start_time = datetime.datetime.timestamp(default_start_time)
                str_end_time = datetime.datetime.timestamp(default_end_time)
                request.session['default_start_time'] = str_start_time
                request.session['default_end_time'] = str_end_time

                return user
            except Exception as e:
                print(e)
        try:
            os_username = username
            os_password = password
            loader = Loader(os_username, os_password)
            projects = loader.load_projects()
            if 'id' in projects[0] and 'name' in projects[0] and 'description' in projects[0] and 'enabled' in projects[0]:
                serialized_projects=[]
                for project in projects:
                    row={'id': project['id'], 'name': project['name'], 'description': project['description'], 'enabled' :project['enabled']}
                    serialized_projects.append(row)

                request.session['projects'] = serialized_projects
                request.session.set_expiry(3000)

                today = datetime.datetime.now().replace(minute=0, hour=0, second=0, microsecond=0)
                default_end_time = today.replace(day=1)
                previous_month = default_end_time - datetime.timedelta(days=1)
                default_start_time = previous_month.replace(day=1)
                str_start_time = datetime.datetime.timestamp(default_start_time)
                str_end_time = datetime.datetime.timestamp(default_end_time)
                request.session['default_start_time'] = str_start_time
                request.session['default_end_time'] = str_end_time                
                try:
                    user = User.objects.get(username=username)
                    return user
                except User.DoesNotExist:
                    user = User(username=username, password=password)
                    user.save()
                    return user
            else:
                return None

        except Exception as e:
            print(e)

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
           return None
