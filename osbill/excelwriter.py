from openpyxl import Workbook
from openpyxl.styles import Color, PatternFill, Font, Border, Side

def excelwriter(billing_sum):
    wb = Workbook()
    wb.remove(wb.active)
    bill_code_ws = wb.create_sheet(title="bill_codes", index=0)
    project_ws = wb.create_sheet(title="projects", index=1)
    server_ws = wb.create_sheet(title="servers", index=2)
    server_events_ws = wb.create_sheet(title="server_events", index=3)
    volume_ws = wb.create_sheet(title="volumes", index=4)
    volume_events_ws = wb.create_sheet(title="volume_events", index=5)
    image_ws = wb.create_sheet(title="images", index=6)
    image_events_ws = wb.create_sheet(title="image_events", index=7)
    backup_ws = wb.create_sheet(title="backups", index=8)
    backup_events_ws = wb.create_sheet(title="backup_events", index=9)

    server_ws.append(['Server ID', 'Server Name', 'Project ID', 'Compute Type','License', 'Run time', 'Flavor', 'VCPU time', 'MEM time', 'Disk time', 'Size Time','TERP'])
    server_events_ws.append(['Server ID', 'Server Name', 'Project ID', 'Project Name', 'Project Description', 'TERP', 'Event Name',
                                  'Event time', 'Flavour', 'VCPUs', 'MEM', 'DISK'])
    volume_ws.append(['Volume ID', 'Volume Name','Project ID', 'Run time', 'Disk time GB/H', 'Storage type', 'Volume Size', 'TERP'])
    volume_events_ws.append(['Volume ID', 'Volume Name', 'Project ID', 'Project Name', 'Project Description', 'TERP', 'Event Name',
                                  'Event time', 'Volume Size on event', 'Storage type'])
    image_ws.append(['Image ID', 'Image Name', 'Project ID', 'Project Name', 'Run time', 'Size', 'Image time GB/H', 'TERP'])
    image_events_ws.append(['Image ID', 'Project ID', 'Project Name', 'Project Description', 'TERP',
                                 'Event Name','Event time', 'Volume Size', 'Run time', 'Image time GB/H'])
    backup_ws.append(['Backup ID', 'Backup Name', 'Project ID', 'Project Name', 'Run time', 'Size', 'Backup time GB/H', 'TERP'])
    backup_events_ws.append(['Backup ID', 'Project ID', 'Project Name', 'Project Description', 'TERP',
                                 'Event Name','Event time', 'Volume Size', 'Run time', 'Backup time GB/H'])
    sum_header = []
    license_types = []
    bill_summ = billing_sum
    for bill_code, bill_code_data in bill_summ.items():
        for item, value in bill_code_data['sum'].items():
            if not item in sum_header:
                sum_header.append(item)
            if item == 'licenses':
                for l in value:
                    if not l in license_types:
                        license_types.append(l)
    sum_header.sort()
    head = sum_header
    license_space = []
    for _ in license_types:
        license_space.append("")
    license_space.pop()
    for index,sh in enumerate(head):
        if "_compute_type" in sh:
            sum_header[index+1:index+1] = ["","",""]
        if "licenses" in sh:
            sum_header[index+1:index+1] = license_space
    
    bill_code_header = ['Billing Code']
    project_header = ['Project OpenStack ID', 'Project Name','Project Description']
    bill_code_header.extend(sum_header)
    project_header.extend(sum_header)
    bill_code_header2 = []   
    project_header2=[] 
    for h in bill_code_header:
        if "_compute_type" in h:
            bill_code_header2.extend(["vcpu_h","mem_h_gb","disk_h_gb","run_time"])
        elif "licenses" == h:
            bill_code_header2.extend(license_types)
        elif not h == "":
            bill_code_header2.append("")

    for h in project_header:
        if "_compute_type" in h:
            project_header2.extend(["vcpu_h","mem_h_gb","disk_h_gb","run_time"])
        elif "licenses" == h:
            project_header2.extend(license_types)
        elif not h == "":
            project_header2.append("")

    bill_code_ws.append(bill_code_header)
    bill_code_ws.append(bill_code_header2)

    project_ws.append(project_header)
    project_ws.append(project_header2)

    

    for bill_code, bill_code_data in bill_summ.items():
        bill_code_row = [bill_code]
        for _ in sum_header:
            bill_code_row.append(0.0)        
        indexer = 0    
        for index,header in enumerate(sum_header):             
            present = False
            for item, value in bill_code_data['sum'].items():                
                if item == header:   
                    present = True                 
                    if "_compute_type" in item:
                        bill_code_row[indexer+1] = round(value["vcpu_h"],2)
                        bill_code_row[indexer+2] = round(value["mem_h_gb"],2)
                        bill_code_row[indexer+3] = round(value["disk_h_gb"],2)
                        bill_code_row[indexer+4] = round(value["run_time"],2)
                        indexer+=4
                    elif "licenses" in item:
                        for i, l in enumerate(license_types):   
                            if l in value:                         
                                bill_code_row[indexer+i+1] = value[l]
                        indexer+=len(license_types)
                    elif item in sum_header:# and value:
                        bill_code_row[indexer+1] = round(value,2)
                        indexer+=1
            if not present and header:
                if "_compute_type" in header:
                    indexer+=4
                elif "licenses" in header:
                    indexer += len(license_types)
                else:
                    indexer+=1         
        bill_code_ws.append(bill_code_row)

        for project, project_data in bill_code_data['projects'].items():
            project_row = [project, project_data['project_name'],project_data['project_description']]
            for _ in sum_header:
                project_row.append(0.0)
            indexer = 2
            for index,header in enumerate(sum_header):
                present = False
                for item, value in project_data['sum'].items():                
                    if item == header:   
                        present = True
                        if "_compute_type" in item:
                            project_row[indexer+1] = round(value["vcpu_h"],2)
                            project_row[indexer+2] = round(value["mem_h_gb"],2)
                            project_row[indexer+3] = round(value["disk_h_gb"],2)
                            project_row[indexer+4] = round(value["run_time"],2)
                            indexer+=4
                        elif "licenses" in item:
                            for i, l in enumerate(license_types): 
                                if l in value:                           
                                    project_row[indexer+i+1] = value[l]
                            indexer+=len(license_types)
                        elif item in sum_header:# and value:
                            project_row[indexer+1] = round(value,2)
                            indexer+=1
                if not present and header:
                    if "_compute_type" in header:
                        indexer+=4
                    elif "licenses" in header:
                        indexer += len(license_types)
                    else:
                        indexer+=1
            project_ws.append(project_row)
            for server in project_data['instances']["server"]:
                server_row = []
                server_row.append(server["openstack_id"])
                server_row.append(server["instance_name"])
                server_row.append(server["project_openstack_id"])
                server_row.append(str(server["consumption"]["compute_type"]))
                if "license" in server:
                    server_row.append(server["license"])
                else:
                    server_row.append("")
                server_row.append(str(server["consumption"]["size_time"]))
                if 'flavor' in server["consumption"]:
                    server_row.append(str(server["consumption"]["flavor"]))
                else:
                    server_row.append('dedicated host')
                server_row.append(server["consumption"]["vcpu_h"])
                server_row.append(server["consumption"]["mem_h_gb"])
                server_row.append(server["consumption"]["disk_h_gb"])
                server_row.append(server["consumption"]["run_time"])
                server_row.append(server["billing_code"])
                server_ws.append(server_row)
                for event in server["events"]:
                    server_event_row = []
                    # server_event_row.append(event["event_openstack_id"])
                    server_event_row.append(server["openstack_id"])
                    server_event_row.append(server["instance_name"])
                    server_event_row.append(server["project_openstack_id"])
                    server_event_row.append(server["project_name"])
                    server_event_row.append(project_data['project_description'])
                    server_event_row.append(server["billing_code"])
                    server_event_row.append(event["name"])
                    server_event_row.append(event["created"])
                    if "flavor_id" in event:
                        server_event_row.append(event["flavor_id"])
                    else:
                        server_event_row.append("dedicated host")
                    server_event_row.append(event["vcpu"])
                    server_event_row.append(event["ram_gb"])
                    server_event_row.append(event["disk_gb"])
                    server_events_ws.append(server_event_row)
            for volume in project_data['instances']['volume']:
                volume_row = []
                volume_row.append(volume["openstack_id"])
                volume_row.append(volume["instance_name"])
                volume_row.append(volume["project_openstack_id"])
                volume_row.append(str(volume['consumption']['size_time']))
                volume_row.append(str(volume['consumption']['total_h_gb']))
                volume_row.append(str(volume['consumption']['volume_type_name']))
                volume_row.append(str(volume['consumption']['disk']))
                volume_row.append(volume["billing_code"])
                volume_ws.append(volume_row)
                for vevent in volume["events"]:
                    volume_event_row = []
                    volume_event_row.append(volume["openstack_id"])
                    volume_event_row.append(volume["instance_name"])
                    volume_event_row.append(volume["project_openstack_id"])
                    volume_event_row.append(volume["project_name"])
                    volume_event_row.append(project_data["project_description"])
                    volume_event_row.append(volume["billing_code"])
                    volume_event_row.append(vevent["name"])
                    volume_event_row.append(vevent["created"])
                    volume_event_row.append(vevent["size_gb"])
                    volume_event_row.append(vevent["volume_type_id"])
                    volume_events_ws.append(volume_event_row)
            for image in project_data['instances']['image']:
                image_row = []
                image_row.append(image["openstack_id"])
                image_row.append(image["instance_name"])
                image_row.append(image["project_openstack_id"])
                image_row.append(image["project_name"])
                image_row.append(image["run_time"])
                image_row.append(image["size"])
                image_row.append(image["consumption"])
                image_row.append(image["billing_code"])                
                image_ws.append(image_row)
                for ievent in image["events"]:
                    image_event_row = []
                    image_event_row.append(image["openstack_id"])
                    image_event_row.append(image["project_openstack_id"])
                    image_event_row.append(image["project_name"])
                    image_event_row.append(image["project_description"])
                    image_event_row.append(image["billing_code"])
                    image_event_row.append(ievent["name"])
                    image_event_row.append(ievent["created"])
                    image_event_row.append(ievent["size_gb"])
                    image_event_row.append(image["run_time"])
                    image_event_row.append(image["consumption"])
                    image_events_ws.append(image_event_row)
            for backup in project_data['instances']['backup']:
                backup_row = []
                backup_row.append(backup["openstack_id"])
                backup_row.append(backup["instance_name"])
                backup_row.append(backup["project_openstack_id"])
                backup_row.append(backup["project_name"])
                backup_row.append(backup["run_time"])
                backup_row.append(backup["size"])
                backup_row.append(backup["consumption"])
                backup_row.append(backup["billing_code"])                
                backup_ws.append(backup_row)
                for ievent in backup["events"]:
                    backup_event_row = []
                    backup_event_row.append(backup["openstack_id"])
                    backup_event_row.append(backup["project_openstack_id"])
                    backup_event_row.append(backup["project_name"])
                    backup_event_row.append(backup["project_description"])
                    backup_event_row.append(backup["billing_code"])
                    backup_event_row.append(ievent["name"])
                    backup_event_row.append(ievent["created"])
                    backup_event_row.append(ievent["size_gb"])
                    backup_event_row.append(backup["run_time"])
                    backup_event_row.append(backup["consumption"])
                    backup_events_ws.append(backup_event_row)        
    header_style =  Font(bold=True)   
    header_fill =  PatternFill(start_color='c0d5eb',
                   fill_type='solid')
    header_border = Border(left=Side(style='thin'), 
                     right=Side(style='thin'), 
                     top=Side(style='thin'), 
                     bottom=Side(style='thin'))
    rowback1=  PatternFill(start_color='e9f0f7',
                   fill_type='solid')
    rowback2 =  PatternFill(start_color='e3e4e6',
                   fill_type='solid')
    row_styles = [rowback1, rowback2] 
    for ws in [bill_code_ws, project_ws, server_ws, server_events_ws, volume_ws, volume_events_ws, image_ws, image_events_ws, backup_ws, backup_events_ws]:
        start_row=0
        for column_cells in ws.columns:
            length = max(len(str(cell.value)) for cell in column_cells)
            if length > 50:
                length = 50
            elif length < 17:
                length = 17
            ws.column_dimensions[column_cells[0].column_letter].width = length
        for cl in ws['1:1']:
                cl.font = header_style
                cl.fill = header_fill
                cl.border = header_border
        if ws in [bill_code_ws, project_ws]:
            start_row = 3
            ws.freeze_panes='A3'            
            for cl in ws['2:2']:
                cl.font = header_style
                cl.fill = header_fill
                cl.border = header_border
        else:
            ws.freeze_panes='A2'
            start_row = 2
        index = 0
        for row in ws.iter_rows(min_row=start_row,max_row=ws.max_row, min_col=1, max_col=ws.max_column):
   
            for cell in row:
                cell.fill = row_styles[index]
            if index == 0:
                index=1
            else:
                index = 0

          

    for col in bill_code_ws.iter_cols(min_row=1,max_row=1, min_col=1, max_col=bill_code_ws.max_column):
        if col[0].value and "_compute_type" in col[0].value:
            bill_code_ws.merge_cells(start_row=1, start_column=col[0].column, end_row=1, end_column=col[0].column + 3)
        elif col[0].value and "licenses" in col[0].value:
            bill_code_ws.merge_cells(start_row=1, start_column=col[0].column, end_row=1, end_column=col[0].column + (len(license_types)-1))
        elif col[0].value:
            bill_code_ws.merge_cells(start_row=1, start_column=col[0].column, end_row=2, end_column=col[0].column)

    for col in project_ws.iter_cols(min_row=1,max_row=1, min_col=1, max_col=project_ws.max_column):
        if col[0].value and "_compute_type" in col[0].value:
            project_ws.merge_cells(start_row=1, start_column=col[0].column, end_row=1, end_column=col[0].column + 3)
        elif col[0].value and "licenses" in col[0].value:
            project_ws.merge_cells(start_row=1, start_column=col[0].column, end_row=1, end_column=col[0].column + (len(license_types)-1))
        elif col[0].value:
            project_ws.merge_cells(start_row=1, start_column=col[0].column, end_row=2, end_column=col[0].column)
    return wb