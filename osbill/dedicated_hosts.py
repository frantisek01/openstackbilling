from osbill.models import Hypervisor, Aggregate, Flavor
import os


def dedicated_hosts():
    dedicated_disk_filter = str(os.environ.get('DEDICATED_DISK_FILTER', 'disk=dedicated'))
    dedicated_flavor_filter = str(os.environ.get('DEDICATED_FLAVOR_FILTER', 'filter_project'))
    dedicated_tenant_filter = str(os.environ.get('DEDICATED_TENANT_FILTER', 'filter_tenant_id'))
    aggregate=Aggregate.objects.filter(deleted__isnull=True)
    hypervisors = Hypervisor.objects.all()    
    dedicated_aggregates=[]    
    project=""
    for aggr in aggregate:
        dedicated_hosts=[]
        flavor_filter=["NONE"]
        dedicated_flavors=[]
        project=""
        out={}
        for a in aggr.aggregate_instance_extra_specs:
            if dedicated_disk_filter in a:
                dedicated_disk = True
            else:
                dedicated_disk = False
            if dedicated_flavor_filter in a:
                flavor_filter=[]
                flavor_filter.append(a)
            if dedicated_tenant_filter in a:
                project=a.split("=")[1]
                if "," in project:
                    pi =  project.split(",")[0]
                else:
                    pi =  project               
            if not flavor_filter[0] =="NONE" and not project == "":                
                dedicated_flavors = Flavor.objects.filter(aggregate_instance_extra_specs__contains=flavor_filter).values_list('name', flat=True)         
        for h in aggr.hypervisor_name:
            for hyp in hypervisors:
                if h in hyp.name:
                    dedicated_hosts.append(hyp.id)
        if dedicated_hosts and dedicated_flavors:
            out = {"project_openstack_id": pi, "dedicated_flavors":dedicated_flavors, "dedicated_hosts":dedicated_hosts, "dedicated_disk":dedicated_disk}
            dedicated_aggregates.append(out)          
    return dedicated_aggregates