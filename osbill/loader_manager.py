from requests.sessions import session
from osbill.loader import Loader
from osbill.dedicated_hosts import dedicated_hosts
from osbill.models import (Server, ServerEvent, Volume, VolumeEvent,VolumeType,Image,ImageEvent, Backup, BackupEvent, Project, Flavor, BillCode,
 BillCodeProjectRelation, Aggregate, Hypervisor, HypervisorEvent)
from osbill.serializers import (ServerSerializer,ServerEventSerializer, ProjectSerializer, ImageSerializer, ImageEventSerializer, BackupSerializer, BackupEventSerializer,FlavorSerializer,
VolumeSerializer,VolumeTypeSerializer, VolumeEventSerializer, BillCodeSerializer, BillCodeProjectRelationSerializer,
AggregateSerializer,HypervisorSerializer, HypervisorEventSerializer)
import datetime
import os
import pytz
import logging
import requests

logger = logging.getLogger(__name__)

class Loader_manager:

    def __init__(self):
        try:
            self.os_username = os.environ['OS_USERNAME']
            self.os_password = os.environ['OS_PASSWORD']
            self.loader = Loader(self.os_username, self.os_password)
            self.projects = self.loader.load_projects()
            
        except Exception as e:
            print("Missing environment variable " + str(e) + " ! EXITING.")
            exit(1)

    
    def upload_projects(self):
        for project in self.projects:
            sserializer = ProjectSerializer(data=project)
            sserializer.create(
               validated_data=project 
            )

    def upload_volume_types(self, projects):
        volumetypes = self.loader.load_volume_types(projects)
        for volumetype in volumetypes:
            vtserializer = VolumeTypeSerializer(data=volumetype)
            if vtserializer.is_valid():
                vtserializer.save()

    def upload_flavors(self):
        flavors = self.loader.load_flavors()
        for flavor in flavors:
            fserializer = FlavorSerializer(data=flavor)
            if fserializer.is_valid():
                fserializer.save()

    def upload_hypervisors(self):
        hypervisors = self.loader.load_hypervisors()
        for hypervisor in hypervisors:
            hserializer = HypervisorSerializer(data=hypervisor)
            hserializer.create(data=hypervisor)

    def update_dedicated_hypervisors(self):
        dedicated_hypervisors = dedicated_hosts()
        exclude_hp_ids = HypervisorEvent.objects.exclude(os_state='deleted').distinct('hypervisor').values('hypervisor')
        active_hp_ids =  HypervisorEvent.objects.exclude(hypervisor__in=exclude_hp_ids).distinct('hypervisor').values('hypervisor')
        dbhosts=list(active_hp_ids)
        ahypervisors = []
        for dh in dedicated_hypervisors:
            for d in dh['dedicated_hosts']:
                ahypervisors.append(d) 
                dt = {}
                dt['id']=d
                dt['status']='active'          
                hypervisoreventserializer = HypervisorEventSerializer(data=dt)
                if hypervisoreventserializer.is_valid():
                    hypervisoreventserializer.save()
        for dbh in dbhosts:
            if not dbh in ahypervisors:
                dt = {}
                dt['id']=dbh
                dt['status']='deleted'          
                hypervisoreventserializer = HypervisorEventSerializer(data=dt)
                if hypervisoreventserializer.is_valid():
                    hypervisoreventserializer.save()

    def upload_aggregates(self):
        aggregates = self.loader.load_aggregates()
        for aggregate in aggregates:            
            aserializer = AggregateSerializer(data=aggregate)
            aserializer.create(data=aggregate)
                
    def upload_images(self):
        images = self.loader.load_images()
        image_ids = []
        for image in images:
            image_ids.append(image['id'])
            imageserializer = ImageSerializer(data=image)
            imageserializer.create(data=image)

            imageeventserializer = ImageEventSerializer(data=image)
            if imageeventserializer.is_valid():
                imageeventserializer.save()
               

        image_exclude = ImageEvent.objects.all().filter(os_state='deleted').values('image_id')
        deletedimages = Image.objects.all().exclude(id__in=image_exclude).exclude(openstack_id__in=image_ids)
        for deletedimage in deletedimages:
            latestevent = ImageEvent.objects.filter(image=deletedimage.id).latest('created')
            dimage = {}
            dimage['id'] = deletedimage.openstack_id
            dimage['status'] = "deleted"
            dimage['size'] = latestevent.size_gb*1024*1024*1024
            dbimageeventserializer = ImageEventSerializer(data=dimage)
            if dbimageeventserializer.is_valid():
                dbimageeventserializer.save()

    def upload_backups(self, projects):
        backups = self.loader.load_backups(projects)
        backup_ids = []
        for backup in backups:
            backup_ids.append(backup['id'])
            backupserializer = BackupSerializer(data=backup)
            backupserializer.create(data=backup)

            backupeventserializer = BackupEventSerializer(data=backup)
            if backupeventserializer.is_valid():
                backupeventserializer.save()
               

        backup_exclude = BackupEvent.objects.all().filter(os_state='deleted').values('backup_id')
        deletedbackups = Backup.objects.all().exclude(id__in=backup_exclude).exclude(openstack_id__in=backup_ids)
        for deletedbackup in deletedbackups:
            latestevent = BackupEvent.objects.filter(backup=deletedbackup.id).latest('created')
            dbackup = {}
            dbackup['id'] = deletedbackup.openstack_id
            dbackup['status'] = "deleted"
            dbackup['size'] = latestevent.size_gb
            dbbackupeventserializer = BackupEventSerializer(data=dbackup)
            if dbbackupeventserializer.is_valid():
                dbbackupeventserializer.save()

    def upload_volumes(self, projects):
        volumes = self.loader.load_volumes(projects)
        volume_ids=[]
        for volume in volumes:
            volume_ids.append(volume['id'])
            volumeserializer = VolumeSerializer(data=volume)
            if "volume_image_metadata" in volume and "os_distro" in volume["volume_image_metadata"]:
                volume["image_os_distro"] = volume["volume_image_metadata"]["os_distro"]
            else:
                volume["image_os_distro"] = ""
            if volume["attachments"] and Server.objects.filter(openstack_id = volume["attachments"][0]["server_id"]).exists():
                volume["attached_to"] = Server.objects.get(openstack_id = volume["attachments"][0]["server_id"])
            else:
                volume["attached_to"] = None
            volumeserializer.create(data=volume)
            volumeeventserializer = VolumeEventSerializer(data=volume)
            if volumeeventserializer.is_valid():
                volumeeventserializer.save()

        excludes = VolumeEvent.objects.all().filter(os_state='deleted').values('volume_id')
        deletedvolumes=Volume.objects.all().exclude(id__in=excludes).exclude(openstack_id__in=volume_ids)
        for deletedvolume in deletedvolumes:
            latestevent = VolumeEvent.objects.filter(volume=deletedvolume.id).prefetch_related('volume_type').latest('created')
            dvolume={}
            dvolume['id'] = deletedvolume.openstack_id
            dvolume['volume_type'] = latestevent.volume_type.name
            dvolume['status'] = 'deleted'
            dvolume['size'] = latestevent.size_gb
            volumeeventserializer = VolumeEventSerializer(data=dvolume)
            if volumeeventserializer.is_valid():
                volumeeventserializer.save()
            else:
                print(volumeeventserializer.errors)

    def upload_servers(self):
        servers = self.loader.load_servers()
        server_ids=[]
        for server in servers:
            server_ids.append(server['id'])
            serverserializer = ServerSerializer(data=server)            
            serverserializer.create(validated_data=server)

            servereventserializer = ServerEventSerializer(data=server)
            if servereventserializer.is_valid():
                servereventserializer.save()

        excludes = ServerEvent.objects.all().filter(os_state='deleted').values('server_id')
        deletedservers=Server.objects.all().exclude(id__in=excludes).exclude(openstack_id__in=server_ids)
        for deletedserver in deletedservers:
            latestevent = ServerEvent.objects.filter(server=deletedserver.id).latest('created')
            dserver={}
            dserver['id'] = deletedserver.openstack_id
            dserver['flavor'] = {}
            dserver['flavor']['id'] = latestevent.flavor.openstack_id
            dserver['status'] = 'deleted'
            servereventserializer = ServerEventSerializer(data=dserver)
            if servereventserializer.is_valid():
                servereventserializer.save()

    def upload_billing_codes(self):
        project_all=Project.objects.all()
        billcodes=BillCode.objects.all()
        terps = list(set([pr.description.split("- terp ")[1] if "- terp " in pr.description else "NoTerp" for pr in project_all]))

        for terp in terps:
            already_there = False
            for bc in billcodes:                
                if terp == bc.code:
                    already_there = True
            if not already_there:
                bcs = BillCodeSerializer(data={'code':terp})
                if bcs.is_valid():
                     bcs.save()

        for pr in project_all:
            pr_bilcode = pr.description.split("- terp ")[1] if "- terp " in pr.description else "NoTerp"
            relation_exists = BillCodeProjectRelation.objects.filter(project=pr).exists()
            if relation_exists:
                latest_relation = BillCodeProjectRelation.objects.filter(project=pr).latest('activated')
                latest_code = BillCode.objects.get(id = latest_relation.billcode.id)
                if latest_code.code != pr_bilcode:
                    new_billcode = BillCode.objects.get(code=pr_bilcode)
                    latest_relation.deactivated = datetime.datetime.now(pytz.utc)
                    latest_relation.save()
            else:
                new_billcode = BillCode.objects.get(code=pr_bilcode)       
                bcprs = BillCodeProjectRelationSerializer(data={'billcode':new_billcode.id, 'project':pr.id})
                if bcprs.is_valid():
                    bcprs.save()                
                    
        for bc in billcodes:
            active_relation_exists = False
            for bcpr in BillCodeProjectRelation.objects.filter(deactivated__isnull=True):                
                if bcpr.billcode.id == bc.id:
                    active_relation_exists = True
            if bc.active != active_relation_exists:
                bc.active = active_relation_exists
                bc.save()

 
