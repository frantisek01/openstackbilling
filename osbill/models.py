from django.db import models
from django.db.models import JSONField
from django.contrib.postgres.fields import ArrayField
from django.contrib.postgres.fields import HStoreField

class Project(models.Model):
    openstack_id = models.CharField(max_length=100, unique=True)
    name = models.CharField(max_length=500)
    description = models.CharField(max_length=500, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    enabled = models.BooleanField()
    # property = models.CharField(max_length=500, blank=True)

    def __str__(self):
        return self.name

class Server(models.Model):
    openstack_id = models.CharField(max_length=100, unique=True)
    name = models.CharField(max_length=100)
    status = models.CharField(max_length=100)
    project = models.ForeignKey('Project', on_delete=models.DO_NOTHING) 

    def __str__(self):
        return self.name

class Flavor(models.Model):
    openstack_id = models.CharField(max_length=100, unique=True)
    name = models.CharField(max_length=100)
    vcpu = models.IntegerField()
    ram_gb = models.FloatField()
    disk_gb = models.IntegerField()
    aggregate_instance_extra_specs = ArrayField(models.CharField(max_length=200,default=''), default=list)

    def __str__(self):
        return self.name

class Aggregate(models.Model):
    openstack_id = models.CharField(max_length=100, unique=True)
    name = models.CharField(max_length=100, unique=True)
    aggregate_instance_extra_specs = ArrayField(models.CharField(max_length=200,default=''), default=list)
    created = models.DateTimeField(null=True)
    deleted = models.DateTimeField(null=True, blank=True)
    hypervisor_name = ArrayField(models.CharField(max_length=200, default=''), default=list)
    hypervisor = models.ManyToManyField('Hypervisor', blank=True)

    class Meta:
        ordering = ['created']

    def __str__(self):
        return self.name

class ServerEvent(models.Model):
    server = models.ForeignKey('Server', on_delete=models.DO_NOTHING, related_name='server_events')
    flavor = models.ForeignKey('Flavor', on_delete=models.DO_NOTHING)
    created = models.DateTimeField()
    name = models.CharField(max_length=100)
    os_state = models.CharField(max_length=100)
    bill_state = models.BooleanField()
    vcpu = models.IntegerField()
    ram_gb = models.FloatField()
    disk_gb = models.FloatField()

    def __str__(self):
        return self.name

class Hypervisor(models.Model):
    openstack_id = models.CharField(max_length=100, unique=True)
    name = models.CharField(max_length=100, unique=True)
    vcpu = models.IntegerField()
    ram_gb = models.FloatField() 
    disk_gb= models.FloatField(null=True)
    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name

class HypervisorEvent(models.Model):
    hypervisor = models.ForeignKey('Hypervisor', on_delete=models.DO_NOTHING, related_name='hypervisors')
    created = models.DateTimeField()
    name = models.CharField(max_length=100)
    os_state = models.CharField(max_length=100)
    bill_state = models.BooleanField()
    vcpu = models.IntegerField()
    ram_gb = models.FloatField()
    disk_gb = models.FloatField()

    def __str__(self):
        return self.name

class Volume(models.Model):
    openstack_id = models.CharField(max_length=100, unique=True)
    name = models.CharField(max_length=100)
    status = models.CharField(max_length=100)
    project = models.ForeignKey('Project', on_delete=models.DO_NOTHING)
    attached_to = models.ForeignKey('Server', on_delete=models.DO_NOTHING, blank=True, null=True)
    image_os_distro = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.name


class VolumeType(models.Model):
    openstack_id = models.CharField(max_length=100, unique=True)
    name = models.CharField(max_length=100)
    is_public = models.BooleanField()

    def __str__(self):
        return self.name


class VolumeEvent(models.Model):
    volume = models.ForeignKey(Volume, on_delete=models.DO_NOTHING, related_name='volume_events')
    created = models.DateTimeField()
    name = models.CharField(max_length=100)
    os_state = models.CharField(max_length=100)
    bill_state = models.BooleanField()
    size_gb = models.FloatField()
    volume_type = models.ForeignKey('VolumeType', on_delete=models.DO_NOTHING, related_name='volume_types')

    def __str__(self):
        return self.name

class Image(models.Model):
    openstack_id = models.CharField(max_length=100, unique=True)
    name = models.CharField(max_length=1000,default="NoName", null=True)
    status = models.CharField(max_length=100)
    project = models.ForeignKey('Project', on_delete=models.DO_NOTHING, verbose_name="project")

    def __str__(self):
        return self.name


class ImageEvent(models.Model):
    image = models.ForeignKey('Image', on_delete=models.DO_NOTHING, related_name='image_events')
    created = models.DateTimeField()
    name = models.CharField(max_length=100) 
    os_state = models.CharField(max_length=100)
    bill_state = models.BooleanField()
    size_gb = models.FloatField()

    def __str__(self):
        return self.name

class Backup(models.Model):
    openstack_id = models.CharField(max_length=100, unique=True)
    name = models.CharField(max_length=1000,default="NoName", null=True)
    status = models.CharField(max_length=100)
    project = models.ForeignKey('Project', on_delete=models.DO_NOTHING, verbose_name="project")

    def __str__(self):
        return self.openstack_id


class BackupEvent(models.Model):
    backup = models.ForeignKey('Backup', on_delete=models.DO_NOTHING, related_name='backup_events')
    created = models.DateTimeField()
    name = models.CharField(max_length=100) 
    os_state = models.CharField(max_length=100)
    bill_state = models.BooleanField()
    size_gb = models.FloatField()

    def __str__(self):
        return self.name

class BillCode(models.Model):
    code = models.CharField(max_length=100, unique=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.code

class BillCodeProjectRelation(models.Model):
    billcode = models.ForeignKey('BillCode', on_delete=models.DO_NOTHING)
    project = models.ForeignKey('Project', on_delete=models.DO_NOTHING)
    activated = models.DateTimeField(auto_now_add=True)
    deactivated = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.project

class MonthlyReport(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    start_date = models.DateTimeField(unique=True)
    end_date = models.DateTimeField(unique=True)
    data = JSONField()

    def __str__(self):
        return str(self.start_date)

class Report(models.Model):
    name = models.CharField(max_length=100, unique=True)
    
    def __str__(self):
        return self.name