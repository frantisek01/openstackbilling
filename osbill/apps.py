from django.apps import AppConfig


class OsbillConfig(AppConfig):
    name = 'osbill'
