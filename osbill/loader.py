import requests
from urllib3.exceptions import InsecureRequestWarning
import json
import datetime
import os
import logging

logger = logging.getLogger(__name__)

class Loader:

    def __init__(self, username, password):
        self.api_limit=1000
        self.url = os.environ['OS_URL']
        # self.token_url = ':5000/v3/auth/tokens/'
        self.token_url = '/identity/v3/auth/tokens/'
        # self.project_list_api = ':5000/v3/auth/projects'
        self.project_list_api = '/identity/v3/projects'
        # self.server_list_api = ':8774/v2.1/servers/detail?all_tenants=1&limit={}'
        self.server_list_api = '/compute/v2.1/servers/detail?all_tenants=1&limit={}'
        self.volume_list_api = '/volume/v3/{}/volumes/detail?all_tenants=True&limit={}'
        # self.volume_type_list_api = '/volume/v3/{}/types?all_tenants=True&osapi_max_limit={}'
        self.volume_type_list_api = '/volume/v3/{}/types?is_public=None'
        self.flavor_list_api = '/compute/v2.1/flavors/detail?is_public=None&limit={}'
        self.flavor_extra_api = '/compute/v2.1/flavors/{}/os-extra_specs'
        self.hypervisor_list_api = '/compute/v2.1/os-hypervisors/detail'
        # self.hypervisor_show_api=''
        self.aggregate_list_api='/compute/v2.1/os-aggregates'
        # self.aggregate_show_api=''
        self.image_list_api = '/image/v2/images?limit={}'
        self.backup_list_api = '/volume/v3/{}/backups/detail?all_tenants=True&limit={}'
        # self.event_list_api = ':8977/v2/events'
        # self.event_query = "?q.field=end_timestamp&q.op=le&q.value={}&q.field=event_type&q.op=eq&q.value={}&q.field=start_timestamp&q.op=ge&q.value={}"
        # self.relevant_events = ['compute.instance.resize.start', 'compute.instance.finish_resize.end',
        #                         'compute.instance.create.end',
        #                         'compute.instance.delete.end', 'compute.instance.shelve_offload.end',
        #                         'compute.instance.unshelve.end',
        #                         'volume.create.end', 'volume.delete.end', 'volume.resize.start', 'volume.resize.end', 'volume.retype',
        #                         'image.create', 'image.delete']
        self.username = username
        self.password = password


    def get_token(self, project=None):
        try:
            if project == None:
                json_data = '{ "auth": { "identity": { "methods": ["password"], "password": { "user": { "name": "' + self.username + '", "domain": { "id": "default" }, "password": "' + self.password + '" } }}}}'
            elif project == "admin":
                json_data = '{ "auth": { "identity": { "methods": ["password"], "password": { "user": { "name": "' + self.username + '", "domain": { "id": "default" }, "password": "' + self.password + '" } }},  "scope": { "project": { "name": "admin", "domain": { "id": "default" }}}}}'
            else:
                json_data = '{ "auth": { "identity": { "methods": ["password"], "password": { "user": { "name": "' + self.username + '", "domain": { "id": "default" }, "password": "' + self.password + '" } }},  "scope": { "project": { "name": "' + project + '", "domain": { "id": "default" }}}}}'

            json.loads(str(json_data))
            token_request = requests.post(self.url + self.token_url, data=json_data)
            token = token_request.headers["X-Subject-Token"] 
        except Exception as e:
            token = "Problem with OpenStack authentication: " + str(e)     
        return token

    def load_projects(self):
        token = self.get_token()
        data_request = requests.get(self.url + self.project_list_api, headers={'X-Auth-Token': token})
        json_obj = json.loads(data_request.text)
        logger.info(" Projects in all openstack: " + str(len(json_obj["projects"])))
        return json_obj["projects"]

    def load_servers(self):
        servers=[]
        token = self.get_token("admin")        
        while True:
            if len(servers) == 0:
                data_request = requests.get(self.url + self.server_list_api.format(self.api_limit), headers={'X-Auth-Token': token})                
                json_obj = json.loads(data_request.text)
                servers += json_obj['servers']
            if len(json_obj['servers']) == self.api_limit:
                data_request = requests.get(self.url + self.server_list_api.format(self.api_limit) + '&marker=' + servers[-1]['id'], headers={'X-Auth-Token': token})
                json_obj = json.loads(data_request.text)
                servers += json_obj['servers']
                continue
            else:
                break
        logger.info(" Servers in all projects: " + str(len(servers)))
        return servers

    def load_backups(self, projects):
        backups=[]
        for pr in projects:
            if pr['name'] == 'admin':
                project = pr  
        token = self.get_token("admin")        
        while True:
            if len(backups) == 0:
                data_request = requests.get(self.url + self.backup_list_api.format(project["id"], self.api_limit), headers={'X-Auth-Token': token})                
                json_obj = json.loads(data_request.text)
                backups += json_obj['backups']
            if len(json_obj['backups']) == self.api_limit:
                data_request = requests.get(self.url + self.backup_list_api.format(project["id"], self.api_limit) + '&marker=' + backups[-1]['id'], headers={'X-Auth-Token': token})
                json_obj = json.loads(data_request.text)
                backups += json_obj['backups']
                continue
            else:
                break
        logger.info(" Backups in all projects: " + str(len(backups)))
        return backups
    
    def load_volumes(self, projects):
        volumes=[]
        for pr in projects:
            if pr['name'] == 'admin':
                project = pr  
        token = self.get_token("admin")        
        while True:
            if len(volumes) == 0:
                data_request = requests.get(self.url + self.volume_list_api.format(project["id"], self.api_limit), headers={'X-Auth-Token': token})                
                json_obj = json.loads(data_request.text)
                volumes += json_obj['volumes']
            if len(json_obj['volumes']) == self.api_limit:
                data_request = requests.get(self.url + self.volume_list_api.format(project["id"], self.api_limit) + '&marker=' + volumes[-1]['id'], headers={'X-Auth-Token': token})
                json_obj = json.loads(data_request.text)
                volumes += json_obj['volumes']
                continue
            else:
                break
        logger.info(" Volumes in all projects: " + str(len(volumes)))
        return volumes

    def load_volume_types(self, projects):
        volume_types=[]
        for pr in projects:
            if pr['name'] == 'admin':
                project = pr  
        token = self.get_token("admin")
        while True:
            if len(volume_types) == 0:
                data_request = requests.get(self.url + self.volume_type_list_api.format(project["id"], self.api_limit), headers={'X-Auth-Token': token})                
                json_obj = json.loads(data_request.text)
                # print(str(json_obj))
                volume_types += json_obj['volume_types']
            if len(json_obj['volume_types']) == self.api_limit:
                data_request = requests.get(self.url + self.volume_type_list_api.format(project["id"], self.api_limit) + '&marker=' + volume_types[-1]['id'], headers={'X-Auth-Token': token})
                json_obj = json.loads(data_request.text)
                volume_types += json_obj['volume_types']
                continue
            else:
                break
        logger.info(" Volume Types in all projects: " + str(len(volume_types)))
        return volume_types

    def load_flavors(self):
        flavors=[]
        token = self.get_token("admin")
        while True:
            if len(flavors) == 0:
                data_request = requests.get(self.url + self.flavor_list_api.format(self.api_limit), headers={'X-Auth-Token': token})                
                json_obj = json.loads(data_request.text)
                # print(str(json_obj))
                flavors += json_obj['flavors']
            if len(json_obj['flavors']) == self.api_limit:
                data_request = requests.get(self.url + self.flavor_list_api.format(self.api_limit) + '&marker=' + flavors[-1]['id'], headers={'X-Auth-Token': token})
                json_obj = json.loads(data_request.text)
                # print(str(json_obj))
                flavors += json_obj['flavors']
                continue
            else:
                break
        logger.info(" Flavors in all projects: " + str(len(flavors)))
        for f in flavors:
            data_request = requests.get(self.url + self.flavor_extra_api.format(f["id"]), headers={'X-Auth-Token': token})
            json_obj = json.loads(data_request.text)
            extra = []
            for key in json_obj['extra_specs']:
                if 'aggregate_instance_extra_specs:' in key:
                  k=key.split('aggregate_instance_extra_specs:')[1]
                  extra.append(f'{k}={json_obj["extra_specs"][key]}')
            f['aggregate_instance_extra_specs']=extra
        return flavors

    def load_hypervisors(self):
        token = self.get_token("admin")
        hypervisors=[]
        while True:
            if len(hypervisors) == 0:
                data_request = requests.get(self.url + self.hypervisor_list_api.format(self.api_limit), headers={'X-Auth-Token': token})                
                json_obj = json.loads(data_request.text)
                hypervisors += json_obj['hypervisors']
            if len(json_obj['hypervisors']) == self.api_limit:
                data_request = requests.get(self.url + self.hypervisor_list_api.format(self.api_limit) + '&marker=' + hypervisors[-1]['id'], headers={'X-Auth-Token': token})
                json_obj = json.loads(data_request.text)
                hypervisors += json_obj['hypervisors']
                continue
            else:
                break
        logger.info(" Hypervisors in all projects: " + str(len(hypervisors)))
        return hypervisors

    def load_aggregates(self):
        token = self.get_token("admin")
        aggregates=[]
        while True:
            if len(aggregates) == 0:
                data_request = requests.get(self.url + self.aggregate_list_api.format(self.api_limit), headers={'X-Auth-Token': token})                
                json_obj = json.loads(data_request.text)
                aggregates += json_obj['aggregates']
            if len(json_obj['aggregates']) == self.api_limit:
                data_request = requests.get(self.url + self.aggregate_list_api.format(self.api_limit) + '&marker=' + aggregates[-1]['id'], headers={'X-Auth-Token': token})
                json_obj = json.loads(data_request.text)
                aggregates += json_obj['aggregates']
                continue
            else:
                break
        logger.info(" Aggregates in all projects: " + str(len(aggregates)))
        for a in aggregates:
            extra=[]  
            if a['metadata']:
                for key in a['metadata']:
                    extra.append(f'{key}={a["metadata"][key]}')
            # else:
            #     extra=None
            a['aggregate_instance_extra_specs']=extra
            hosts = []
            if a['hosts']:
                for h in a['hosts']:
                    hosts.append(h)
            a['hosts']=hosts
        return aggregates

    def load_images(self):
        images=[]
        token = self.get_token("admin")
        while True:
            if len(images) == 0:
                data_request = requests.get(self.url + self.image_list_api.format(self.api_limit), headers={'X-Auth-Token': token})
                json_obj = json.loads(data_request.text)
                images += json_obj['images']
                continue
            elif len(json_obj['images']) == self.api_limit:
                data_request = requests.get(self.url + self.image_list_api.format(self.api_limit) + '&marker=' + images[-1]['id'], headers={'X-Auth-Token': token})
                json_obj = json.loads(data_request.text)
                images += json_obj['images']
                continue
            else:
                break
        logger.info(" Images in all projects: " + str(len(images)))
        return images

    def load_events(self, projects, start, end):
        events = []
        for project in projects:            
            if project['enabled']==True:
                logger.info(str(datetime.datetime.now().strftime("%Y-%m-%dT%H%:%M%:%S")) + ' Project events loading started: ' + str(project['name']))
                token = self.get_token(project["name"])
                project_events=[]
                for event in self.relevant_events:
                    logger.info(str(datetime.datetime.now().strftime("%Y-%m-%dT%H%:%M%:%S")) + ' Loading event: ' + str(event) + 'for project: ' + str(project['name']))
                    data_request = requests.get(self.url + self.event_list_api + self.event_query.format(end, event, start), headers={'X-Auth-Token': token})
                    json_obj = json.loads(data_request.text)
                    if json_obj:
                        project_events.extend(json_obj)
                if project_events:
                    events.extend(project_events)                    
                logger.info(str(datetime.datetime.now().strftime("%Y-%m-%dT%H%:%M%:%S")) + ' Project ' + str(project['name']) + ' has ' + str(len(project_events)) + 'events')
        logger.info(" Events in all projects: " + str(len(events)))
        return events
