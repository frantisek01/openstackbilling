from osbill.models import (Server, ServerEvent, Volume, VolumeEvent, Image, ImageEvent, Backup, BackupEvent, BillCodeProjectRelation, Hypervisor, HypervisorEvent, Project, Flavor)
from osbill.dedicated_hosts import dedicated_hosts
import os


class Biller:

    def __init__(self, start_time, finish_time, projects):
        self.start_time = start_time
        self.finish_time = finish_time
        self.interval_h = (finish_time - start_time).total_seconds()/3600        
        self.projects = projects
        self.project_ids = self.projects.values_list('id', flat=True)

    def instance_calc(self):
        output = []
        shelved = False
        preshelved = False
        for instance in self.instances:
            run_time = 0.0
            resize_run_time = []
            consumption = {}
            instanceevents = self.get_events(instance)
            if not instanceevents:
                consumption = self.default_size(instance)
            if self.check_early_event(instance):
                early_event = self.get_early_event(instance)  
                if early_event.name == "shelve_offload":
                    run_time = 0    
                    shelved = True
                    preshelved = True                  
                else:          
                    run_time = self.interval_h
                consumption = self.size(early_event, run_time)
            for event in instanceevents:
                if "create" in event.name:
                    run_time = self.interval_h
                    run_time -= (event.created -
                                 self.start_time).total_seconds()/3600
                    consumption = self.size(event, run_time)
                elif "delete" in event.name and not shelved:
                    run_time -= (self.finish_time -
                                 event.created).total_seconds()/3600
                    if resize_run_time:
                        resize_run_time.pop()
                        resize_run_time.append(run_time)
                        consumption['size_time'] = resize_run_time
                    else:
                        consumption = self.size(event, run_time)
                elif event.name in ["resize", "retype"]:
                    run_time -= (self.finish_time -
                                 event.created).total_seconds()/3600
                    if resize_run_time:
                        resize_run_time.pop()
                    resize_run_time.append(run_time)
                    run_time = (self.finish_time -
                                event.created).total_seconds()/3600
                    resize_run_time.append(run_time)
                    consumption = self.resize(
                        event, resize_run_time, consumption)
                elif "shelve_offload" == event.name:
                    run_time -= (self.finish_time -
                                 event.created).total_seconds()/3600
                    if resize_run_time:
                        resize_run_time.pop()
                        resize_run_time.append(run_time)
                        consumption['size_time'] = resize_run_time
                    else:
                        consumption = self.size(event, run_time)
                    shelved = True
                elif "unshelve" == event.name:
                    if preshelved == True:
                        run_time = (self.finish_time -
                                 event.created).total_seconds()/3600
                    else:
                        run_time += (self.finish_time -
                                 event.created).total_seconds()/3600
                    if resize_run_time: 
                        resize_run_time.pop()    
                        resize_run_time.append(run_time)
                    else:
                        consumption = self.size(event, run_time)
                    shelved = False
                    preshelved = False
            if 'sum' in dir(self):
                consumption = self.sum(consumption)
            instance.billing_code = BillCodeProjectRelation.objects.filter(project=instance.project.id).filter(activated__lte=self.finish_time).prefetch_related('billcode').latest('activated').billcode.code
            output_instance = self.output_instance(consumption, instance)

            events_formated = list(instanceevents.values())
            for index, inst in enumerate(instanceevents.values()):
                events_formated[index]['created'] = inst['created'].strftime('%Y-%m-%d %H:%M:%S')
            output_instance['events'] = events_formated
            
            # output_instance['events']=list(instanceevents.values())
            output.append(output_instance)
        return output
    
    
class ImageBiller(Biller):
    def __init__(self, start_time, finish_time, projects, instance_id="No id provided"):
        super().__init__(start_time, finish_time, projects)
        self.instances = self.get_instances(instance_id)

    def get_events(self, instance):
        events = ImageEvent.objects.filter(image=instance).filter(
            created__lte=self.finish_time, created__gte=self.start_time).order_by('created')
        return events

    def get_instances(self, instance_id):
        if instance_id == "No id provided":
            exclude_early = ImageEvent.objects.filter(name__contains='delete', created__lte=self.start_time).values('image_id')
            exclude_late =ImageEvent.objects.filter(name__contains='create', created__gte=self.finish_time).values('image_id')
            instances = Image.objects.filter(project__in=self.project_ids).exclude(id__in=exclude_early).exclude(id__in=exclude_late).prefetch_related('project', 'project_id__openstack_id', 'project_id__description', 'project_id__name')
        else:
            instances = Image.objects.filter(openstack_id=instance_id).prefetch_related(
                'project', 'project_id__openstack_id', 'project_id__description', 'project_id__name')
        return instances

    def check_early_event(self, instance):
        if ImageEvent.objects.filter(image=instance, created__lte=self.start_time).exists() and ImageEvent.objects.filter(image=instance, created__lte=self.start_time).latest('created').name != 'delete':
            return 1
        else:
            return 0

    def get_early_event(self, instance):
        return ImageEvent.objects.filter(image=instance, created__lte=self.start_time).latest('created')
    
    def default_size(self, instance):
        if ImageEvent.objects.filter(image=instance, created__lte=self.start_time).exists():
            event = ImageEvent.objects.filter(image=instance, created__lte=self.start_time).latest('created')
        else:
            event = ImageEvent.objects.filter(image=instance, created__gte=self.start_time).earliest('created')
        size = {'size_gb': event.size_gb, 'size_time': 0.0}
        return size

    def size(self, event, size_time):
        size = {'size_gb': event.size_gb, 'size_time': size_time}
        return size

    def output_instance(self, consumption, instance):        
        out_instance = {
            'instance_id': instance.id,
            'instance_name': instance.name,
            'openstack_id': instance.openstack_id,
            'consumption': consumption['size_gb'] * consumption['size_time'],
            'project': instance.project.id,
            'project_name': instance.project.name,
            'project_description': instance.project.description,
            'project_openstack_id': instance.project.openstack_id,
            'size': consumption['size_gb'],            
            'run_time': consumption['size_time'],
            'billing_code': instance.billing_code
        }
        return out_instance

class VolumeBiller(Biller):
    def __init__(self, start_time, finish_time, projects, instance_id="No id provided"):
        super().__init__(start_time, finish_time, projects)
        self.instances = self.get_instances(instance_id)
        

    def get_events(self, instance):
        events = VolumeEvent.objects.filter(volume=instance).filter(
            created__lte=self.finish_time, created__gte=self.start_time).prefetch_related('volume_id__project', 'volume_type').order_by('created')
        return events

    def get_instances(self, instance_id):
        if instance_id == "No id provided":
            exclude_early = VolumeEvent.objects.filter(name__contains='delete', created__lte=self.start_time).values('volume_id')
            exclude_late =VolumeEvent.objects.filter(name__contains='create', created__gte=self.finish_time).values('volume_id')
            instances = Volume.objects.filter(project__in=self.project_ids).exclude(
                id__in=exclude_early).exclude(
                id__in=exclude_late).prefetch_related('project', 'project_id__openstack_id', 'attached_to', 'project_id__description', 'project_id__name')
        else:
            instances = Volume.objects.filter(openstack_id=instance_id).prefetch_related(
                'project', 'project_id__openstack_id', 'attached_to', 'project_id__description', 'project_id__name')
        return instances

    def check_early_event(self, instance):
        if VolumeEvent.objects.filter(volume=instance, created__lte=self.start_time).exists() and VolumeEvent.objects.filter(volume=instance, created__lte=self.start_time).latest('created').name != 'delete':
            return 1
        else:
            return 0

    def get_early_event(self, instance):
        return VolumeEvent.objects.filter(volume=instance, created__lte=self.start_time).latest('created')
    
    def default_size(self, instance):
        if VolumeEvent.objects.filter(volume=instance, created__lte=self.start_time).exists():
            event = VolumeEvent.objects.filter(volume=instance, created__lte=self.start_time).latest('created')
        else:
            event = VolumeEvent.objects.filter(volume=instance, created__gte=self.start_time).earliest('created')
        size = {'disk': [event.size_gb], 'volume_type_name': [event.volume_type.name], 'volume_type': [event.volume_type.id], 'size_time': [0.0]}
        return size

    def size(self, event, size_time):
        size = {'disk': [event.size_gb], 'volume_type_name': [event.volume_type.name], 'volume_type': [event.volume_type.id], 'size_time': [size_time]}
        return size

    def resize(self, event, resize_time, consumption):
        consumption['disk'].append(event.size_gb)
        consumption['volume_type_name'].append(event.volume_type.name)
        consumption['volume_type'].append(event.volume_type.id)
        consumption['size_time'] = resize_time
        return consumption

    def output_instance(self, consumption, instance):
        if instance.attached_to == None:
            attached_to = ''
        else:
            attached_to = instance.attached_to.id
        out_instance = {
            'instance_id': instance.id,
            'instance_name': instance.name,
            'openstack_id': instance.openstack_id,
            'consumption': consumption,
            'attached_to': attached_to,
            'project': instance.project.id,
            'project_name': instance.project.name,
            'project_description': instance.project.description,
            'project_openstack_id': instance.project.openstack_id,
            'billing_code': instance.billing_code
        }
        return out_instance

    def sum(self,consumption):
        total_h_gb = {}
        for index, v in enumerate(consumption['size_time']):
            if consumption['volume_type_name'][index]+ '_volume_type' in total_h_gb and v:
                total_h_gb[consumption['volume_type_name'][index] + '_volume_type'] += consumption['disk'][index] * v
            elif v:
                total_h_gb[consumption['volume_type_name'][index]+ '_volume_type'] = consumption['disk'][index] * v
        consumption['total_h_gb'] = total_h_gb
        return consumption

class ServerBiller(Biller):
    def __init__(self, start_time, finish_time, projects, instance_id="No id provided"):
        super().__init__(start_time, finish_time, projects)
        self.instances = self.get_instances(instance_id)

    def get_events(self, instance):
        events = ServerEvent.objects.filter(server=instance).filter(
            created__lte=self.finish_time, created__gte=self.start_time).prefetch_related('server_id__project', 'flavor', 'flavor_id__name', 'flavor_id__aggregate_instance_extra_specs').order_by('created')
        return events

    def get_instances(self, instance_id):        
        if instance_id == "No id provided":
            exclude_early = ServerEvent.objects.filter(name__contains='delete', created__lte=self.start_time).values('server_id')
            exclude_late =ServerEvent.objects.filter(name__contains='create', created__gte=self.finish_time).values('server_id')
            instances = Server.objects.filter(project__in=self.project_ids).exclude(
                id__in=exclude_early).exclude(
                id__in=exclude_late).prefetch_related('project', 'project_id__openstack_id', 'project_id__description', 'project_id__name')
        else:
            instances = Server.objects.filter(openstack_id=instance_id).prefetch_related(
                'project', 'project_id__openstack_id', 'project_id__description', 'project_id__name')
        return instances

    def check_early_event(self, instance):
        if ServerEvent.objects.filter(server=instance, created__lte=self.start_time).exists() and ServerEvent.objects.filter(server=instance, created__lte=self.start_time).latest('created').name != 'delete':
            return 1
        else:
            return 0

    def get_early_event(self, instance):
        return ServerEvent.objects.filter(server=instance, created__lte=self.start_time).latest('created')
    
    def default_size(self, instance):
        if ServerEvent.objects.filter(server=instance, created__lte=self.start_time).exists():
            event = ServerEvent.objects.filter(server=instance, created__lte=self.start_time).latest('created')
        else:
            event = ServerEvent.objects.filter(server=instance, created__gte=self.start_time).earliest('created')
        compute_type = self.get_compute_type(event)
        size = {'vcpu': [float(event.vcpu)],'ram': [float(event.ram_gb)], 'disk': [float(event.disk_gb)], 'flavor': [event.flavor.name], 'size_time': [0.0], 'compute_type':[compute_type],}
        return size

    def get_compute_type(self, event):
        dedicated_disk_filter = str(os.environ.get('COMPUTE_TYPE_FILTER', 'compute_type'))
        ct = 'default'
        for a in event.flavor.aggregate_instance_extra_specs:
            if dedicated_disk_filter in a:
                ct = a.split("=")[1]
        return ct

    def size(self, event, size_time):
        compute_type = self.get_compute_type(event)
        size = {
            'flavor': [event.flavor.name],
            'size_time': [size_time],
            'compute_type':[compute_type],
            'vcpu': [float(event.vcpu)],
            'ram': [float(event.ram_gb)],
            'disk': [float(event.disk_gb)],
            }
        return size

    def resize(self, event, resize_time, consumption):
        compute_type = self.get_compute_type(event)
        consumption['flavor'].append(event.flavor.name)
        consumption['compute_type'].append(compute_type)
        consumption['size_time'] = resize_time        
        consumption['ram'].append(float(event.ram_gb))
        consumption['vcpu'].append(float(event.vcpu))
        consumption['disk'].append(float(event.disk_gb))
        return consumption

    def get_license(self,instance):  
        licensed = "NoLicense"      
        if Volume.objects.filter(attached_to=instance).exists():
            for vol in Volume.objects.filter(attached_to=instance):
                if vol.image_os_distro:
                    licensed = vol.image_os_distro
        return licensed


    def output_instance(self, consumption, instance):
        out_instance = {
            'instance_id': instance.id,
            'instance_name': instance.name,
            'openstack_id': instance.openstack_id,
            'consumption': consumption,
            'project': instance.project.id,
            'project_name': instance.project.name,
            'project_description': instance.project.description,
            'project_openstack_id': instance.project.openstack_id,
            'billing_code': instance.billing_code,
            'license': self.get_license(instance)
        }
        return out_instance

    def sum(self,consumption):
        vcpu_h = 0.0
        mem_h_gb = 0.0
        disk_h_gb = 0.0
        run_time = 0.0
        for index, value in enumerate(consumption['size_time']): 
            vcpu_h += consumption['size_time'][index] * consumption['vcpu'][index]
            mem_h_gb += consumption['size_time'][index] * consumption['ram'][index]
            disk_h_gb += consumption['size_time'][index] * consumption['disk'][index]
            run_time += consumption['size_time'][index]
        consumption['vcpu_h']=vcpu_h
        consumption['mem_h_gb']=mem_h_gb
        consumption['disk_h_gb']=disk_h_gb
        consumption['run_time'] = run_time
        return consumption


class HypervisorBiller(Biller):
    def __init__(self, start_time, finish_time, projects, instance_id="No id provided"):
        super().__init__(start_time, finish_time, projects)
        self.instances = self.get_instances(instance_id)
        # self.dedicated_hosts = dedicated_hosts()

    def get_events(self, instance):
        dedicated_h = dedicated_hosts()
        for dh in dedicated_h:
            if instance.id in dh['dedicated_hosts']:
                project = Project.objects.get(openstack_id=dh['project_openstack_id'])
                flavor = Flavor.objects.filter(name__in=dh['dedicated_flavors']).latest('id')
        events = HypervisorEvent.objects.filter(hypervisor=instance).filter(
            created__lte=self.finish_time, created__gte=self.start_time).order_by('created')
        for e in events:
            e.project = project
            e.flavor = flavor

        return events

    def get_instances(self, instance_id): 
        dedicated_h = dedicated_hosts()
        hosts = []
        for dh in dedicated_h:                
            project = Project.objects.get(openstack_id=dh['project_openstack_id'])
            flavor = Flavor.objects.filter(name__in=dh['dedicated_flavors']).latest('id')
            if instance_id == "No id provided":
                for h in dh['dedicated_hosts']:
                    instance = Hypervisor.objects.get(id=h)                
                    instance.project = project
                    instance.flavor = flavor
                    instance.dedicated_disk = dh['dedicated_disk']
                    hosts.append(instance)
            else:
                instance = Hypervisor.objects.get(openstack_id=h)
                instance.project = project
                instance.flavor = flavor
                instance.dedicated_disk = dh['dedicated_disk']
                hosts.append(instance)
        return hosts

    def check_early_event(self, instance):
        if HypervisorEvent.objects.filter(hypervisor=instance, created__lte=self.start_time).exists() and HypervisorEvent.objects.filter(hypervisor=instance, created__lte=self.start_time).latest('created').name != 'delete':
            return 1
        else:
            return 0

    def get_early_event(self, instance):
        event = HypervisorEvent.objects.filter(hypervisor=instance, created__lte=self.start_time).latest('created')
        event.flavor = instance.flavor
        return 
    
    def default_size(self, instance):
        if HypervisorEvent.objects.filter(hypervisor=instance, created__lte=self.start_time).exists():
            event = HypervisorEvent.objects.filter(hypervisor=instance, created__lte=self.start_time).latest('created')
        else:
            event = HypervisorEvent.objects.filter(hypervisor=instance, created__gte=self.start_time).earliest('created')
        size = {'vcpu': [float(event.vcpu)],'ram': [float(event.ram_gb)], 'disk': [float(event.disk_gb)], 'flavor': [instance.flavor.name], 'size_time': [0.0]}
        return size

    def get_compute_type(self, event):
        dedicated_disk_filter = str(os.environ.get('COMPUTE_TYPE_FILTER', 'compute_type'))
        ct = 'default_dedicated_host'
        for a in event.flavor.aggregate_instance_extra_specs:
            if dedicated_disk_filter in a:
                ct = a.split("=")[1]
        return ct

    def size(self, event, size_time):
        compute_type = self.get_compute_type(event)
        size = {
            'size_time': [size_time],
            'compute_type':[compute_type],
            'vcpu': [float(event.vcpu)],
            'ram': [float(event.ram_gb)],
            'disk': [float(event.disk_gb)],
            }
        return size

    def resize(self, event, resize_time, consumption):
        compute_type = self.get_compute_type(event)
        consumption['compute_type'].append(compute_type)
        consumption['size_time'] = resize_time        
        consumption['ram'].append(float(event.ram_gb))
        consumption['vcpu'].append(float(event.vcpu))
        consumption['disk'].append(float(event.disk_gb))
        return consumption

    def output_instance(self, consumption, instance):
        out_instance = {
            'instance_id': instance.id,
            'instance_name': instance.name,
            'openstack_id': instance.openstack_id,
            'consumption': consumption,
            'project': instance.project.id,
            'project_name': instance.project.name,
            'project_description': instance.project.description,
            'project_openstack_id': instance.project.openstack_id,
            'billing_code': instance.billing_code
        }
        return out_instance

    def sum(self,consumption):
        vcpu_h = 0.0
        mem_h_gb = 0.0
        disk_h_gb = 0.0
        run_time = 0.0
        for index, value in enumerate(consumption['size_time']): 
            vcpu_h += consumption['size_time'][index] * consumption['vcpu'][index]
            mem_h_gb += consumption['size_time'][index] * consumption['ram'][index]
            disk_h_gb += consumption['size_time'][index] * consumption['disk'][index]
            run_time += consumption['size_time'][index]
        consumption['vcpu_h']=vcpu_h
        consumption['mem_h_gb']=mem_h_gb
        consumption['disk_h_gb']=disk_h_gb
        consumption['run_time'] = run_time
        return consumption

class BackupBiller(Biller):
    def __init__(self, start_time, finish_time, projects, instance_id="No id provided"):
        super().__init__(start_time, finish_time, projects)
        self.instances = self.get_instances(instance_id)

    def get_events(self, instance):
        events = BackupEvent.objects.filter(backup=instance).filter(
            created__lte=self.finish_time, created__gte=self.start_time).order_by('created')
        return events

    def get_instances(self, instance_id):
        if instance_id == "No id provided":
            exclude_early = BackupEvent.objects.filter(name__contains='delete', created__lte=self.start_time).values('backup_id')
            exclude_late =BackupEvent.objects.filter(name__contains='create', created__gte=self.finish_time).values('backup_id')
            instances = Backup.objects.filter(project__in=self.project_ids).exclude(id__in=exclude_early).exclude(id__in=exclude_late).prefetch_related('project', 'project_id__openstack_id', 'project_id__description', 'project_id__name')
        else:
            instances = Backup.objects.filter(openstack_id=instance_id).prefetch_related(
                'project', 'project_id__openstack_id', 'project_id__description', 'project_id__name')
        return instances

    def check_early_event(self, instance):
        if BackupEvent.objects.filter(backup=instance, created__lte=self.start_time).exists() and BackupEvent.objects.filter(backup=instance, created__lte=self.start_time).latest('created').name != 'delete':
            return 1
        else:
            return 0

    def get_early_event(self, instance):
        return BackupEvent.objects.filter(backup=instance, created__lte=self.start_time).latest('created')
    
    def default_size(self, instance):
        if BackupEvent.objects.filter(backup=instance, created__lte=self.start_time).exists():
            event = BackupEvent.objects.filter(backup=instance, created__lte=self.start_time).latest('created')
        else:
            event = BackupEvent.objects.filter(backup=instance, created__gte=self.start_time).earliest('created')
        size = {'size_gb': event.size_gb, 'size_time': 0.0}
        return size

    def size(self, event, size_time):
        size = {'size_gb': event.size_gb, 'size_time': size_time}
        return size

    def output_instance(self, consumption, instance):        
        out_instance = {
            'backup': True,
            'instance_id': instance.id,
            'instance_name': instance.name,
            'openstack_id': instance.openstack_id,
            'consumption': consumption['size_gb'] * consumption['size_time'],
            'project': instance.project.id,
            'project_name': instance.project.name,
            'project_description': instance.project.description,
            'project_openstack_id': instance.project.openstack_id,
            'size': consumption['size_gb'],            
            'run_time': consumption['size_time'],
            'billing_code': instance.billing_code
        }
        return out_instance