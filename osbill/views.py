from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from .loader_manager import Loader_manager
from .models import Project, BillCodeProjectRelation
from .biller import ImageBiller
from .bill_sum import bill_sum
from .excelwriter import excelwriter
# from openpyxl.writer.excel import save_virtual_workbook
import logging
import datetime
import traceback
import pytz
import json

logger = logging.getLogger(__name__)

def billsum(request):
    start_time = datetime.datetime.strptime("2023-01-01 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
    finish_time = datetime.datetime.strptime("2023-04-02 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
    pb_sum = bill_sum(start_time, finish_time)
    return JsonResponse(pb_sum,json_dumps_params={'indent': 2}, safe=False)

def test(request):
    start_time = datetime.datetime.strptime("2023-01-01 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
    finish_time = datetime.datetime.strptime("2023-02-02 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
    projects = Project.objects.filter(enabled=True,description__contains=" - terp ")
    bc =BillCodeProjectRelation.objects.all()
    for pr in projects:
        pr.billing_code=BillCodeProjectRelation.objects.filter(project=pr).prefetch_related('billcode').latest('activated').billcode.code
    bill_run = ImageBiller(start_time, finish_time, projects)
    bill_cons = bill_run.instance_calc()
    return JsonResponse(bill_cons,json_dumps_params={'indent': 2}, safe=False)

def loader(request):
    logger.info(str(datetime.datetime.now().strftime(
        "%Y-%m-%dT%H:%M:%S")) + ' Loader started ****************************')
    print(str(datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S")) +
          ' Loader started ****************************')
    lm = Loader_manager()
    
    try:
        lm.upload_projects()
    except Exception as e:
        logger.error("Project loader failed: " + str(e), exc_info=True)
        print("Project loader failed: " + str(e))

    try:
        lm.upload_flavors()
    except Exception as e:
        logger.error("Flavor loader failed: " + str(e), exc_info=True)
        print("Flavor loader failed: " + str(e))
    try:
        lm.upload_servers()
    except Exception as e:
        logger.error("Server loader failed: " + str(e), exc_info=True)
        print("Server loader failed: " + str(e))
    try:
        lm.upload_images()
    except Exception as e:
        logger.error("Image loader failed: " + str(e), exc_info=True)
        print("Image loader failed: " + str(e))

    projects = Project.objects.filter(enabled=True)
    serialized_projects = []
    for project in projects:
        row = {'id': project.openstack_id, 'name': project.name,
               'description': project.description, 'enabled': project.enabled}
        serialized_projects.append(row)

    try:
        # projects = Project.objects.filter(enabled=True)
        # serialized_projects = []
        # for project in projects:
        #     row = {'id': project.openstack_id, 'name': project.name,
        #            'description': project.description, 'enabled': project.enabled}
        #     serialized_projects.append(row)
        lm.upload_volume_types(serialized_projects)
        lm.upload_volumes(serialized_projects)
    except Exception as e:
        logger.error("Volume types or volume loader failed: " +
                     str(e), exc_info=True)
        print("Volume types or volume loader failed: " + str(e) + " " + str())

    # try:
    lm.upload_backups(serialized_projects)
    # except Exception as e:
    #     logger.error("Backup loader failed: " + str(e), exc_info=True)
    #     print("Backup loader failed: " + str(e))

    try:
        lm.upload_hypervisors()
    except Exception as e:
        logger.error("Hypervisor loader failed: " + str(e), exc_info=True)
        print("Hypervisor loader failed: " + str(traceback.print_exc()))

    try:
        lm.upload_aggregates()
    except Exception as e:
        logger.error("Aggregate loader failed: " + str(e), exc_info=True)
        print("Aggregate loader failed: " + str(traceback.print_exc()))

    try:
        lm.upload_billing_codes()
    except Exception as e:
        logger.error("Billing codes loader failed: " + str(e), exc_info=True)
        print("Billing codes loader failed: " + str(traceback.print_exc()))
    
    try:
        lm.update_dedicated_hypervisors()
    except Exception as e:
        logger.error("Hypervisor update failed: " + str(e), exc_info=True)
        print("Hypervisor update failed: " + str(traceback.print_exc()))

    logger.info(str(datetime.datetime.now().strftime(
        "%Y-%m-%dT%H:%M:%S")) + ' Loader finished ***************************')
    print(str(datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S")) +
          ' Loader finished ***************************')

    return redirect('/admin')
