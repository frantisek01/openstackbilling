from django.urls import include, path, reverse_lazy
from django.conf.urls.static import static
from django.conf import settings
from . import views
from django.views.generic.base import RedirectView
from django.contrib import admin

urlpatterns = [
    path('loader/', views.loader, name='osbill-loader'),
    path('test/', views.test, name='osbill-test'),
    path('billsum/', views.billsum, name='osbill-billsum'),
    path('', RedirectView.as_view(url=reverse_lazy('admin:index'))),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [path('__debug__/', include(debug_toolbar.urls)),] + urlpatterns
