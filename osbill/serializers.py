from rest_framework import serializers
import datetime
import logging
import pytz
from osbill.models import (Server, ServerEvent, Project, Image, ImageEvent, Backup, BackupEvent, Flavor,
                           Volume, VolumeEvent, VolumeType, BillCode, BillCodeProjectRelation, MonthlyReport,
                           Aggregate, Hypervisor, HypervisorEvent)


logger = logging.getLogger(__name__)

class HypervisorEventSerializer(serializers.ModelSerializer):
    class Meta:
        model = HypervisorEvent
        fields = ('hypervisor', 'created', 'name','os_state',
                  'bill_state', 'vcpu', 'ram_gb', 'disk_gb')

    def to_internal_value(self, data):
        if data['id']:
            host = Hypervisor.objects.get(id=data['id'])
        if data['status'] == 'deleted':
            name = "delete"
        elif not HypervisorEvent.objects.filter(hypervisor=data['id']).exists():
            name = "create"
        else:
            latest_event = HypervisorEvent.objects.filter(
                hypervisor=data['id']).latest('created')
            if latest_event.vcpu != host.vcpu or latest_event.ram_gb != host.ram_gb or latest_event.disk_gb != host.disk_gb:
                    name = "resize"
            else:
                 raise serializers.ValidationError(
                            "No billing related changes to record")
        created = datetime.datetime.now()
        hypervisor = host.id
        os_state = data['status'].lower()        
        vcpu = host.vcpu
        ram_gb = host.ram_gb
        disk_gb = host.disk_gb
        bill_state = False if os_state == 'deleted' else True
        return super().to_internal_value({
            'hypervisor': hypervisor,
            'created': created,
            'name': name,
            'os_state': os_state,
            'bill_state': bill_state,
            'vcpu': int(vcpu),
            'ram_gb': float(ram_gb),
            'disk_gb': float(disk_gb)
        })

class ServerEventSerializer(serializers.ModelSerializer):
    class Meta:
        model = ServerEvent
        fields = ('server', 'flavor', 'created', 'name','os_state',
                  'bill_state', 'vcpu', 'ram_gb', 'disk_gb')

    def to_internal_value(self, data):
        if data['id'] and data['flavor']['id']:
            server = Server.objects.get(openstack_id=data['id'])
            flavor = Flavor.objects.get(openstack_id=data['flavor']['id'])
        else:
            raise serializers.ValidationError("Invalid data")
        # name = 'Dead parrot'    
        if data['status'] == 'deleted':
            name = "delete"
        elif not ServerEvent.objects.filter(server=server.id).exists():
            name = "create"
        else:
            latest_event = ServerEvent.objects.filter(
                server=server.id).latest('created')
            if latest_event.flavor.id != flavor.id:
                    name = "resize"
            elif latest_event.os_state != data['status'].lower():
                    if latest_event.os_state == 'shelved_offloaded' and data['status'].lower() != 'shelved_offloaded':
                        name = "unshelve"
                    elif data['status'].lower() == 'shelved_offloaded' and latest_event.os_state != 'shelved_offloaded':
                        name = "shelve"
                    else:
                        raise serializers.ValidationError(
                            "No billing related changes to record")
            else:
                raise serializers.ValidationError(
                    "No billing related changes to record")
        
        created = datetime.datetime.now()
        server = server.id
        os_state = data['status'].lower()
        flavor_id = flavor.id  
        vcpu = flavor.vcpu
        ram_gb = flavor.ram_gb
        disk_gb = flavor.disk_gb

        bill_state = False if os_state == 'deleted' or os_state == 'shelved_offloaded' or os_state == 'shelved' else True
        return super().to_internal_value({
            'server': server,
            'flavor': flavor_id,
            'created': created,
            'name': name,
            'os_state': os_state,
            'bill_state': bill_state,
            'vcpu': int(vcpu),
            'ram_gb': float(ram_gb),
            'disk_gb': float(disk_gb)
        })



class ServerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Server
        fields = ('openstack_id', 'name', 'status', 'project')
        
    def create(self, validated_data):
        server, created = Server.objects.update_or_create(
            openstack_id=validated_data.get('id', None),
            project = Project.objects.get(openstack_id = validated_data.get('tenant_id')),
            defaults = {'name' : validated_data.get('name', None), 'status' : validated_data.get('status', None).lower()})
        return server
    
class ProjectSerializer(serializers.ModelSerializer):
    class Meta:

        model = Project
        fields = ('openstack_id','name', 'description', 'enabled')

    def create(self, validated_data):
        project, created = Project.objects.update_or_create(
            openstack_id=validated_data.get('id', None),
            defaults = {'description': validated_data.get('description', None),
            'name' : validated_data.get('name', None),
            # 'property' : validated_data.get('property', None),
            'enabled': validated_data.get('enabled', None)
            })
        return project

class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ('openstack_id', 'name', 'status', 'project')

    def create(self, data):
        image, created = Image.objects.update_or_create(
            openstack_id=data.get('id', None),
            project = Project.objects.get(openstack_id = data.get('owner')),
            defaults = {'name' : data.get('name', None),
            'status': data.get('status', None)
            })
        return image


class ImageEventSerializer(serializers.ModelSerializer):
    class Meta:
        model = ImageEvent
        fields = ('image', 'created', 'name', 'os_state', 'bill_state', 'size_gb' )

    def to_internal_value(self, data):
        if data['id']:
            image = Image.objects.get(openstack_id=data['id'])            
        else:
            raise serializers.ValidationError("Invalid data")
        size_gb = None
        if data['status'] == 'deleted':
            name = "delete"
        elif not ImageEvent.objects.filter(image=image.id).exists():
            name = "create"
        else:
            latest_event = ImageEvent.objects.filter(
                image=image.id).latest('created')            
            if data['size'] != None and latest_event.size_gb != data['size']/1024/1024/1024:
                    name = "size_update"
            else:
                    raise serializers.ValidationError(
                            "No billing related changes to record")

        image = image.id
        created = datetime.datetime.now() 
        os_state = data['status'].lower()
        size_gb = data['size']
        bill_state = False if os_state == 'deleted' else True
        if size_gb == None: size_gb = 0
        return super().to_internal_value({
            'image': image,
            'created': created,
            'name': name,
            'os_state': os_state,
            'bill_state': bill_state,
            'size_gb': int(size_gb)/1024/1024/1024
        })

class BackupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Backup
        fields = ('openstack_id', 'name', 'status', 'project')

    def create(self, data):
        if not Volume.objects.filter(openstack_id=data.get('volume_id', None)).exists():
            if not Project.objects.filter(openstack_id='ProjectForBackupsWithNoVolume').exists():
                p = Project(openstack_id='ProjectForBackupsWithNoVolume',name='ProjectForBackupsWithNoVolume', enabled=True)
                p.save()
            pr = Project.objects.get(openstack_id='ProjectForBackupsWithNoVolume')
        else:
            pr = Volume.objects.get(openstack_id=data.get('volume_id', None)).project
        backup, created = Backup.objects.update_or_create(
            openstack_id=data.get('id', None),
            project = pr,
            defaults = {'name' : data.get('name', None),
            'status': data.get('status', None)
            })
        return backup


class BackupEventSerializer(serializers.ModelSerializer):
    class Meta:
        model = BackupEvent
        fields = ('backup', 'created', 'name', 'os_state', 'bill_state', 'size_gb' )

    def to_internal_value(self, data):
        if data['id']:
            backup = Backup.objects.get(openstack_id=data['id'])            
        else:
            raise serializers.ValidationError("Invalid data")
        size_gb = None
        if data['status'] == 'deleted':
            name = "delete"
        elif not BackupEvent.objects.filter(backup=backup.id).exists():
            name = "create"
        else:
            latest_event = BackupEvent.objects.filter(
                backup=backup.id).latest('created')            
            if data['size'] != None and latest_event.size_gb != data['size']:
                    name = "size_update"
            else:
                    raise serializers.ValidationError(
                            "No billing related changes to record")

        backup = backup.id
        created = datetime.datetime.now() 
        os_state = data['status'].lower()
        size_gb = data['size']
        bill_state = False if os_state == 'deleted' else True
        if size_gb == None: size_gb = 0
        return super().to_internal_value({
            'backup': backup,
            'created': created,
            'name': name,
            'os_state': os_state,
            'bill_state': bill_state,
            'size_gb': int(size_gb)
        })


class VolumeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Volume
        fields = ('openstack_id', 'name', 'status', 'project', 'image_os_distro')
    
    def create(self, data):
        volume, created = Volume.objects.update_or_create(
            openstack_id=data.get('id', None),            
            project = Project.objects.get(openstack_id = data.get('os-vol-tenant-attr:tenant_id')),
            defaults = {'name' : data.get('name', None),
            'status': data.get('status', None),
            'image_os_distro':data.get('image_os_distro', None),
            'attached_to': data.get('attached_to', None),
            })
        return volume


class VolumeEventSerializer(serializers.ModelSerializer):
    class Meta:
        model = VolumeEvent
        fields = ('volume', 'created', 'name', 'os_state', 'bill_state', 'size_gb', 'volume_type')

    def to_internal_value(self, data):        
        if 'id' in data and 'volume_type' in data:
            if not VolumeType.objects.filter(name='Volume type not available').exists():
                vt = VolumeType(openstack_id='Volume type not available',name='Volume type not available', is_public=True)
                vt.save()
            volume = Volume.objects.get(openstack_id=data['id'])
            vtype = VolumeType.objects.get(openstack_id='Volume type not available')
            try:
               vtype = VolumeType.objects.filter(name=data['volume_type']).latest('id')
            except Exception as e:
                logger.error("Volume type not found, default used, volume ID: " + str(volume.id) + ", volume type: " + str(data['volume_type']) + " - " + str(e), exc_info=True)
                print("Volume type loader failed: " + str(e))                
        else:
            raise serializers.ValidationError("Invalid data")
       # name = 'Dead parrot'   
        if data['status'] == 'deleted':
            name = "delete"
        elif not VolumeEvent.objects.filter(volume_id=volume.id).exists():
            name = "create"
        else:               
            latest_event = VolumeEvent.objects.filter(
                volume=volume.id).latest('created')
            if latest_event.volume_type.id != vtype.id:
                name = "retype"
            elif latest_event.size_gb != data['size']:
                name = "resize"
            else:
                raise serializers.ValidationError(
                        "No billing related changes to record")

        # if name == "Dead parrot":
        #     raise serializers.ValidationError(
        #             "Dead parrot is not valid name for billing event.")
        

        os_state = data['status'].lower()   
        created = datetime.datetime.now()
        size_gb = data['size']

        bill_state = False if os_state == 'deleted' else True
        if size_gb == None: size_gb = 0
        return super().to_internal_value({
            'volume': volume.id,
            'created': created,
            'name': name,
            'os_state': os_state,
            'bill_state': bill_state,
            'size_gb': int(size_gb),
            # 'storage_type': vtype.name,
            'volume_type': vtype.id
        })

class FlavorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Flavor
        fields = ('openstack_id','name', 'vcpu', 'ram_gb', 'disk_gb', 'aggregate_instance_extra_specs')
    
    def to_internal_value(self, data):
        if data["OS-FLV-EXT-DATA:ephemeral"]:
            disk = data['disk'] + data["OS-FLV-EXT-DATA:ephemeral"]
        else:
            disk = data['disk']
        input = {
                'openstack_id': data["id"],
                'name': data['name'],
                'vcpu': data['vcpus'],
                'ram_gb': int(data['ram'])/1024,
                'disk_gb': disk,               
            }
        if data['aggregate_instance_extra_specs']:
            input['aggregate_instance_extra_specs'] = data['aggregate_instance_extra_specs']         
        return super().to_internal_value(input)
        

class VolumeTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = VolumeType
        fields = ('openstack_id','name', 'is_public')

    def to_internal_value(self, data):
        return super().to_internal_value({
            'openstack_id': data["id"],
            'name': data['name'],
            'is_public': data['is_public']
        })

class BillCodeSerializer(serializers.ModelSerializer):
    class Meta:
        model = BillCode
        fields = ('code','active')

class BillCodeProjectRelationSerializer(serializers.ModelSerializer):
    class Meta:
        model = BillCodeProjectRelation
        fields = ('billcode','project', 'activated', 'deactivated')

class MonthlyReportSerializer(serializers.ModelSerializer):
    class Meta:
        model = MonthlyReport
        fields = ('created','start_date','end_date','data')

    
class HypervisorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Hypervisor
        fields = ('openstack_id','name','vcpu','ram_gb','disk_gb')

    def create(self, data):
        hypervisor, created = Hypervisor.objects.update_or_create(
            openstack_id=data.get('id', None),
            defaults = {'name' : data.get('hypervisor_hostname', None),
            'vcpu': data.get('vcpus', None),
            'ram_gb' : int(data.get('memory_mb', None))/1024,
            'disk_gb' : data.get('local_gb', None),
            })
        return hypervisor

class AggregateSerializer(serializers.ModelSerializer):
    hypervisor_name = serializers.ListField(allow_empty=True,child=serializers.CharField(max_length=500, allow_blank=True))
    aggregate_instance_extra_specs= serializers.ListField(allow_empty=True,child=serializers.CharField(max_length=500, allow_blank=True))
    class Meta:
        model = Aggregate
        fields = ('openstack_id','name','aggregate_instance_extra_specs','created','deleted', 'hypervisor_name', 'hypervisor')

    def create(self, data):
        aggregate, created = Aggregate.objects.update_or_create(
            openstack_id=data.get('id', None),
            created = datetime.datetime.strptime(data.get('created_at', None), '%Y-%m-%dT%H:%M:%S.%f').replace(tzinfo=pytz.utc) if data.get('created_at', None) else None,
            deleted = datetime.datetime.strptime(data.get('deleted_at', None), '%Y-%m-%dT%H:%M:%S.%f').replace(tzinfo=pytz.utc) if data.get('deleted_at', None) else None,
            defaults = {'name' : data.get('name', None),
            'aggregate_instance_extra_specs': data.get('aggregate_instance_extra_specs', None),
            'hypervisor_name' : data.get('hosts', None),
            })
        for h in data['hosts']:
            aggregate.hypervisor.add(Hypervisor.objects.get(name__contains=h))
        return aggregate

