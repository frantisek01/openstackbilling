from osbill.biller import (ImageBiller, BackupBiller, VolumeBiller, ServerBiller, HypervisorBiller)
from osbill.models import Project, BillCodeProjectRelation, MonthlyReport
from osbill.models import Hypervisor, Aggregate, Flavor, Volume, ServerEvent, VolumeEvent
from datetime import datetime
from osbill.dedicated_hosts import dedicated_hosts
import os


def bill_sum(start_time, finish_time):
    projects = Project.objects.filter(enabled=True)#,description__contains=" - terp ")
    # for pr in projects:
    #     pr.billing_code=BillCodeProjectRelation.objects.filter(project=pr).prefetch_related('billcode').latest('activated').billcode.code

    image_bill = ImageBiller(start_time, finish_time, projects)
    backup_bill = BackupBiller(start_time, finish_time, projects)
    volume_bill = VolumeBiller(start_time, finish_time, projects)
    server_bill = ServerBiller(start_time, finish_time, projects)
    hypervisor_bill = HypervisorBiller(start_time, finish_time, projects)
    images = image_bill.instance_calc()
    backups = backup_bill.instance_calc()
    volumes = volume_bill.instance_calc()
    servers = server_bill.instance_calc()
    hypervisors = hypervisor_bill.instance_calc()
    consumption=[]
    consumption.extend(images)
    consumption.extend(backups)
    consumption.extend(volumes)
    consumption.extend(servers)
    consumption.extend(hypervisors)
    # bills = serversum(start_time, finish_time)
    d_hosts=dedicated_hosts()
    report = {}
    for instance in consumption:
        if not instance['billing_code'] in report:
            report[instance['billing_code']] = {}
            report[instance['billing_code']]['sum'] = {}
            report[instance['billing_code']]['projects'] = {}
        if not instance['project_openstack_id'] in report[instance['billing_code']]['projects']:
            report[instance['billing_code']]['projects'][instance['project_openstack_id']] = {}
            report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum'] = {}
        if not 'project_name' in report[instance['billing_code']]['projects'][instance['project_openstack_id']]:
            report[instance['billing_code']]['projects'][instance['project_openstack_id']]['project_name'] = instance['project_name']
            report[instance['billing_code']]['projects'][instance['project_openstack_id']]['project_description'] = instance['project_description']
        if 'size' in instance and not 'backup' in instance:
            if not 'glance_image_storage' in report[instance['billing_code']]['sum']:
                report[instance['billing_code']]['sum']['glance_image_storage'] = instance['consumption']
            else:
                report[instance['billing_code']]['sum']['glance_image_storage'] += instance['consumption']
            if not 'glance_image_storage' in report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum']:    
                report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum']['glance_image_storage'] = instance['consumption']
            else:                
                report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum']['glance_image_storage'] += instance['consumption']
        elif 'backup' in instance:
            if not 'backup_storage' in report[instance['billing_code']]['sum']:
                report[instance['billing_code']]['sum']['backup_storage'] = instance['consumption']
            else:
                report[instance['billing_code']]['sum']['backup_storage'] += instance['consumption']
            if not 'backup_storage' in report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum']:    
                report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum']['backup_storage'] = instance['consumption']
            else:                
                report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum']['backup_storage'] += instance['consumption']        
        elif 'flavor' in instance['consumption']:
            if not 'licenses' in report[instance['billing_code']]['sum']:
                report[instance['billing_code']]['sum']['licenses'] = {}
            if not 'licenses' in report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum']:
                report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum']['licenses'] = {}
                
            if not instance['license'] in report[instance['billing_code']]['sum']['licenses']:
                report[instance['billing_code']]['sum']['licenses'][instance['license']] = 1
            else:
                report[instance['billing_code']]['sum']['licenses'][instance['license']] += 1    

            if not instance['license'] in report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum']['licenses']:
                report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum']['licenses'][instance['license']] = 1
            else:
                report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum']['licenses'][instance['license']] += 1

            for index, con in enumerate(instance['consumption']['flavor']):
                if con in d_hosts:
                    instance['consumption']['vcpu'][index] = 0
                    instance['consumption']['ram'][index] = 0
                    instance['consumption']['disk'][index] = 0
                if not instance['consumption']['compute_type'][index]+'_compute_type' in report[instance['billing_code']]['sum']:
                    report[instance['billing_code']]['sum'][instance['consumption']['compute_type'][index] +'_compute_type'] = {}
                if not instance['consumption']['compute_type'][index]+'_compute_type' in report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum']:
                    report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type'] = {}
                if not 'vcpu_h' in report[instance['billing_code']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']:
                    report[instance['billing_code']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['vcpu_h'] = instance['consumption']['vcpu'][index] * instance['consumption']['size_time'][index]
                    report[instance['billing_code']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['mem_h_gb'] = instance['consumption']['ram'][index] * instance['consumption']['size_time'][index]
                    report[instance['billing_code']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['disk_h_gb'] = instance['consumption']['disk'][index] * instance['consumption']['size_time'][index]
                    report[instance['billing_code']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['run_time'] = instance['consumption']['size_time'][index]
                else:
                    report[instance['billing_code']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['vcpu_h'] += instance['consumption']['vcpu'][index] * instance['consumption']['size_time'][index]
                    report[instance['billing_code']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['mem_h_gb'] += instance['consumption']['ram'][index] * instance['consumption']['size_time'][index]
                    report[instance['billing_code']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['disk_h_gb'] += instance['consumption']['disk'][index] * instance['consumption']['size_time'][index]
                    report[instance['billing_code']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['run_time'] += instance['consumption']['size_time'][index]
                if not 'vcpu_h' in report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']:
                    report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['vcpu_h'] = instance['consumption']['vcpu'][index] * instance['consumption']['size_time'][index]
                    report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['mem_h_gb'] = instance['consumption']['ram'][index] * instance['consumption']['size_time'][index]
                    report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['disk_h_gb'] = instance['consumption']['disk'][index] * instance['consumption']['size_time'][index]
                    report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['run_time'] = instance['consumption']['size_time'][index]
                else:
                    report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['vcpu_h'] += instance['consumption']['vcpu'][index] * instance['consumption']['size_time'][index]
                    report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['mem_h_gb'] += instance['consumption']['ram'][index] * instance['consumption']['size_time'][index]
                    report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['disk_h_gb'] += instance['consumption']['disk'][index] * instance['consumption']['size_time'][index]
                    report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['run_time'] += instance['consumption']['size_time'][index]
        elif 'vcpu' in instance['consumption']:
            for index, con in enumerate(instance['consumption']['vcpu']):
                if not instance['consumption']['compute_type'][index]+'_compute_type' in report[instance['billing_code']]['sum']:
                    report[instance['billing_code']]['sum'][instance['consumption']['compute_type'][index] +'_compute_type'] = {}
                if not instance['consumption']['compute_type'][index]+'_compute_type' in report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum']:
                    report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type'] = {}
                if not 'vcpu_h' in report[instance['billing_code']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']:
                    report[instance['billing_code']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['vcpu_h'] = instance['consumption']['vcpu'][index] * instance['consumption']['size_time'][index]
                    report[instance['billing_code']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['mem_h_gb'] = instance['consumption']['ram'][index] * instance['consumption']['size_time'][index]
                    report[instance['billing_code']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['disk_h_gb'] = instance['consumption']['disk'][index] * instance['consumption']['size_time'][index]
                    report[instance['billing_code']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['run_time'] = instance['consumption']['size_time'][index]
                else:
                    report[instance['billing_code']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['vcpu_h'] += instance['consumption']['vcpu'][index] * instance['consumption']['size_time'][index]
                    report[instance['billing_code']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['mem_h_gb'] += instance['consumption']['ram'][index] * instance['consumption']['size_time'][index]
                    report[instance['billing_code']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['disk_h_gb'] += instance['consumption']['disk'][index] * instance['consumption']['size_time'][index]
                    report[instance['billing_code']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['run_time'] += instance['consumption']['size_time'][index]

                if not 'vcpu_h' in report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']:
                    report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['vcpu_h'] = instance['consumption']['vcpu'][index] * instance['consumption']['size_time'][index]
                    report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['mem_h_gb'] = instance['consumption']['ram'][index] * instance['consumption']['size_time'][index]
                    report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['disk_h_gb'] = instance['consumption']['disk'][index] * instance['consumption']['size_time'][index]
                    report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['run_time'] = instance['consumption']['size_time'][index]
                else:
                    report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['vcpu_h'] += instance['consumption']['vcpu'][index] * instance['consumption']['size_time'][index]
                    report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['mem_h_gb'] += instance['consumption']['ram'][index] * instance['consumption']['size_time'][index]
                    report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['disk_h_gb'] += instance['consumption']['disk'][index] * instance['consumption']['size_time'][index]
                    report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum'][instance['consumption']['compute_type'][index]+'_compute_type']['run_time'] += instance['consumption']['size_time'][index]
                
        elif 'volume_type_name' in instance['consumption']:
                for index, con in enumerate(instance['consumption']['volume_type_name']):
                    if not instance['consumption']['volume_type_name'][index]+'_volume_type' in report[instance['billing_code']]['sum']:
                        report[instance['billing_code']]['sum'][instance['consumption']['volume_type_name'][index]+'_volume_type'] = instance['consumption']['disk'][index] * instance['consumption']['size_time'][index]
                    else:
                        report[instance['billing_code']]['sum'][instance['consumption']['volume_type_name'][index]+'_volume_type'] += instance['consumption']['disk'][index] * instance['consumption']['size_time'][index]
                    if not instance['consumption']['volume_type_name'][index]+'_volume_type' in report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum']:
                        report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum'][instance['consumption']['volume_type_name'][index]+'_volume_type'] = instance['consumption']['disk'][index] * instance['consumption']['size_time'][index]
                    else:                        
                        report[instance['billing_code']]['projects'][instance['project_openstack_id']]['sum'][instance['consumption']['volume_type_name'][index]+'_volume_type'] += instance['consumption']['disk'][index] * instance['consumption']['size_time'][index]
        
        if not 'instances' in report[instance['billing_code']]['projects'][instance['project_openstack_id']]:
            report[instance['billing_code']]['projects'][instance['project_openstack_id']]['instances'] = {}
            report[instance['billing_code']]['projects'][instance['project_openstack_id']]['instances']['server'] = []
            report[instance['billing_code']]['projects'][instance['project_openstack_id']]['instances']['volume'] = []
            report[instance['billing_code']]['projects'][instance['project_openstack_id']]['instances']['image'] = []
            report[instance['billing_code']]['projects'][instance['project_openstack_id']]['instances']['backup'] = []
        # if not instance in report[instance['billing_code']]['projects'][instance['project_openstack_id']]['instances']:
        if 'backup' in instance:
            report[instance['billing_code']]['projects'][instance['project_openstack_id']]['instances']['backup'].append(instance)
        if 'size' in instance:
            report[instance['billing_code']]['projects'][instance['project_openstack_id']]['instances']['image'].append(instance)
        elif 'volume_type_name' in instance['consumption']:
            report[instance['billing_code']]['projects'][instance['project_openstack_id']]['instances']['volume'].append(instance)
        elif 'vcpu' in instance['consumption']: 
            report[instance['billing_code']]['projects'][instance['project_openstack_id']]['instances']['server'].append(instance)
    return report















        


  