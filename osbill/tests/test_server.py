from django.test import TestCase
from osbill.biller import ServerBiller
from osbill.models import Project, Server, ServerEvent, Flavor, BillCodeProjectRelation, BillCode
import datetime
import pytz

class ServerCalcTestCase(TestCase):
     
    maxDiff = None
    def setUp(self):
        bc = BillCode.objects.create( 
            id = 1, 
            code = "20986.91.04",
            active = True)        
        project = Project.objects.create( 
            id = 1, 
            openstack_id = "d670223972c2479ebcc5c7924e9f4a2e",
            name = "test-project", 
            description = "test project - terp 20986.91.04", 
            created = "2020-01-01 00:00:00.000000+00",
            enabled = True)
        bcpr = BillCodeProjectRelation.objects.create( 
            id = 1, 
            billcode = BillCode.objects.get(id=1),
            project = Project.objects.get(id=1), 
            activated = "2020-01-01 00:00:00+00:00")
        Flavor.objects.bulk_create(
        [
        Flavor(
            id = 0, 
            openstack_id = 'd670223972c2479ebcc5c7924e9f4a2r', 
            name = '1-1-0', 
            ram_gb = '1', 
            disk_gb = '0', 
            vcpu=1),
        Flavor(
            id = 1, 
            openstack_id = 'd670223972c2479ebcc5c7924e9f4a2q', 
            name = '2-2-0', 
            ram_gb = '2', 
            disk_gb = '0', 
            vcpu=2),
        Flavor(
            id = 2, 
            openstack_id = 'd670223972c2479ebcc5c7924e9f4a2w', 
            name = '3-3-10', 
            ram_gb = '3', 
            disk_gb = '10', 
            vcpu=3),
        ])
        Server.objects.bulk_create(
        [
        Server(
            id = 1, 
            openstack_id = "test-server-id-1", 
            name = "test_server_1", 
            project = Project.objects.get(id=1)),
        Server(
            id = 2, 
            openstack_id = "test-server-id-2", 
            name = "test_server_2", 
            project = Project.objects.get(id=1)),
        Server(
            id = 3, 
            openstack_id = "test-server-id-3", 
            name = "test_server_3", 
            project = Project.objects.get(id=1)),
        Server(
            id = 4, 
            openstack_id = "test-server-id-4", 
            name = "test_server_4", 
            project = Project.objects.get(id=1)),
        Server(
            id = 5, 
            openstack_id = "test-server-id-5", 
            name = "test_server_5", 
            project = Project.objects.get(id=1)),
        Server(
            id = 6, 
            openstack_id = "test-server-id-6", 
            name = "test_server_6", 
            project = Project.objects.get(id=1)),
        Server(
            id = 7, 
            openstack_id = "test-server-id-7", 
            name = "test_server_7", 
            project = Project.objects.get(id=1)),
        Server(
            id = 8, 
            openstack_id = "test-server-id-8", 
            name = "test_server_8", 
            project = Project.objects.get(id=1)),
        Server(
            id = 9, 
            openstack_id = "test-server-id-9", 
            name = "test_server_9", 
            project = Project.objects.get(id=1)),
        Server(
            id = 10, 
            openstack_id = "test-server-id-10", 
            name = "test_server_10", 
            project = Project.objects.get(id=1)),
        Server(
            id = 11, 
            openstack_id = "test-server-id-11", 
            name = "test_server_11", 
            project = Project.objects.get(id=1)),
        Server(
            id = 12, 
            openstack_id = "test-server-id-12", 
            name = "test_server_12", 
            project = Project.objects.get(id=1)),
        Server(
            id = 13, 
            openstack_id = "test-server-id-13", 
            name = "test_server_13", 
            project = Project.objects.get(id=1)),
        Server(
            id = 14, 
            openstack_id = "test-server-id-14", 
            name = "test_server_14", 
            project = Project.objects.get(id=1)),
        Server(
            id = 15, 
            openstack_id = "test-server-id-15", 
            name = "test_server_15", 
            project = Project.objects.get(id=1)),
        Server(
            id = 16, 
            openstack_id = "test-server-id-16", 
            name = "test_server_16", 
            project = Project.objects.get(id=1)),
        Server(
            id = 17, 
            openstack_id = "test-server-id-17", 
            name = "test_server_17", 
            project = Project.objects.get(id=1)),
        Server(
            id = 18, 
            openstack_id = "test-server-id-18", 
            name = "test_server_18", 
            project = Project.objects.get(id=1)),
        Server(
            id = 19, 
            openstack_id = "test-server-id-19", 
            name = "test_server_19", 
            project = Project.objects.get(id=1)),
        Server(
            id = 20, 
            openstack_id = "test-server-id-20", 
            name = "test_server_20", 
            project = Project.objects.get(id=1)),
        Server(
            id = 21, 
            openstack_id = "test-server-id-21", 
            name = "test_server_21", 
            project = Project.objects.get(id=1)),
        Server(
            id = 22, 
            openstack_id = "test-server-id-22", 
            name = "test_server_22", 
            project = Project.objects.get(id=1)),
        Server(
            id = 23, 
            openstack_id = "test-server-id-23", 
            name = "test_server_23", 
            project = Project.objects.get(id=1)),
        Server(
            id = 24, 
            openstack_id = "test-server-id-24", 
            name = "test_server_24", 
            project = Project.objects.get(id=1)),
        Server(
            id = 25, 
            openstack_id = "test-server-id-25", 
            name = "test_server_25", 
            project = Project.objects.get(id=1)),
        Server(
            id = 26, 
            openstack_id = "test-server-id-26", 
            name = "test_server_26", 
            project = Project.objects.get(id=1)),
        Server(
            id = 27, 
            openstack_id = "test-server-id-27", 
            name = "test_server_27", 
            project = Project.objects.get(id=1)),
        Server(
            id = 28, 
            openstack_id = "test-server-id-28", 
            name = "test_server_28", 
            project = Project.objects.get(id=1)),
        Server(
            id = 29, 
            openstack_id = "test-server-id-29", 
            name = "test_server_29", 
            project = Project.objects.get(id=1)),
        Server(
            id = 30, 
            openstack_id = "test-server-id-30", 
            name = "test_server_30", 
            project = Project.objects.get(id=1)),
        Server(
            id = 31, 
            openstack_id = "test-server-id-31", 
            name = "test_server_31", 
            project = Project.objects.get(id=1)),
        Server(
            id = 32, 
            openstack_id = "test-server-id-32", 
            name = "test_server_32", 
            project = Project.objects.get(id=1)),
        Server(
            id = 33, 
            openstack_id = "test-server-id-33", 
            name = "test_server_33", 
            project = Project.objects.get(id=1)),
        Server(
            id = 34, 
            openstack_id = "test-server-id-34", 
            name = "test_server_34", 
            project = Project.objects.get(id=1)),
        Server(
            id = 35, 
            openstack_id = "test-server-id-35", 
            name = "test_server_35", 
            project = Project.objects.get(id=1)),
        Server(
            id = 36, 
            openstack_id = "test-server-id-36", 
            name = "test_server_36", 
            project = Project.objects.get(id=1)),
        Server(
            id = 37, 
            openstack_id = "test-server-id-37", 
            name = "test_server_37", 
            project = Project.objects.get(id=1)),
        Server(
            id = 38, 
            openstack_id = "test-server-id-38", 
            name = "test_server_38", 
            project = Project.objects.get(id=1)),
        Server(
            id = 39, 
            openstack_id = "test-server-id-39", 
            name = "test_server_39", 
            project = Project.objects.get(id=1)),
        Server(
            id = 40, 
            openstack_id = "test-server-id-40", 
            name = "test_server_40", 
            project = Project.objects.get(id=1)),
        Server(
            id = 41, 
            openstack_id = "test-server-id-41", 
            name = "test_server_41", 
            project = Project.objects.get(id=1)),
        Server(
            id = 42, 
            openstack_id = "test-server-id-42", 
            name = "test_server_42", 
            project = Project.objects.get(id=1)),
        Server(
            id = 43, 
            openstack_id = "test-server-id-43", 
            name = "test_server_43", 
            project = Project.objects.get(id=1)),
        Server(
            id = 44, 
            openstack_id = "test-server-id-44", 
            name = "test_server_44", 
            project = Project.objects.get(id=1)),
        Server(
            id = 45, 
            openstack_id = "test-server-id-45", 
            name = "test_server_45", 
            project = Project.objects.get(id=1)),
        Server(
            id = 46, 
            openstack_id = "test-server-id-46", 
            name = "test_server_46", 
            project = Project.objects.get(id=1)),
        Server(
            id = 47, 
            openstack_id = "test-server-id-47", 
            name = "test_server_47", 
            project = Project.objects.get(id=1)),
        Server(
            id = 48, 
            openstack_id = "test-server-id-48", 
            name = "test_server_48", 
            project = Project.objects.get(id=1)),
        Server(
            id = 49, 
            openstack_id = "test-server-id-49", 
            name = "test_server_49", 
            project = Project.objects.get(id=1)),
        Server(
            id = 50, 
            openstack_id = "test-server-id-50", 
            name = "test_server_50", 
            project = Project.objects.get(id=1)),
        Server(
            id = 51, 
            openstack_id = "test-server-id-51", 
            name = "test_server_51", 
            project = Project.objects.get(id=1)),
        Server(
            id = 52, 
            openstack_id = "test-server-id-52", 
            name = "test_server_52", 
            project = Project.objects.get(id=1)),
        Server(
            id = 53, 
            openstack_id = "test-server-id-53", 
            name = "test_server_53", 
            project = Project.objects.get(id=1)),
        Server(
            id = 54, 
            openstack_id = "test-server-id-54", 
            name = "test_server_54", 
            project = Project.objects.get(id=1)),
        Server(
            id = 55, 
            openstack_id = "test-server-id-55", 
            name = "test_server_55", 
            project = Project.objects.get(id=1)),
        Server(
            id = 56, 
            openstack_id = "test-server-id-56", 
            name = "test_server_56", 
            project = Project.objects.get(id=1)),
        Server(
            id = 57, 
            openstack_id = "test-server-id-57", 
            name = "test_server_57", 
            project = Project.objects.get(id=1)),
        Server(
            id = 58, 
            openstack_id = "test-server-id-58", 
            name = "test_server_58", 
            project = Project.objects.get(id=1)),
        Server(
            id = 59, 
            openstack_id = "test-server-id-59", 
            name = "test_server_59", 
            project = Project.objects.get(id=1)),
        Server(
            id = 60, 
            openstack_id = "test-server-id-60", 
            name = "test_server_60", 
            project = Project.objects.get(id=1)),
        Server(
            id = 61, 
            openstack_id = "test-server-id-61", 
            name = "test_server_61", 
            project = Project.objects.get(id=1)),
        Server(
            id = 62, 
            openstack_id = "test-server-id-62", 
            name = "test_server_62", 
            project = Project.objects.get(id=1)),
        Server(
            id = 63, 
            openstack_id = "test-server-id-63", 
            name = "test_server_63", 
            project = Project.objects.get(id=1)),
        Server(
            id = 64, 
            openstack_id = "test-server-id-64", 
            name = "test_server_64", 
            project = Project.objects.get(id=1)),
        Server(
            id = 65, 
            openstack_id = "test-server-id-65", 
            name = "test_server_65", 
            project = Project.objects.get(id=1)),
        Server(
            id = 66, 
            openstack_id = "test-server-id-66", 
            name = "test_server_66", 
            project = Project.objects.get(id=1)),
        Server(
            id = 67, 
            openstack_id = "test-server-id-67", 
            name = "test_server_67", 
            project = Project.objects.get(id=1)),
        Server(
            id = 68, 
            openstack_id = "test-server-id-68", 
            name = "test_server_68", 
            project = Project.objects.get(id=1)),
        Server(
            id = 69, 
            openstack_id = "test-server-id-69", 
            name = "test_server_69", 
            project = Project.objects.get(id=1)),
        Server(
            id = 70, 
            openstack_id = "test-server-id-70", 
            name = "test_server_70", 
            project = Project.objects.get(id=1)),
        Server(
            id = 71, 
            openstack_id = "test-server-id-71", 
            name = "test_server_71", 
            project = Project.objects.get(id=1)),
        Server(
            id = 72, 
            openstack_id = "test-server-id-72", 
            name = "test_server_72", 
            project = Project.objects.get(id=1)),
        Server(
            id = 73, 
            openstack_id = "test-server-id-73", 
            name = "test_server_73", 
            project = Project.objects.get(id=1)),
        Server(
            id = 74, 
            openstack_id = "test-server-id-74", 
            name = "test_server_74", 
            project = Project.objects.get(id=1)),
        Server(
            id = 75, 
            openstack_id = "test-server-id-75", 
            name = "test_server_75", 
            project = Project.objects.get(id=1)),
        Server(
            id = 76, 
            openstack_id = "test-server-id-76", 
            name = "test_server_76", 
            project = Project.objects.get(id=1)),
        Server(
            id = 77, 
            openstack_id = "test-server-id-77", 
            name = "test_server_77", 
            project = Project.objects.get(id=1)),
        Server(
            id = 78, 
            openstack_id = "test-server-id-78", 
            name = "test_server_78", 
            project = Project.objects.get(id=1)),
        Server(
            id = 79, 
            openstack_id = "test-server-id-79", 
            name = "test_server_79", 
            project = Project.objects.get(id=1)),
        Server(
            id = 80, 
            openstack_id = "test-server-id-80", 
            name = "test_server_80", 
            project = Project.objects.get(id=1)),
        Server(
            id = 81, 
            openstack_id = "test-server-id-81", 
            name = "test_server_81", 
            project = Project.objects.get(id=1)),
        Server(
            id = 82, 
            openstack_id = "test-server-id-82", 
            name = "test_server_82", 
            project = Project.objects.get(id=1)),
        Server(
            id = 83, 
            openstack_id = "test-server-id-83", 
            name = "test_server_83", 
            project = Project.objects.get(id=1)),
        Server(
            id = 84, 
            openstack_id = "test-server-id-84", 
            name = "test_server_84", 
            project = Project.objects.get(id=1)),
        Server(
            id = 85, 
            openstack_id = "test-server-id-85", 
            name = "test_server_85", 
            project = Project.objects.get(id=1)),
        Server(
            id = 86, 
            openstack_id = "test-server-id-86", 
            name = "test_server_86", 
            project = Project.objects.get(id=1)),
        Server(
            id = 87, 
            openstack_id = "test-server-id-87", 
            name = "test_server_87", 
            project = Project.objects.get(id=1)),
        Server(
            id = 88, 
            openstack_id = "test-server-id-88", 
            name = "test_server_88", 
            project = Project.objects.get(id=1)),
        Server(
            id = 89, 
            openstack_id = "test-server-id-89", 
            name = "test_server_89", 
            project = Project.objects.get(id=1)),
        Server(
            id = 90, 
            openstack_id = "test-server-id-90", 
            name = "test_server_90", 
            project = Project.objects.get(id=1)),
        Server(
            id = 91, 
            openstack_id = "test-server-id-91", 
            name = "test_server_91", 
            project = Project.objects.get(id=1)),
        Server(
            id = 92, 
            openstack_id = "test-server-id-92", 
            name = "test_server_92", 
            project = Project.objects.get(id=1)),
        Server(
            id = 93, 
            openstack_id = "test-server-id-93", 
            name = "test_server_93", 
            project = Project.objects.get(id=1)),
        Server(
            id = 94, 
            openstack_id = "test-server-id-94", 
            name = "test_server_94", 
            project = Project.objects.get(id=1)),
        Server(
            id = 95, 
            openstack_id = "test-server-id-95", 
            name = "test_server_95", 
            project = Project.objects.get(id=1)),
        Server(
            id = 96, 
            openstack_id = "test-server-id-96", 
            name = "test_server_96", 
            project = Project.objects.get(id=1)),
        Server(
            id = 97, 
            openstack_id = "test-server-id-97", 
            name = "test_server_97", 
            project = Project.objects.get(id=1)),
        Server(
            id = 98, 
            openstack_id = "test-server-id-98", 
            name = "test_server_98", 
            project = Project.objects.get(id=1)),
        Server(
            id = 99, 
            openstack_id = "test-server-id-99", 
            name = "test_server_99", 
            project = Project.objects.get(id=1)),
        Server(
            id = 100, 
            openstack_id = "test-server-id-100", 
            name = "test_server_100", 
            project = Project.objects.get(id=1)),
        Server(
            id = 101, 
            openstack_id = "test-server-id-101", 
            name = "test_server_101", 
            project = Project.objects.get(id=1)),
        Server(
            id = 102, 
            openstack_id = "test-server-id-102", 
            name = "test_server_102", 
            project = Project.objects.get(id=1)),
        Server(
            id = 103, 
            openstack_id = "test-server-id-103", 
            name = "test_server_103", 
            project = Project.objects.get(id=1)),
        Server(
            id = 104, 
            openstack_id = "test-server-id-104", 
            name = "test_server_104", 
            project = Project.objects.get(id=1)),
        Server(
            id = 105, 
            openstack_id = "test-server-id-105", 
            name = "test_server_105", 
            project = Project.objects.get(id=1)),
        Server(
            id = 106, 
            openstack_id = "test-server-id-106", 
            name = "test_server_106", 
            project = Project.objects.get(id=1)),
        Server(
            id = 107, 
            openstack_id = "test-server-id-107", 
            name = "test_server_107", 
            project = Project.objects.get(id=1)),
        Server(
            id = 108, 
            openstack_id = "test-server-id-108", 
            name = "test_server_108", 
            project = Project.objects.get(id=1)),
        Server(
            id = 109, 
            openstack_id = "test-server-id-109", 
            name = "test_server_109", 
            project = Project.objects.get(id=1)),
        Server(
            id = 110, 
            openstack_id = "test-server-id-110", 
            name = "test_server_110", 
            project = Project.objects.get(id=1)),
        Server(
            id = 111, 
            openstack_id = "test-server-id-111", 
            name = "test_server_111", 
            project = Project.objects.get(id=1)),
        Server(
            id = 112, 
            openstack_id = "test-server-id-112", 
            name = "test_server_112", 
            project = Project.objects.get(id=1)),
        Server(
            id = 113, 
            openstack_id = "test-server-id-113", 
            name = "test_server_113", 
            project = Project.objects.get(id=1)),
        Server(
            id = 114, 
            openstack_id = "test-server-id-114", 
            name = "test_server_114", 
            project = Project.objects.get(id=1)),
        Server(
            id = 115, 
            openstack_id = "test-server-id-115", 
            name = "test_server_115", 
            project = Project.objects.get(id=1)),
        Server(
            id = 116, 
            openstack_id = "test-server-id-116", 
            name = "test_server_116", 
            project = Project.objects.get(id=1)),
        Server(
            id = 117, 
            openstack_id = "test-server-id-117", 
            name = "test_server_117", 
            project = Project.objects.get(id=1)),
        Server(
            id = 118, 
            openstack_id = "test-server-id-118", 
            name = "test_server_118", 
            project = Project.objects.get(id=1)),
        Server(
            id = 119, 
            openstack_id = "test-server-id-119", 
            name = "test_server_119", 
            project = Project.objects.get(id=1)),
        Server(
            id = 120, 
            openstack_id = "test-server-id-120", 
            name = "test_server_120", 
            project = Project.objects.get(id=1)),
        Server(
            id = 121, 
            openstack_id = "test-server-id-121", 
            name = "test_server_121", 
            project = Project.objects.get(id=1)),
        Server(
            id = 122, 
            openstack_id = "test-server-id-122", 
            name = "test_server_122", 
            project = Project.objects.get(id=1)),
        Server(
            id = 123, 
            openstack_id = "test-server-id-123", 
            name = "test_server_123", 
            project = Project.objects.get(id=1)),
        Server(
            id = 124, 
            openstack_id = "test-server-id-124", 
            name = "test_server_124", 
            project = Project.objects.get(id=1)),
        Server(
            id = 125, 
            openstack_id = "test-server-id-125", 
            name = "test_server_125", 
            project = Project.objects.get(id=1)),
        Server(
            id = 126, 
            openstack_id = "test-server-id-126", 
            name = "test_server_126", 
            project = Project.objects.get(id=1)),
        Server(
            id = 127, 
            openstack_id = "test-server-id-127", 
            name = "test_server_127", 
            project = Project.objects.get(id=1)),
        Server(
            id = 128, 
            openstack_id = "test-server-id-128", 
            name = "test_server_128", 
            project = Project.objects.get(id=1)),
        Server(
            id = 129, 
            openstack_id = "test-server-id-129", 
            name = "test_server_129", 
            project = Project.objects.get(id=1)),
        Server(
            id = 130, 
            openstack_id = "test-server-id-130", 
            name = "test_server_130", 
            project = Project.objects.get(id=1)),
        Server(
            id = 131, 
            openstack_id = "test-server-id-131", 
            name = "test_server_131", 
            project = Project.objects.get(id=1)),
        Server(
            id = 132, 
            openstack_id = "test-server-id-132", 
            name = "test_server_132", 
            project = Project.objects.get(id=1)),
        Server(
            id = 133, 
            openstack_id = "test-server-id-133", 
            name = "test_server_133", 
            project = Project.objects.get(id=1)),
        Server(
            id = 134, 
            openstack_id = "test-server-id-134", 
            name = "test_server_134", 
            project = Project.objects.get(id=1)),
        Server(
            id = 135, 
            openstack_id = "test-server-id-135", 
            name = "test_server_135", 
            project = Project.objects.get(id=1)),
        Server(
            id = 136, 
            openstack_id = "test-server-id-136", 
            name = "test_server_136", 
            project = Project.objects.get(id=1)),
        Server(
            id = 137, 
            openstack_id = "test-server-id-137", 
            name = "test_server_137", 
            project = Project.objects.get(id=1)),
        Server(
            id = 138, 
            openstack_id = "test-server-id-138", 
            name = "test_server_138", 
            project = Project.objects.get(id=1)),
        Server(
            id = 139, 
            openstack_id = "test-server-id-139", 
            name = "test_server_139", 
            project = Project.objects.get(id=1)),
        Server(
            id = 140, 
            openstack_id = "test-server-id-140", 
            name = "test_server_140", 
            project = Project.objects.get(id=1)),
        Server(
            id = 141, 
            openstack_id = "test-server-id-141", 
            name = "test_server_141", 
            project = Project.objects.get(id=1)),
        Server(
            id = 142, 
            openstack_id = "test-server-id-142", 
            name = "test_server_142", 
            project = Project.objects.get(id=1)),
        Server(
            id = 143, 
            openstack_id = "test-server-id-143", 
            name = "test_server_143", 
            project = Project.objects.get(id=1)),
        Server(
            id = 144, 
            openstack_id = "test-server-id-144", 
            name = "test_server_144", 
            project = Project.objects.get(id=1)),
        Server(
            id = 145, 
            openstack_id = "test-server-id-145", 
            name = "test_server_145", 
            project = Project.objects.get(id=1)),
        Server(
            id = 146, 
            openstack_id = "test-server-id-146", 
            name = "test_server_146", 
            project = Project.objects.get(id=1)),
        Server(
            id = 147, 
            openstack_id = "test-server-id-147", 
            name = "test_server_147", 
            project = Project.objects.get(id=1)),
        Server(
            id = 148, 
            openstack_id = "test-server-id-148", 
            name = "test_server_148", 
            project = Project.objects.get(id=1)),
        Server(
            id = 149, 
            openstack_id = "test-server-id-149", 
            name = "test_server_149", 
            project = Project.objects.get(id=1)),
        Server(
            id = 150, 
            openstack_id = "test-server-id-150", 
            name = "test_server_150", 
            project = Project.objects.get(id=1)),
        Server(
            id = 151, 
            openstack_id = "test-server-id-151", 
            name = "test_server_151", 
            project = Project.objects.get(id=1)),
        Server(
            id = 152, 
            openstack_id = "test-server-id-152", 
            name = "test_server_152", 
            project = Project.objects.get(id=1)),
        Server(
            id = 153, 
            openstack_id = "test-server-id-153", 
            name = "test_server_153", 
            project = Project.objects.get(id=1)),
        Server(
            id = 154, 
            openstack_id = "test-server-id-154", 
            name = "test_server_154", 
            project = Project.objects.get(id=1)),
        Server(
            id = 155, 
            openstack_id = "test-server-id-155", 
            name = "test_server_155", 
            project = Project.objects.get(id=1)),
        Server(
            id = 156, 
            openstack_id = "test-server-id-156", 
            name = "test_server_156", 
            project = Project.objects.get(id=1)),
        Server(
            id = 157, 
            openstack_id = "test-server-id-157", 
            name = "test_server_157", 
            project = Project.objects.get(id=1)),
        Server(
            id = 158, 
            openstack_id = "test-server-id-158", 
            name = "test_server_158", 
            project = Project.objects.get(id=1)),
        Server(
            id = 159, 
            openstack_id = "test-server-id-159", 
            name = "test_server_159", 
            project = Project.objects.get(id=1)),
        Server(
            id = 160, 
            openstack_id = "test-server-id-160", 
            name = "test_server_160", 
            project = Project.objects.get(id=1)),
        Server(
            id = 161, 
            openstack_id = "test-server-id-161", 
            name = "test_server_161", 
            project = Project.objects.get(id=1)),
        Server(
            id = 162, 
            openstack_id = "test-server-id-162", 
            name = "test_server_162", 
            project = Project.objects.get(id=1)),
        Server(
            id = 163, 
            openstack_id = "test-server-id-163", 
            name = "test_server_163", 
            project = Project.objects.get(id=1)),
        Server(
            id = 164, 
            openstack_id = "test-server-id-164", 
            name = "test_server_164", 
            project = Project.objects.get(id=1)),
        Server(
            id = 165, 
            openstack_id = "test-server-id-165", 
            name = "test_server_165", 
            project = Project.objects.get(id=1)),
        Server(
            id = 166, 
            openstack_id = "test-server-id-166", 
            name = "test_server_166", 
            project = Project.objects.get(id=1)),
        Server(
            id = 167, 
            openstack_id = "test-server-id-167", 
            name = "test_server_167", 
            project = Project.objects.get(id=1)),
        Server(
            id = 168, 
            openstack_id = "test-server-id-168", 
            name = "test_server_168", 
            project = Project.objects.get(id=1)),
        Server(
            id = 169, 
            openstack_id = "test-server-id-169", 
            name = "test_server_169", 
            project = Project.objects.get(id=1)),
        Server(
            id = 170, 
            openstack_id = "test-server-id-170", 
            name = "test_server_170", 
            project = Project.objects.get(id=1)),
        Server(
            id = 171, 
            openstack_id = "test-server-id-171", 
            name = "test_server_171", 
            project = Project.objects.get(id=1)),
        Server(
            id = 172, 
            openstack_id = "test-server-id-172", 
            name = "test_server_172", 
            project = Project.objects.get(id=1)),
        Server(
            id = 173, 
            openstack_id = "test-server-id-173", 
            name = "test_server_173", 
            project = Project.objects.get(id=1)),
        Server(
            id = 174, 
            openstack_id = "test-server-id-174", 
            name = "test_server_174", 
            project = Project.objects.get(id=1)),
        ])
        ServerEvent.objects.bulk_create(
        [
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=1), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=1), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=2), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=2), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=3), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=3), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=4), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=4), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=5), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=5), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=6), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=6), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=7), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=7), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=7), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=8), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=8), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=8), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=8), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=9), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=9), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=9), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=10), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=10), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=10), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=11), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=11), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=11), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=11), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=12), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=12), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=12), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=12), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=12), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=12), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=13), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=13), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=13), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=14), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=14), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=14), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=14), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=15), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=15), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=15), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=16), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=16), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=16), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=17), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=17), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=17), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=18), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=18), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=18), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=18), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=19), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=19), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=19), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=19), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=19), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=20), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=20), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=20), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=20), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=20), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=20), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=20), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=20), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=21), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=21), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=21), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=22), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=22), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=22), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=23), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=23), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=23), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=23), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=24), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=24), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=24), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=24), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=24), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=24), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=25), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=25), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=25), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=26), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=26), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=26), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=26), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=27), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=27), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=27), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=28), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=28), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=28), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=28), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=29), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=29), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=29), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=29), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=29), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=30), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=30), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=30), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=30), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=30), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=30), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=31), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=31), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=31), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=31), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=32), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=32), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=32), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=32), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=32), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=33), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=33), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=33), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=33), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=33), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=33), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=34), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=34), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=34), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=34), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=35), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=35), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=35), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=35), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=35), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=36), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=36), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=36), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=36), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=36), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=36), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=37), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=37), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=37), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=37), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=38), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=38), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=38), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=38), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=38), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=38), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=39), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=39), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=39), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=39), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=39), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=39), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=39), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=39), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=40), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=40), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=40), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=41), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=41), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=41), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=41), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=41), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=42), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=42), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=42), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=42), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=43), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=43), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=43), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=43), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=43), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=44), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=44), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=44), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=44), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=44), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=44), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=45), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=45), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=45), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=46), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=46), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=46), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=46), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=47), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=47), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=47), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=47), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=47), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=48), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=48), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=48), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=48), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=48), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=49), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=49), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=49), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=49), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=49), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=49), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=49), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=49), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=50), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=50), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=50), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=50), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=50), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=50), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=50), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=51), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=51), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=51), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=51), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=51), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=51), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=51), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=51), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=51), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 12:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=51), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=52), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=52), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=52), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=52), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=52), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=52), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=52), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=52), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=52), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 12:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=52), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 13:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=52), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=53), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=53), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=53), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=53), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=54), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=54), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=54), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=54), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=54), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=55), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=55), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=55), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=55), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=55), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=55), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=55), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=56), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=56), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=56), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=56), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=57), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=57), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=57), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=57), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=57), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=58), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=58), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=58), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=58), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=58), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=58), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=59), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=59), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=59), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=59), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=59), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=59), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=59), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=59), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=60), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=60), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=60), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=60), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=60), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=60), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=60), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=60), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 11:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=60), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=61), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=61), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=61), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=62), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=62), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=62), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=62), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=62), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=63), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=63), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=63), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=64), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=64), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=64), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=64), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=64), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=64), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=64), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=65), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=65), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=65), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=66), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=66), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=66), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=66), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=66), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=67), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=67), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=67), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=67), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=67), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=68), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=68), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=68), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=68), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=68), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=68), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=68), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=68), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=69), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=69), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=69), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=69), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=70), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=70), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=70), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=70), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=70), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=70), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=71), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=71), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=71), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=71), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=71), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=71), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=71), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=71), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=72), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=72), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=72), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=72), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=73), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=73), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=73), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=73), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=73), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=73), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=74), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=74), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=74), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=75), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=75), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=75), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=75), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=76), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=76), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=76), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=76), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=76), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=77), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=77), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=77), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=78), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=78), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=78), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=78), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=78), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=79), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=79), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=79), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=79), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=80), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=80), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=80), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=80), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=80), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=81), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=81), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=81), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=81), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=81), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=81), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=81), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=82), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=82), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=82), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=82), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=83), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=83), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=83), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=83), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=83), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=84), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=84), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=84), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=85), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=85), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=85), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=85), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=86), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=86), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=86), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=86), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=87), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=87), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=87), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=87), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=88), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=88), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=88), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=88), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=88), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=89), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=89), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=89), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=89), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=89), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=90), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=90), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=90), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=90), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=90), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=91), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=91), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=91), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=91), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=91), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=91), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=92), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=92), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=92), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=92), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=92), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=92), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=92), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=92), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=93), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=93), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=93), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=93), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=93), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=94), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=94), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=94), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=94), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=94), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=95), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=95), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=95), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=95), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=95), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=96), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=96), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=96), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=96), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=96), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=96), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=97), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=97), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=97), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=97), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=97), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=97), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=97), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=98), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=98), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=98), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=98), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=98), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=98), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=98), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=98), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 11:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=98), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 12:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=98), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=99), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=99), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=99), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=99), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=99), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=100), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=100), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=100), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=100), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=100), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=101), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=101), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=101), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=101), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=101), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=101), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=102), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=102), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=102), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=102), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=102), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=102), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=102), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=102), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=103), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=103), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=103), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=103), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=103), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=104), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=104), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=104), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=104), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=104), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=105), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=105), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=105), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=105), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=105), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=106), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=106), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=106), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=106), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=106), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=106), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=107), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=107), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=107), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=107), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=107), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=107), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=107), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=108), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=108), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=108), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=108), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=108), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=108), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=108), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=108), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 11:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=108), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 12:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=108), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=109), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=109), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=109), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=109), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=110), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=110), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=110), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=110), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=111), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=111), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=111), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=111), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=111), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=112), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=112), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=112), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=112), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=112), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=112), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=112), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=113), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=113), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=113), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=113), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=113), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=114), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=114), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=114), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=114), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=114), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=115), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=115), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=115), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=115), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=115), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=115), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=116), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=116), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=116), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=116), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=116), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=116), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=116), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=116), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=117), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=117), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=117), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=117), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=118), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=118), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=118), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=118), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=118), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=119), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=119), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=119), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=119), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=119), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=120), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=120), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=120), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=120), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=120), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=121), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=121), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=121), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=121), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=121), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=122), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=122), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=122), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=122), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=122), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=123), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=123), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=123), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=123), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=123), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=123), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=124), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=124), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=124), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=124), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=124), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=124), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=124), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=125), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=125), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=125), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=125), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=125), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=125), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=125), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=125), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=126), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=126), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=126), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=126), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=126), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=126), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=126), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=126), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=126), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 12:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=126), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 13:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=126), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 14:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=126), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=127), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=127), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=127), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=127), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=127), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=128), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=128), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=128), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=128), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=128), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=129), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=129), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=129), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=129), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=129), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=130), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=130), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=130), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=130), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=130), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=130), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=131), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=131), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=131), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=131), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=131), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=131), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=131), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=132), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=132), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=132), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=132), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=132), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=132), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=132), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=132), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 11:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=132), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 12:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=132), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=133), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=133), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=133), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=133), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=133), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=134), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=134), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=134), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=134), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=134), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=135), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=135), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=135), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=135), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=135), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=135), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=136), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=136), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=136), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=136), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=136), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=136), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=137), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=137), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=137), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=137), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=137), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=137), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=138), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=138), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=138), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=138), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=138), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=138), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=138), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=138), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=139), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=139), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=139), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=139), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=140), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=140), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=140), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=140), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=140), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=141), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=141), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=141), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=141), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=142), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=142), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=142), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=142), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=143), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=143), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=143), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=143), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=143), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=144), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=144), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=144), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=144), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=144), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=144), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=144), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=145), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=145), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=145), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=145), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=146), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=146), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=146), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=146), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=147), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=147), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=147), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=147), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=148), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=148), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=148), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=148), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=148), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=149), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=149), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=149), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=149), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=149), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=149), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=150), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=150), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=150), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=150), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=150), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=150), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=150), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=150), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 11:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=150), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=151), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=151), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=151), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=151), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=151), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=152), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=152), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=152), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=152), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=152), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=153), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=153), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=153), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=153), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=153), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=154), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=154), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=154), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=154), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=154), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=154), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=155), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=155), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=155), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=155), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=155), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=155), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=155), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=156), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=156), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=156), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=156), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=156), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=156), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=156), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=156), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=156), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 11:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=156), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=157), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=157), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=157), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=157), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=157), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=158), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=158), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=158), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=158), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=158), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=159), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=159), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=159), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=159), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=159), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=160), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=160), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=160), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=160), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=160), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=160), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=161), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=161), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=161), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=161), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=161), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=161), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=161), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=162), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=162), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=162), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=162), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=162), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=162), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=162), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=162), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=162), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 11:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=162), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=163), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=163), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=163), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=163), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=164), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=164), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=164), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=164), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=164), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=165), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=165), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=165), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=165), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=166), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=166), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=166), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=166), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=167), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=167), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=167), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=167), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=167), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=168), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=168), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=168), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=168), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=168), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=168), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=168), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=169), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=169), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=169), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=169), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=169), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=170), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=170), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=170), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=170), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=170), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=171), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=171), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=171), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=171), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=171), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=171), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=172), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=172), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=172), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=172), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "unshelve", 
            server = Server.objects.get(id=172), 
            bill_state = True, 
            os_state = "unshelve",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=172), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=172), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=172), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=173), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=173), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=173), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=173), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            server = Server.objects.get(id=174), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=0),
            vcpu = 1,
            disk_gb = 0,
            ram_gb = 1),
        ServerEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=174), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=1),
            vcpu = 2,
            disk_gb = 0,
            ram_gb = 2),
        ServerEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            server = Server.objects.get(id=174), 
            bill_state = True, 
            os_state = "active",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "shelve_offload", 
            server = Server.objects.get(id=174), 
            bill_state = False, 
            os_state = "shelve_offload",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ServerEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            server = Server.objects.get(id=174), 
            bill_state = False, 
            os_state = "deleted",
            flavor = Flavor.objects.get(id=2),
            vcpu = 3,
            disk_gb = 10,
            ram_gb = 3),
        ])

    def test_calc_server1(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-1")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 1,
            'instance_name': 'test_server_1',
            'openstack_id': 'test-server-id-1',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server2(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-2")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 2,
            'instance_name': 'test_server_2',
            'openstack_id': 'test-server-id-2',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server3(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-3")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 3,
            'instance_name': 'test_server_3',
            'openstack_id': 'test-server-id-3',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server4(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-4")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 4,
            'instance_name': 'test_server_4',
            'openstack_id': 'test-server-id-4',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server5(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-5")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 5,
            'instance_name': 'test_server_5',
            'openstack_id': 'test-server-id-5',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server6(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-6")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 6,
            'instance_name': 'test_server_6',
            'openstack_id': 'test-server-id-6',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server7(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-7")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 7,
            'instance_name': 'test_server_7',
            'openstack_id': 'test-server-id-7',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server8(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-8")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 8,
            'instance_name': 'test_server_8',
            'openstack_id': 'test-server-id-8',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server9(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-9")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 9,
            'instance_name': 'test_server_9',
            'openstack_id': 'test-server-id-9',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0'], 'vcpu_h': 3.0, 'mem_h_gb': 3.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 0.0, 'compute_type': ['default', 'default'], 'vcpu': [1.0, 2.0], 'ram': [1.0, 2.0], 'disk': [0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server10(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-10")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 10,
            'instance_name': 'test_server_10',
            'openstack_id': 'test-server-id-10',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server11(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-11")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 11,
            'instance_name': 'test_server_11',
            'openstack_id': 'test-server-id-11',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0'], 'vcpu_h': 3.0, 'mem_h_gb': 3.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 0.0, 'compute_type': ['default', 'default'], 'vcpu': [1.0, 2.0], 'ram': [1.0, 2.0], 'disk': [0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server12(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-12")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 12,
            'instance_name': 'test_server_12',
            'openstack_id': 'test-server-id-12',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0', '3-3-10'], 'vcpu_h': 6.0, 'mem_h_gb': 6.0, 'run_time': 3.0, 'size_time': [1.0, 1.0, 1.0], 'disk_h_gb': 10.0, 'compute_type': ['default', 'default', 'default'], 'vcpu': [1.0, 2.0, 3.0], 'ram': [1.0, 2.0, 3.0], 'disk': [0.0, 0.0, 10.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server13(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-13")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 13,
            'instance_name': 'test_server_13',
            'openstack_id': 'test-server-id-13',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0'], 'vcpu_h': 3.0, 'mem_h_gb': 3.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 0.0, 'compute_type': ['default', 'default'], 'vcpu': [1.0, 2.0], 'ram': [1.0, 2.0], 'disk': [0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server14(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-14")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 14,
            'instance_name': 'test_server_14',
            'openstack_id': 'test-server-id-14',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0', '3-3-10'], 'vcpu_h': 6.0, 'mem_h_gb': 6.0, 'run_time': 3.0, 'size_time': [1.0, 1.0, 1.0], 'disk_h_gb': 10.0, 'compute_type': ['default', 'default', 'default'], 'vcpu': [1.0, 2.0, 3.0], 'ram': [1.0, 2.0, 3.0], 'disk': [0.0, 0.0, 10.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server15(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-15")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 15,
            'instance_name': 'test_server_15',
            'openstack_id': 'test-server-id-15',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [2.0], 'ram': [2.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server16(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-16")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 16,
            'instance_name': 'test_server_16',
            'openstack_id': 'test-server-id-16',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0'], 'vcpu_h': 3.0, 'mem_h_gb': 3.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 0.0, 'compute_type': ['default', 'default'], 'vcpu': [1.0, 2.0], 'ram': [1.0, 2.0], 'disk': [0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server17(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-17")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 17,
            'instance_name': 'test_server_17',
            'openstack_id': 'test-server-id-17',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server18(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-18")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 18,
            'instance_name': 'test_server_18',
            'openstack_id': 'test-server-id-18',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0', '3-3-10'], 'vcpu_h': 5.0, 'mem_h_gb': 5.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 10.0, 'compute_type': ['default', 'default'], 'vcpu': [2.0, 3.0], 'ram': [2.0, 3.0], 'disk': [0.0, 10.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server19(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-19")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 19,
            'instance_name': 'test_server_19',
            'openstack_id': 'test-server-id-19',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0', '3-3-10'], 'vcpu_h': 5.0, 'mem_h_gb': 5.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 10.0, 'compute_type': ['default', 'default'], 'vcpu': [2.0, 3.0], 'ram': [2.0, 3.0], 'disk': [0.0, 10.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server20(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-20")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 20,
            'instance_name': 'test_server_20',
            'openstack_id': 'test-server-id-20',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['3-3-10', '1-1-0', '2-2-0'], 'vcpu_h': 6.0, 'mem_h_gb': 6.0, 'run_time': 3.0, 'size_time': [1.0, 1.0, 1.0], 'disk_h_gb': 10.0, 'compute_type': ['default', 'default', 'default'], 'vcpu': [3.0, 1.0, 2.0], 'ram': [3.0, 1.0, 2.0], 'disk': [10.0, 0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server21(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-21")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 21,
            'instance_name': 'test_server_21',
            'openstack_id': 'test-server-id-21',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [2.0], 'ram': [2.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server22(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-22")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 22,
            'instance_name': 'test_server_22',
            'openstack_id': 'test-server-id-22',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0'], 'vcpu_h': 3.0, 'mem_h_gb': 3.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 0.0, 'compute_type': ['default', 'default'], 'vcpu': [1.0, 2.0], 'ram': [1.0, 2.0], 'disk': [0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server23(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-23")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 23,
            'instance_name': 'test_server_23',
            'openstack_id': 'test-server-id-23',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0', '3-3-10'], 'vcpu_h': 5.0, 'mem_h_gb': 5.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 10.0, 'compute_type': ['default', 'default'], 'vcpu': [2.0, 3.0], 'ram': [2.0, 3.0], 'disk': [0.0, 10.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server24(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 08:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-24")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 24,
            'instance_name': 'test_server_24',
            'openstack_id': 'test-server-id-24',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['3-3-10', '1-1-0', '2-2-0'], 'vcpu_h': 6.0, 'mem_h_gb': 6.0, 'run_time': 3.0, 'size_time': [1.0, 1.0, 1.0], 'disk_h_gb': 10.0, 'compute_type': ['default', 'default', 'default'], 'vcpu': [3.0, 1.0, 2.0], 'ram': [3.0, 1.0, 2.0], 'disk': [10.0, 0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server25(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-25")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 25,
            'instance_name': 'test_server_25',
            'openstack_id': 'test-server-id-25',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [2.0], 'ram': [2.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server26(self):
        start_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-26")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 26,
            'instance_name': 'test_server_26',
            'openstack_id': 'test-server-id-26',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['3-3-10'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [3.0], 'ram': [3.0], 'disk': [10.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server27(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-27")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 27,
            'instance_name': 'test_server_27',
            'openstack_id': 'test-server-id-27',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server28(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-28")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 28,
            'instance_name': 'test_server_28',
            'openstack_id': 'test-server-id-28',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server29(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-29")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 29,
            'instance_name': 'test_server_29',
            'openstack_id': 'test-server-id-29',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server30(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-30")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 30,
            'instance_name': 'test_server_30',
            'openstack_id': 'test-server-id-30',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server31(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-31")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 31,
            'instance_name': 'test_server_31',
            'openstack_id': 'test-server-id-31',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [2.0], 'run_time': 2.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server32(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-32")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 32,
            'instance_name': 'test_server_32',
            'openstack_id': 'test-server-id-32',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [2.0], 'run_time': 2.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server33(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-33")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 33,
            'instance_name': 'test_server_33',
            'openstack_id': 'test-server-id-33',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [2.0], 'run_time': 2.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server34(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-34")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 34,
            'instance_name': 'test_server_34',
            'openstack_id': 'test-server-id-34',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server35(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-35")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 35,
            'instance_name': 'test_server_35',
            'openstack_id': 'test-server-id-35',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [2.0], 'run_time': 2.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server36(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-36")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 36,
            'instance_name': 'test_server_36',
            'openstack_id': 'test-server-id-36',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [2.0], 'run_time': 2.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server37(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-37")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 37,
            'instance_name': 'test_server_37',
            'openstack_id': 'test-server-id-37',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server38(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-38")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 38,
            'instance_name': 'test_server_38',
            'openstack_id': 'test-server-id-38',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [2.0], 'run_time': 2.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server39(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-39")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 39,
            'instance_name': 'test_server_39',
            'openstack_id': 'test-server-id-39',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 3.0, 'mem_h_gb': 3.0, 'size_time': [3.0], 'run_time': 3.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server40(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-40")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 40,
            'instance_name': 'test_server_40',
            'openstack_id': 'test-server-id-40',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server41(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-41")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 41,
            'instance_name': 'test_server_41',
            'openstack_id': 'test-server-id-41',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [2.0], 'run_time': 2.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server42(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-42")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 42,
            'instance_name': 'test_server_42',
            'openstack_id': 'test-server-id-42',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [2.0], 'run_time': 2.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server43(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-43")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 43,
            'instance_name': 'test_server_43',
            'openstack_id': 'test-server-id-43',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [2.0], 'run_time': 2.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server44(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 08:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-44")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 44,
            'instance_name': 'test_server_44',
            'openstack_id': 'test-server-id-44',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 3.0, 'mem_h_gb': 3.0, 'size_time': [3.0], 'run_time': 3.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server45(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-45")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 45,
            'instance_name': 'test_server_45',
            'openstack_id': 'test-server-id-45',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server46(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-46")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 46,
            'instance_name': 'test_server_46',
            'openstack_id': 'test-server-id-46',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server47(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-47")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 47,
            'instance_name': 'test_server_47',
            'openstack_id': 'test-server-id-47',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server48(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-48")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 48,
            'instance_name': 'test_server_48',
            'openstack_id': 'test-server-id-48',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server49(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-49")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 49,
            'instance_name': 'test_server_49',
            'openstack_id': 'test-server-id-49',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [2.0], 'run_time': 2.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server50(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-50")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 50,
            'instance_name': 'test_server_50',
            'openstack_id': 'test-server-id-50',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [2.0], 'run_time': 2.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server51(self):
        start_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 11:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-51")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 51,
            'instance_name': 'test_server_51',
            'openstack_id': 'test-server-id-51',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 3.0, 'mem_h_gb': 3.0, 'size_time': [3.0], 'run_time': 3.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server52(self):
        start_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 11:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-52")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 52,
            'instance_name': 'test_server_52',
            'openstack_id': 'test-server-id-52',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 3.0, 'mem_h_gb': 3.0, 'size_time': [3.0], 'run_time': 3.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server53(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-53")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 53,
            'instance_name': 'test_server_53',
            'openstack_id': 'test-server-id-53',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server54(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-54")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 54,
            'instance_name': 'test_server_54',
            'openstack_id': 'test-server-id-54',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server55(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-55")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 55,
            'instance_name': 'test_server_55',
            'openstack_id': 'test-server-id-55',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [2.0], 'run_time': 2.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server56(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-56")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 56,
            'instance_name': 'test_server_56',
            'openstack_id': 'test-server-id-56',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server57(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-57")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 57,
            'instance_name': 'test_server_57',
            'openstack_id': 'test-server-id-57',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server58(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-58")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 58,
            'instance_name': 'test_server_58',
            'openstack_id': 'test-server-id-58',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server59(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-59")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 59,
            'instance_name': 'test_server_59',
            'openstack_id': 'test-server-id-59',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [2.0], 'run_time': 2.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server60(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 09:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-60")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 60,
            'instance_name': 'test_server_60',
            'openstack_id': 'test-server-id-60',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 3.0, 'mem_h_gb': 3.0, 'size_time': [3.0], 'run_time': 3.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server61(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-61")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 61,
            'instance_name': 'test_server_61',
            'openstack_id': 'test-server-id-61',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server62(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-62")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 62,
            'instance_name': 'test_server_62',
            'openstack_id': 'test-server-id-62',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server63(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-63")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 63,
            'instance_name': 'test_server_63',
            'openstack_id': 'test-server-id-63',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server64(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-64")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 64,
            'instance_name': 'test_server_64',
            'openstack_id': 'test-server-id-64',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [2.0], 'run_time': 2.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server65(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-65")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 65,
            'instance_name': 'test_server_65',
            'openstack_id': 'test-server-id-65',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server66(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-66")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 66,
            'instance_name': 'test_server_66',
            'openstack_id': 'test-server-id-66',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server67(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-67")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 67,
            'instance_name': 'test_server_67',
            'openstack_id': 'test-server-id-67',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [2.0], 'run_time': 2.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server68(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-68")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 68,
            'instance_name': 'test_server_68',
            'openstack_id': 'test-server-id-68',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [2.0], 'run_time': 2.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server69(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-69")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 69,
            'instance_name': 'test_server_69',
            'openstack_id': 'test-server-id-69',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server70(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 08:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-70")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 70,
            'instance_name': 'test_server_70',
            'openstack_id': 'test-server-id-70',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [2.0], 'run_time': 2.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server71(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 10:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-71")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 71,
            'instance_name': 'test_server_71',
            'openstack_id': 'test-server-id-71',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 3.0, 'mem_h_gb': 3.0, 'size_time': [3.0], 'run_time': 3.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server72(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-72")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 72,
            'instance_name': 'test_server_72',
            'openstack_id': 'test-server-id-72',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [2.0], 'run_time': 2.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server73(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 08:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-73")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 73,
            'instance_name': 'test_server_73',
            'openstack_id': 'test-server-id-73',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [2.0], 'run_time': 2.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server74(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-74")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 74,
            'instance_name': 'test_server_74',
            'openstack_id': 'test-server-id-74',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server75(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-75")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 75,
            'instance_name': 'test_server_75',
            'openstack_id': 'test-server-id-75',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server76(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-76")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 76,
            'instance_name': 'test_server_76',
            'openstack_id': 'test-server-id-76',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server77(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-77")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 77,
            'instance_name': 'test_server_77',
            'openstack_id': 'test-server-id-77',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server78(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-78")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 78,
            'instance_name': 'test_server_78',
            'openstack_id': 'test-server-id-78',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [2.0], 'run_time': 2.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server79(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-79")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 79,
            'instance_name': 'test_server_79',
            'openstack_id': 'test-server-id-79',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server80(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-80")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 80,
            'instance_name': 'test_server_80',
            'openstack_id': 'test-server-id-80',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server81(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 09:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-81")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 81,
            'instance_name': 'test_server_81',
            'openstack_id': 'test-server-id-81',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [2.0], 'run_time': 2.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server82(self):
        start_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-82")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 82,
            'instance_name': 'test_server_82',
            'openstack_id': 'test-server-id-82',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server83(self):
        start_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-83")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 83,
            'instance_name': 'test_server_83',
            'openstack_id': 'test-server-id-83',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server84(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-84")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 84,
            'instance_name': 'test_server_84',
            'openstack_id': 'test-server-id-84',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server85(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-85")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 85,
            'instance_name': 'test_server_85',
            'openstack_id': 'test-server-id-85',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server86(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-86")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 86,
            'instance_name': 'test_server_86',
            'openstack_id': 'test-server-id-86',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server87(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-87")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 87,
            'instance_name': 'test_server_87',
            'openstack_id': 'test-server-id-87',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server88(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-88")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 88,
            'instance_name': 'test_server_88',
            'openstack_id': 'test-server-id-88',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server89(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-89")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 89,
            'instance_name': 'test_server_89',
            'openstack_id': 'test-server-id-89',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server90(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-90")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 90,
            'instance_name': 'test_server_90',
            'openstack_id': 'test-server-id-90',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server91(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-91")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 91,
            'instance_name': 'test_server_91',
            'openstack_id': 'test-server-id-91',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server92(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-92")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 92,
            'instance_name': 'test_server_92',
            'openstack_id': 'test-server-id-92',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server93(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-93")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 93,
            'instance_name': 'test_server_93',
            'openstack_id': 'test-server-id-93',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0'], 'vcpu_h': 5.0, 'mem_h_gb': 5.0, 'run_time': 3.0, 'size_time': [1.0, 2.0], 'disk_h_gb': 0.0, 'compute_type': ['default', 'default'], 'vcpu': [1.0, 2.0], 'ram': [1.0, 2.0], 'disk': [0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server94(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-94")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 94,
            'instance_name': 'test_server_94',
            'openstack_id': 'test-server-id-94',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0'], 'vcpu_h': 4.0, 'mem_h_gb': 4.0, 'run_time': 3.0, 'size_time': [2.0, 1.0], 'disk_h_gb': 0.0, 'compute_type': ['default', 'default'], 'vcpu': [1.0, 2.0], 'ram': [1.0, 2.0], 'disk': [0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server95(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-95")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 95,
            'instance_name': 'test_server_95',
            'openstack_id': 'test-server-id-95',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [2.0], 'run_time': 2.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server96(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-96")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 96,
            'instance_name': 'test_server_96',
            'openstack_id': 'test-server-id-96',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0', '3-3-10'], 'vcpu_h': 8.0, 'mem_h_gb': 8.0, 'run_time': 4.0, 'size_time': [1.0, 2.0, 1.0], 'disk_h_gb': 10.0, 'compute_type': ['default', 'default', 'default'], 'vcpu': [1.0, 2.0, 3.0], 'ram': [1.0, 2.0, 3.0], 'disk': [0.0, 0.0, 10.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server97(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-97")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 97,
            'instance_name': 'test_server_97',
            'openstack_id': 'test-server-id-97',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0', '3-3-10'], 'vcpu_h': 8.0, 'mem_h_gb': 8.0, 'run_time': 4.0, 'size_time': [1.0, 2.0, 1.0], 'disk_h_gb': 10.0, 'compute_type': ['default', 'default', 'default'], 'vcpu': [1.0, 2.0, 3.0], 'ram': [1.0, 2.0, 3.0], 'disk': [0.0, 0.0, 10.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server98(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 09:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-98")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 98,
            'instance_name': 'test_server_98',
            'openstack_id': 'test-server-id-98',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0', '3-3-10', '1-1-0', '2-2-0'], 'vcpu_h': 12.0, 'mem_h_gb': 12.0, 'run_time': 6.0, 'size_time': [1.0, 1.0, 2.0, 1.0, 1.0], 'disk_h_gb': 20.0, 'compute_type': ['default', 'default', 'default', 'default', 'default'], 'vcpu': [1.0, 2.0, 3.0, 1.0, 2.0], 'ram': [1.0, 2.0, 3.0, 1.0, 2.0], 'disk': [0.0, 0.0, 10.0, 0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server99(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-99")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 99,
            'instance_name': 'test_server_99',
            'openstack_id': 'test-server-id-99',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0'], 'vcpu_h': 3.0, 'mem_h_gb': 3.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 0.0, 'compute_type': ['default', 'default'], 'vcpu': [1.0, 2.0], 'ram': [1.0, 2.0], 'disk': [0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server100(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-100")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 100,
            'instance_name': 'test_server_100',
            'openstack_id': 'test-server-id-100',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server101(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-101")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 101,
            'instance_name': 'test_server_101',
            'openstack_id': 'test-server-id-101',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0'], 'vcpu_h': 3.0, 'mem_h_gb': 3.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 0.0, 'compute_type': ['default', 'default'], 'vcpu': [1.0, 2.0], 'ram': [1.0, 2.0], 'disk': [0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server102(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-102")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 102,
            'instance_name': 'test_server_102',
            'openstack_id': 'test-server-id-102',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0', '3-3-10'], 'vcpu_h': 6.0, 'mem_h_gb': 6.0, 'run_time': 3.0, 'size_time': [1.0, 1.0, 1.0], 'disk_h_gb': 10.0, 'compute_type': ['default', 'default', 'default'], 'vcpu': [1.0, 2.0, 3.0], 'ram': [1.0, 2.0, 3.0], 'disk': [0.0, 0.0, 10.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server103(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-103")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 103,
            'instance_name': 'test_server_103',
            'openstack_id': 'test-server-id-103',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0'], 'vcpu_h': 3.0, 'mem_h_gb': 3.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 0.0, 'compute_type': ['default', 'default'], 'vcpu': [1.0, 2.0], 'ram': [1.0, 2.0], 'disk': [0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server104(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-104")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 104,
            'instance_name': 'test_server_104',
            'openstack_id': 'test-server-id-104',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server105(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-105")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 105,
            'instance_name': 'test_server_105',
            'openstack_id': 'test-server-id-105',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server106(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-106")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 106,
            'instance_name': 'test_server_106',
            'openstack_id': 'test-server-id-106',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0'], 'vcpu_h': 3.0, 'mem_h_gb': 3.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 0.0, 'compute_type': ['default', 'default'], 'vcpu': [1.0, 2.0], 'ram': [1.0, 2.0], 'disk': [0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server107(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-107")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 107,
            'instance_name': 'test_server_107',
            'openstack_id': 'test-server-id-107',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0'], 'vcpu_h': 3.0, 'mem_h_gb': 3.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 0.0, 'compute_type': ['default', 'default'], 'vcpu': [1.0, 2.0], 'ram': [1.0, 2.0], 'disk': [0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server108(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-108")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 108,
            'instance_name': 'test_server_108',
            'openstack_id': 'test-server-id-108',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0', '3-3-10'], 'vcpu_h': 6.0, 'mem_h_gb': 6.0, 'run_time': 3.0, 'size_time': [1.0, 1.0, 1.0], 'disk_h_gb': 10.0, 'compute_type': ['default', 'default', 'default'], 'vcpu': [1.0, 2.0, 3.0], 'ram': [1.0, 2.0, 3.0], 'disk': [0.0, 0.0, 10.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server109(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-109")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 109,
            'instance_name': 'test_server_109',
            'openstack_id': 'test-server-id-109',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0'], 'vcpu_h': 3.0, 'mem_h_gb': 3.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 0.0, 'compute_type': ['default', 'default'], 'vcpu': [1.0, 2.0], 'ram': [1.0, 2.0], 'disk': [0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server110(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-110")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 110,
            'instance_name': 'test_server_110',
            'openstack_id': 'test-server-id-110',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server111(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-111")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 111,
            'instance_name': 'test_server_111',
            'openstack_id': 'test-server-id-111',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0'], 'vcpu_h': 3.0, 'mem_h_gb': 3.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 0.0, 'compute_type': ['default', 'default'], 'vcpu': [1.0, 2.0], 'ram': [1.0, 2.0], 'disk': [0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server112(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-112")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 112,
            'instance_name': 'test_server_112',
            'openstack_id': 'test-server-id-112',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0', '3-3-10'], 'vcpu_h': 6.0, 'mem_h_gb': 6.0, 'run_time': 3.0, 'size_time': [1.0, 1.0, 1.0], 'disk_h_gb': 10.0, 'compute_type': ['default', 'default', 'default'], 'vcpu': [1.0, 2.0, 3.0], 'ram': [1.0, 2.0, 3.0], 'disk': [0.0, 0.0, 10.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server113(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-113")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 113,
            'instance_name': 'test_server_113',
            'openstack_id': 'test-server-id-113',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0'], 'vcpu_h': 5.0, 'mem_h_gb': 5.0, 'run_time': 3.0, 'size_time': [1.0, 2.0], 'disk_h_gb': 0.0, 'compute_type': ['default', 'default'], 'vcpu': [1.0, 2.0], 'ram': [1.0, 2.0], 'disk': [0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server114(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-114")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 114,
            'instance_name': 'test_server_114',
            'openstack_id': 'test-server-id-114',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0'], 'vcpu_h': 4.0, 'mem_h_gb': 4.0, 'run_time': 3.0, 'size_time': [2.0, 1.0], 'disk_h_gb': 0.0, 'compute_type': ['default', 'default'], 'vcpu': [1.0, 2.0], 'ram': [1.0, 2.0], 'disk': [0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server115(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 08:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-115")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 115,
            'instance_name': 'test_server_115',
            'openstack_id': 'test-server-id-115',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0', '3-3-10'], 'vcpu_h': 8.0, 'mem_h_gb': 8.0, 'run_time': 4.0, 'size_time': [1.0, 2.0, 1.0], 'disk_h_gb': 10.0, 'compute_type': ['default', 'default', 'default'], 'vcpu': [1.0, 2.0, 3.0], 'ram': [1.0, 2.0, 3.0], 'disk': [0.0, 0.0, 10.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server116(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 10:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-116")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 116,
            'instance_name': 'test_server_116',
            'openstack_id': 'test-server-id-116',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0', '3-3-10', '1-1-0', '2-2-0'], 'vcpu_h': 12.0, 'mem_h_gb': 12.0, 'run_time': 6.0, 'size_time': [1.0, 1.0, 2.0, 1.0, 1.0], 'disk_h_gb': 20.0, 'compute_type': ['default', 'default', 'default', 'default', 'default'], 'vcpu': [1.0, 2.0, 3.0, 1.0, 2.0], 'ram': [1.0, 2.0, 3.0, 1.0, 2.0], 'disk': [0.0, 0.0, 10.0, 0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server117(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-117")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 117,
            'instance_name': 'test_server_117',
            'openstack_id': 'test-server-id-117',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0'], 'vcpu_h': 3.0, 'mem_h_gb': 3.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 0.0, 'compute_type': ['default', 'default'], 'vcpu': [1.0, 2.0], 'ram': [1.0, 2.0], 'disk': [0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server118(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-118")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 118,
            'instance_name': 'test_server_118',
            'openstack_id': 'test-server-id-118',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0', '3-3-10'], 'vcpu_h': 6.0, 'mem_h_gb': 6.0, 'run_time': 3.0, 'size_time': [1.0, 1.0, 1.0], 'disk_h_gb': 10.0, 'compute_type': ['default', 'default', 'default'], 'vcpu': [1.0, 2.0, 3.0], 'ram': [1.0, 2.0, 3.0], 'disk': [0.0, 0.0, 10.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server119(self):
        start_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-119")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 119,
            'instance_name': 'test_server_119',
            'openstack_id': 'test-server-id-119',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [2.0], 'ram': [2.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server120(self):
        start_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-120")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 120,
            'instance_name': 'test_server_120',
            'openstack_id': 'test-server-id-120',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [2.0], 'ram': [2.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server121(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-121")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 121,
            'instance_name': 'test_server_121',
            'openstack_id': 'test-server-id-121',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0'], 'vcpu_h': 3.0, 'mem_h_gb': 3.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 0.0, 'compute_type': ['default', 'default'], 'vcpu': [1.0, 2.0], 'ram': [1.0, 2.0], 'disk': [0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server122(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-122")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 122,
            'instance_name': 'test_server_122',
            'openstack_id': 'test-server-id-122',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server123(self):
        start_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-123")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 123,
            'instance_name': 'test_server_123',
            'openstack_id': 'test-server-id-123',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['3-3-10'], 'vcpu_h': 3.0, 'mem_h_gb': 3.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 10, 'compute_type': ['default'], 'vcpu': [3.0], 'ram': [3.0], 'disk': [10.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server124(self):
        start_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 08:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-124")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 124,
            'instance_name': 'test_server_124',
            'openstack_id': 'test-server-id-124',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['3-3-10', '1-1-0'], 'vcpu_h': 4.0, 'mem_h_gb': 4.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 10.0, 'compute_type': ['default', 'default'], 'vcpu': [3.0, 1.0], 'ram': [3.0, 1.0], 'disk': [10.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server125(self):
        start_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 08:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-125")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 125,
            'instance_name': 'test_server_125',
            'openstack_id': 'test-server-id-125',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['3-3-10', '1-1-0'], 'vcpu_h': 4.0, 'mem_h_gb': 4.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 10.0, 'compute_type': ['default', 'default'], 'vcpu': [3.0, 1.0], 'ram': [3.0, 1.0], 'disk': [10.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server126(self):
        start_time = datetime.datetime.strptime("2023-01-11 08:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 11:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-126")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 126,
            'instance_name': 'test_server_126',
            'openstack_id': 'test-server-id-126',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0', '3-3-10', '1-1-0'], 'vcpu_h': 6.0, 'mem_h_gb': 6.0, 'run_time': 3.0, 'size_time': [1.0, 1.0, 1.0], 'disk_h_gb': 10.0, 'compute_type': ['default', 'default', 'default'], 'vcpu': [2.0, 3.0, 1.0], 'ram': [2.0, 3.0, 1.0], 'disk': [0.0, 10.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server127(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-127")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 127,
            'instance_name': 'test_server_127',
            'openstack_id': 'test-server-id-127',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [2.0], 'ram': [2.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server128(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-128")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 128,
            'instance_name': 'test_server_128',
            'openstack_id': 'test-server-id-128',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0'], 'vcpu_h': 3.0, 'mem_h_gb': 3.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 0.0, 'compute_type': ['default', 'default'], 'vcpu': [1.0, 2.0], 'ram': [1.0, 2.0], 'disk': [0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server129(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-129")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 129,
            'instance_name': 'test_server_129',
            'openstack_id': 'test-server-id-129',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server130(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-130")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 130,
            'instance_name': 'test_server_130',
            'openstack_id': 'test-server-id-130',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0', '3-3-10'], 'vcpu_h': 5.0, 'mem_h_gb': 5.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 10.0, 'compute_type': ['default', 'default'], 'vcpu': [2.0, 3.0], 'ram': [2.0, 3.0], 'disk': [0.0, 10.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server131(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-131")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 131,
            'instance_name': 'test_server_131',
            'openstack_id': 'test-server-id-131',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0', '3-3-10'], 'vcpu_h': 5.0, 'mem_h_gb': 5.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 10.0, 'compute_type': ['default', 'default'], 'vcpu': [2.0, 3.0], 'ram': [2.0, 3.0], 'disk': [0.0, 10.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server132(self):
        start_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 09:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-132")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 132,
            'instance_name': 'test_server_132',
            'openstack_id': 'test-server-id-132',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['3-3-10', '1-1-0', '2-2-0'], 'vcpu_h': 6.0, 'mem_h_gb': 6.0, 'run_time': 3.0, 'size_time': [1.0, 1.0, 1.0], 'disk_h_gb': 10.0, 'compute_type': ['default', 'default', 'default'], 'vcpu': [3.0, 1.0, 2.0], 'ram': [3.0, 1.0, 2.0], 'disk': [10.0, 0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server133(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-133")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 133,
            'instance_name': 'test_server_133',
            'openstack_id': 'test-server-id-133',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [2.0], 'ram': [2.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server134(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-134")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 134,
            'instance_name': 'test_server_134',
            'openstack_id': 'test-server-id-134',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server135(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-135")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 135,
            'instance_name': 'test_server_135',
            'openstack_id': 'test-server-id-135',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [2.0], 'ram': [2.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server136(self):
        start_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-136")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 136,
            'instance_name': 'test_server_136',
            'openstack_id': 'test-server-id-136',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['3-3-10'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [3.0], 'ram': [3.0], 'disk': [10.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server137(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-137")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 137,
            'instance_name': 'test_server_137',
            'openstack_id': 'test-server-id-137',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server138(self):
        start_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-138")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 138,
            'instance_name': 'test_server_138',
            'openstack_id': 'test-server-id-138',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['3-3-10'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [3.0], 'ram': [3.0], 'disk': [10.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server139(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-139")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 139,
            'instance_name': 'test_server_139',
            'openstack_id': 'test-server-id-139',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [2.0], 'ram': [2.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server140(self):
        start_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-140")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 140,
            'instance_name': 'test_server_140',
            'openstack_id': 'test-server-id-140',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['3-3-10'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [3.0], 'ram': [3.0], 'disk': [10.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server141(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-141")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 141,
            'instance_name': 'test_server_141',
            'openstack_id': 'test-server-id-141',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [2.0], 'ram': [2.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server142(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-142")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 142,
            'instance_name': 'test_server_142',
            'openstack_id': 'test-server-id-142',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0'], 'vcpu_h': 3.0, 'mem_h_gb': 3.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 0.0, 'compute_type': ['default', 'default'], 'vcpu': [1.0, 2.0], 'ram': [1.0, 2.0], 'disk': [0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server143(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-143")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 143,
            'instance_name': 'test_server_143',
            'openstack_id': 'test-server-id-143',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0', '3-3-10'], 'vcpu_h': 5.0, 'mem_h_gb': 5.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 10.0, 'compute_type': ['default', 'default'], 'vcpu': [2.0, 3.0], 'ram': [2.0, 3.0], 'disk': [0.0, 10.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server144(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 08:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-144")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 144,
            'instance_name': 'test_server_144',
            'openstack_id': 'test-server-id-144',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['3-3-10', '1-1-0', '2-2-0'], 'vcpu_h': 6.0, 'mem_h_gb': 6.0, 'run_time': 3.0, 'size_time': [1.0, 1.0, 1.0], 'disk_h_gb': 10.0, 'compute_type': ['default', 'default', 'default'], 'vcpu': [3.0, 1.0, 2.0], 'ram': [3.0, 1.0, 2.0], 'disk': [10.0, 0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server145(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-145")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 145,
            'instance_name': 'test_server_145',
            'openstack_id': 'test-server-id-145',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [2.0], 'ram': [2.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server146(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-146")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 146,
            'instance_name': 'test_server_146',
            'openstack_id': 'test-server-id-146',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0'], 'vcpu_h': 3.0, 'mem_h_gb': 3.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 0.0, 'compute_type': ['default', 'default'], 'vcpu': [1.0, 2.0], 'ram': [1.0, 2.0], 'disk': [0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server147(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-147")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 147,
            'instance_name': 'test_server_147',
            'openstack_id': 'test-server-id-147',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0'], 'vcpu_h': 1.0, 'mem_h_gb': 1.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [1.0], 'ram': [1.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server148(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-148")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 148,
            'instance_name': 'test_server_148',
            'openstack_id': 'test-server-id-148',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0', '3-3-10'], 'vcpu_h': 5.0, 'mem_h_gb': 5.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 10.0, 'compute_type': ['default', 'default'], 'vcpu': [2.0, 3.0], 'ram': [2.0, 3.0], 'disk': [0.0, 10.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server149(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-149")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 149,
            'instance_name': 'test_server_149',
            'openstack_id': 'test-server-id-149',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0', '3-3-10'], 'vcpu_h': 5.0, 'mem_h_gb': 5.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 10.0, 'compute_type': ['default', 'default'], 'vcpu': [2.0, 3.0], 'ram': [2.0, 3.0], 'disk': [0.0, 10.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server150(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-150")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 150,
            'instance_name': 'test_server_150',
            'openstack_id': 'test-server-id-150',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['3-3-10', '1-1-0', '2-2-0'], 'vcpu_h': 6.0, 'mem_h_gb': 6.0, 'run_time': 3.0, 'size_time': [1.0, 1.0, 1.0], 'disk_h_gb': 10.0, 'compute_type': ['default', 'default', 'default'], 'vcpu': [3.0, 1.0, 2.0], 'ram': [3.0, 1.0, 2.0], 'disk': [10.0, 0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server151(self):
        start_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-151")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 151,
            'instance_name': 'test_server_151',
            'openstack_id': 'test-server-id-151',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [2.0], 'ram': [2.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server152(self):
        start_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-152")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 152,
            'instance_name': 'test_server_152',
            'openstack_id': 'test-server-id-152',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [2.0], 'ram': [2.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server153(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-153")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 153,
            'instance_name': 'test_server_153',
            'openstack_id': 'test-server-id-153',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0'], 'vcpu_h': 3.0, 'mem_h_gb': 3.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 0.0, 'compute_type': ['default', 'default'], 'vcpu': [1.0, 2.0], 'ram': [1.0, 2.0], 'disk': [0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server154(self):
        start_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 08:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-154")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 154,
            'instance_name': 'test_server_154',
            'openstack_id': 'test-server-id-154',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['3-3-10'], 'vcpu_h': 3.0, 'mem_h_gb': 3.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 10, 'compute_type': ['default'], 'vcpu': [3.0], 'ram': [3.0], 'disk': [10.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server155(self):
        start_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 09:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-155")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 155,
            'instance_name': 'test_server_155',
            'openstack_id': 'test-server-id-155',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['3-3-10', '1-1-0'], 'vcpu_h': 4.0, 'mem_h_gb': 4.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 10.0, 'compute_type': ['default', 'default'], 'vcpu': [3.0, 1.0], 'ram': [3.0, 1.0], 'disk': [10.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server156(self):
        start_time = datetime.datetime.strptime("2023-01-11 08:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 12:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-156")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 156,
            'instance_name': 'test_server_156',
            'openstack_id': 'test-server-id-156',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0', '3-3-10', '1-1-0'], 'vcpu_h': 6.0, 'mem_h_gb': 6.0, 'run_time': 3.0, 'size_time': [1.0, 1.0, 1.0], 'disk_h_gb': 10.0, 'compute_type': ['default', 'default', 'default'], 'vcpu': [2.0, 3.0, 1.0], 'ram': [2.0, 3.0, 1.0], 'disk': [0.0, 10.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server157(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-157")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 157,
            'instance_name': 'test_server_157',
            'openstack_id': 'test-server-id-157',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0'], 'vcpu_h': 4.0, 'mem_h_gb': 4.0, 'size_time': [2.0], 'run_time': 2.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [2.0], 'ram': [2.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server158(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-158")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 158,
            'instance_name': 'test_server_158',
            'openstack_id': 'test-server-id-158',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0'], 'vcpu_h': 5.0, 'mem_h_gb': 5.0, 'run_time': 3.0, 'size_time': [1.0, 2.0], 'disk_h_gb': 0.0, 'compute_type': ['default', 'default'], 'vcpu': [1.0, 2.0], 'ram': [1.0, 2.0], 'disk': [0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server159(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-159")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 159,
            'instance_name': 'test_server_159',
            'openstack_id': 'test-server-id-159',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0'], 'vcpu_h': 4.0, 'mem_h_gb': 4.0, 'run_time': 3.0, 'size_time': [2.0, 1.0], 'disk_h_gb': 0.0, 'compute_type': ['default', 'default'], 'vcpu': [1.0, 2.0], 'ram': [1.0, 2.0], 'disk': [0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server160(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 08:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-160")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 160,
            'instance_name': 'test_server_160',
            'openstack_id': 'test-server-id-160',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0', '3-3-10'], 'vcpu_h': 8.0, 'mem_h_gb': 8.0, 'run_time': 3.0, 'size_time': [1.0, 2.0], 'disk_h_gb': 20.0, 'compute_type': ['default', 'default'], 'vcpu': [2.0, 3.0], 'ram': [2.0, 3.0], 'disk': [0.0, 10.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server161(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 09:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-161")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 161,
            'instance_name': 'test_server_161',
            'openstack_id': 'test-server-id-161',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0', '3-3-10', '1-1-0'], 'vcpu_h': 9.0, 'mem_h_gb': 9.0, 'run_time': 4.0, 'size_time': [1.0, 2.0, 1.0], 'disk_h_gb': 20.0, 'compute_type': ['default', 'default', 'default'], 'vcpu': [2.0, 3.0, 1.0], 'ram': [2.0, 3.0, 1.0], 'disk': [0.0, 10.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server162(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 12:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-162")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 162,
            'instance_name': 'test_server_162',
            'openstack_id': 'test-server-id-162',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['3-3-10', '1-1-0', '2-2-0', '3-3-10', '1-1-0'], 'vcpu_h': 12.0, 'mem_h_gb': 12.0, 'run_time': 6.0, 'size_time': [1.0, 1.0, 2.0, 1.0, 1.0], 'disk_h_gb': 20.0, 'compute_type': ['default', 'default', 'default', 'default', 'default'], 'vcpu': [3.0, 1.0, 2.0, 3.0, 1.0], 'ram': [3.0, 1.0, 2.0, 3.0, 1.0], 'disk': [10.0, 0.0, 0.0, 10.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server163(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-163")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 163,
            'instance_name': 'test_server_163',
            'openstack_id': 'test-server-id-163',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [2.0], 'ram': [2.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server164(self):
        start_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-164")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 164,
            'instance_name': 'test_server_164',
            'openstack_id': 'test-server-id-164',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['3-3-10'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [3.0], 'ram': [3.0], 'disk': [10.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server165(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-165")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 165,
            'instance_name': 'test_server_165',
            'openstack_id': 'test-server-id-165',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0'], 'vcpu_h': 2.0, 'mem_h_gb': 2.0, 'size_time': [1.0], 'run_time': 1.0, 'disk_h_gb': 0, 'compute_type': ['default'], 'vcpu': [2.0], 'ram': [2.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server166(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-166")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 166,
            'instance_name': 'test_server_166',
            'openstack_id': 'test-server-id-166',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['1-1-0', '2-2-0'], 'vcpu_h': 3.0, 'mem_h_gb': 3.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 0.0, 'compute_type': ['default', 'default'], 'vcpu': [1.0, 2.0], 'ram': [1.0, 2.0], 'disk': [0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server167(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-167")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 167,
            'instance_name': 'test_server_167',
            'openstack_id': 'test-server-id-167',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0', '3-3-10'], 'vcpu_h': 5.0, 'mem_h_gb': 5.0, 'run_time': 2.0, 'size_time': [1.0, 1.0], 'disk_h_gb': 10.0, 'compute_type': ['default', 'default'], 'vcpu': [2.0, 3.0], 'ram': [2.0, 3.0], 'disk': [0.0, 10.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server168(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 09:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-168")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 168,
            'instance_name': 'test_server_168',
            'openstack_id': 'test-server-id-168',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['3-3-10', '1-1-0', '2-2-0'], 'vcpu_h': 6.0, 'mem_h_gb': 6.0, 'run_time': 3.0, 'size_time': [1.0, 1.0, 1.0], 'disk_h_gb': 10.0, 'compute_type': ['default', 'default', 'default'], 'vcpu': [3.0, 1.0, 2.0], 'ram': [3.0, 1.0, 2.0], 'disk': [10.0, 0.0, 0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server169(self):
        start_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-169")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 169,
            'instance_name': 'test_server_169',
            'openstack_id': 'test-server-id-169',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [2.0], 'ram': [2.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server170(self):
        start_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-170")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 170,
            'instance_name': 'test_server_170',
            'openstack_id': 'test-server-id-170',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [2.0], 'ram': [2.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server171(self):
        start_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 08:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-171")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 171,
            'instance_name': 'test_server_171',
            'openstack_id': 'test-server-id-171',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['3-3-10'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [3.0], 'ram': [3.0], 'disk': [10.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server172(self):
        start_time = datetime.datetime.strptime("2023-01-11 09:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 10:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-172")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 172,
            'instance_name': 'test_server_172',
            'openstack_id': 'test-server-id-172',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [2.0], 'ram': [2.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server173(self):
        start_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-173")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 173,
            'instance_name': 'test_server_173',
            'openstack_id': 'test-server-id-173',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['2-2-0'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [2.0], 'ram': [2.0], 'disk': [0.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_server174(self):
        start_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ServerBiller(start_time, finish_time, projects, "test-server-id-174")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 174,
            'instance_name': 'test_server_174',
            'openstack_id': 'test-server-id-174',
            'events': [],
            'license': 'NoLicense',
            'consumption': {'flavor': ['3-3-10'], 'vcpu_h': 0.0, 'mem_h_gb': 0.0, 'size_time': [0.0], 'run_time': 0.0, 'disk_h_gb': 0.0, 'compute_type': ['default'], 'vcpu': [3.0], 'ram': [3.0], 'disk': [10.0]},                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'      
            }
        self.assertEqual(bill_cons[0], test_controller)
         