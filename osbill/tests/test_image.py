from django.test import TestCase
from osbill.biller import ImageBiller
from osbill.models import Project, Image, ImageEvent, BillCodeProjectRelation, BillCode
import datetime
import pytz

class ImageCalcTestCase(TestCase):
     
    maxDiff = None
    def setUp(self):
        bc = BillCode.objects.create( 
            id = 1, 
            code = "20986.91.04",
            active = "True")        
        project = Project.objects.create( 
            id = 1, 
            openstack_id = "d670223972c2479ebcc5c7924e9f4a2e",
            name = "test-project", 
            description = "test project - terp 20986.91.04", 
            created = "2020-01-01 00:00:00.000000+00",
            enabled = True)
        bcpr = BillCodeProjectRelation.objects.create( 
            id = 1, 
            billcode = BillCode.objects.get(id=1),
            project = Project.objects.get(id=1), 
            activated = "2021-01-01 00:00:00+00:00")
        Image.objects.bulk_create(
        [
        Image(
            id = 1, 
            openstack_id = "test-image-id-1", 
            name = "test_image_1", 
            project = Project.objects.get(id=1)),
        Image(
            id = 2, 
            openstack_id = "test-image-id-2", 
            name = "test_image_2", 
            project = Project.objects.get(id=1)),
        Image(
            id = 3, 
            openstack_id = "test-image-id-3", 
            name = "test_image_3", 
            project = Project.objects.get(id=1)),
        Image(
            id = 4, 
            openstack_id = "test-image-id-4", 
            name = "test_image_4", 
            project = Project.objects.get(id=1)),
        Image(
            id = 5, 
            openstack_id = "test-image-id-5", 
            name = "test_image_5", 
            project = Project.objects.get(id=1)),
        Image(
            id = 6, 
            openstack_id = "test-image-id-6", 
            name = "test_image_6", 
            project = Project.objects.get(id=1)),
        Image(
            id = 7, 
            openstack_id = "test-image-id-7", 
            name = "test_image_7", 
            project = Project.objects.get(id=1)),
        Image(
            id = 8, 
            openstack_id = "test-image-id-8", 
            name = "test_image_8", 
            project = Project.objects.get(id=1)),
        Image(
            id = 9, 
            openstack_id = "test-image-id-9", 
            name = "test_image_9", 
            project = Project.objects.get(id=1)),
        Image(
            id = 10, 
            openstack_id = "test-image-id-10", 
            name = "test_image_10", 
            project = Project.objects.get(id=1)),
        Image(
            id = 11, 
            openstack_id = "test-image-id-11", 
            name = "test_image_11", 
            project = Project.objects.get(id=1)),
        Image(
            id = 12, 
            openstack_id = "test-image-id-12", 
            name = "test_image_12", 
            project = Project.objects.get(id=1)),
        Image(
            id = 13, 
            openstack_id = "test-image-id-13", 
            name = "test_image_13", 
            project = Project.objects.get(id=1)),
        Image(
            id = 14, 
            openstack_id = "test-image-id-14", 
            name = "test_image_14", 
            project = Project.objects.get(id=1)),
        Image(
            id = 15, 
            openstack_id = "test-image-id-15", 
            name = "test_image_15", 
            project = Project.objects.get(id=1)),
        ])
        ImageEvent.objects.bulk_create(
        [
        ImageEvent(
            created = "2023-01-11 03:00:00",
            name = "create", 
            image = Image.objects.get(id=1), 
            bill_state = True, 
            os_state = "active",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 04:00:00",
            name = "delete", 
            image = Image.objects.get(id=1), 
            bill_state = False, 
            os_state = "deleted",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 02:00:00",
            name = "create", 
            image = Image.objects.get(id=2), 
            bill_state = True, 
            os_state = "active",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 04:00:00",
            name = "delete", 
            image = Image.objects.get(id=2), 
            bill_state = False, 
            os_state = "deleted",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 02:00:00",
            name = "create", 
            image = Image.objects.get(id=3), 
            bill_state = True, 
            os_state = "active",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 03:00:00",
            name = "delete", 
            image = Image.objects.get(id=3), 
            bill_state = False, 
            os_state = "deleted",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 01:00:00",
            name = "create", 
            image = Image.objects.get(id=4), 
            bill_state = True, 
            os_state = "active",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 04:00:00",
            name = "delete", 
            image = Image.objects.get(id=4), 
            bill_state = False, 
            os_state = "deleted",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 01:00:00",
            name = "create", 
            image = Image.objects.get(id=5), 
            bill_state = True, 
            os_state = "active",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 03:00:00",
            name = "delete", 
            image = Image.objects.get(id=5), 
            bill_state = False, 
            os_state = "deleted",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 01:00:00",
            name = "create", 
            image = Image.objects.get(id=6), 
            bill_state = True, 
            os_state = "active",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 02:00:00",
            name = "delete", 
            image = Image.objects.get(id=6), 
            bill_state = False, 
            os_state = "deleted",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 03:00:00",
            name = "create", 
            image = Image.objects.get(id=7), 
            bill_state = True, 
            os_state = "active",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 04:00:00",
            name = "image_size_update", 
            image = Image.objects.get(id=7), 
            bill_state = True, 
            os_state = "active",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 05:00:00",
            name = "delete", 
            image = Image.objects.get(id=7), 
            bill_state = False, 
            os_state = "deleted",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 02:00:00",
            name = "create", 
            image = Image.objects.get(id=8), 
            bill_state = True, 
            os_state = "active",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 03:00:00",
            name = "image_size_update", 
            image = Image.objects.get(id=8), 
            bill_state = True, 
            os_state = "active",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 05:00:00",
            name = "delete", 
            image = Image.objects.get(id=8), 
            bill_state = False, 
            os_state = "deleted",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 02:00:00",
            name = "create", 
            image = Image.objects.get(id=9), 
            bill_state = True, 
            os_state = "active",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 04:00:00",
            name = "image_size_update", 
            image = Image.objects.get(id=9), 
            bill_state = True, 
            os_state = "active",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 05:00:00",
            name = "delete", 
            image = Image.objects.get(id=9), 
            bill_state = False, 
            os_state = "deleted",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 02:00:00",
            name = "create", 
            image = Image.objects.get(id=10), 
            bill_state = True, 
            os_state = "active",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 03:00:00",
            name = "image_size_update", 
            image = Image.objects.get(id=10), 
            bill_state = True, 
            os_state = "active",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 04:00:00",
            name = "delete", 
            image = Image.objects.get(id=10), 
            bill_state = False, 
            os_state = "deleted",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 01:00:00",
            name = "create", 
            image = Image.objects.get(id=11), 
            bill_state = True, 
            os_state = "active",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 02:00:00",
            name = "image_size_update", 
            image = Image.objects.get(id=11), 
            bill_state = True, 
            os_state = "active",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 05:00:00",
            name = "delete", 
            image = Image.objects.get(id=11), 
            bill_state = False, 
            os_state = "deleted",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 01:00:00",
            name = "create", 
            image = Image.objects.get(id=12), 
            bill_state = True, 
            os_state = "active",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 03:00:00",
            name = "image_size_update", 
            image = Image.objects.get(id=12), 
            bill_state = True, 
            os_state = "active",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 05:00:00",
            name = "delete", 
            image = Image.objects.get(id=12), 
            bill_state = False, 
            os_state = "deleted",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 01:00:00",
            name = "create", 
            image = Image.objects.get(id=13), 
            bill_state = True, 
            os_state = "active",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 02:00:00",
            name = "image_size_update", 
            image = Image.objects.get(id=13), 
            bill_state = True, 
            os_state = "active",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 04:00:00",
            name = "delete", 
            image = Image.objects.get(id=13), 
            bill_state = False, 
            os_state = "deleted",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 01:00:00",
            name = "create", 
            image = Image.objects.get(id=14), 
            bill_state = True, 
            os_state = "active",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 03:00:00",
            name = "image_size_update", 
            image = Image.objects.get(id=14), 
            bill_state = True, 
            os_state = "active",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 04:00:00",
            name = "delete", 
            image = Image.objects.get(id=14), 
            bill_state = False, 
            os_state = "deleted",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 01:00:00",
            name = "create", 
            image = Image.objects.get(id=15), 
            bill_state = True, 
            os_state = "active",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 02:00:00",
            name = "image_size_update", 
            image = Image.objects.get(id=15), 
            bill_state = True, 
            os_state = "active",
            size_gb = 10),
        ImageEvent(
            created = "2023-01-11 03:00:00",
            name = "delete", 
            image = Image.objects.get(id=15), 
            bill_state = False, 
            os_state = "deleted",
            size_gb = 10),
        ])

    def test_calc_image1(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ImageBiller(start_time, finish_time, projects, "test-image-id-1")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 1,
            'instance_name': 'test_image_1',
            'openstack_id': 'test-image-id-1',
            'consumption': 0.0, 
            'events': [],                       
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'size': 10.0,            
            'run_time': 0.0,
            'billing_code' : '20986.91.04'         
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_image2(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ImageBiller(start_time, finish_time, projects, "test-image-id-2")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 2,
            'instance_name': 'test_image_2',
            'openstack_id': 'test-image-id-2',
            'consumption': 10.0, 
            'events': [],                       
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'size': 10.0,            
            'run_time': 1.0,
            'billing_code' : '20986.91.04'         
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_image3(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ImageBiller(start_time, finish_time, projects, "test-image-id-3")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 3,
            'instance_name': 'test_image_3',
            'openstack_id': 'test-image-id-3',
            'consumption': 10.0, 
            'events': [],                       
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'size': 10.0,            
            'run_time': 1.0,
            'billing_code' : '20986.91.04'         
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_image4(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ImageBiller(start_time, finish_time, projects, "test-image-id-4")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 4,
            'instance_name': 'test_image_4',
            'openstack_id': 'test-image-id-4',
            'consumption': 10.0, 
            'events': [],                       
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'size': 10.0,            
            'run_time': 1.0,
            'billing_code' : '20986.91.04'         
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_image5(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ImageBiller(start_time, finish_time, projects, "test-image-id-5")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 5,
            'instance_name': 'test_image_5',
            'openstack_id': 'test-image-id-5',
            'consumption': 10.0, 
            'events': [],                       
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'size': 10.0,            
            'run_time': 1.0,
            'billing_code' : '20986.91.04'         
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_image6(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ImageBiller(start_time, finish_time, projects, "test-image-id-6")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 6,
            'instance_name': 'test_image_6',
            'openstack_id': 'test-image-id-6',
            'consumption': 0.0, 
            'events': [],                       
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'size': 10.0,            
            'run_time': 0.0,
            'billing_code' : '20986.91.04'         
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_image7(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ImageBiller(start_time, finish_time, projects, "test-image-id-7")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 7,
            'instance_name': 'test_image_7',
            'openstack_id': 'test-image-id-7',
            'consumption': 0.0, 
            'events': [],                       
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'size': 10.0,            
            'run_time': 0.0,
            'billing_code' : '20986.91.04'         
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_image8(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ImageBiller(start_time, finish_time, projects, "test-image-id-8")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 8,
            'instance_name': 'test_image_8',
            'openstack_id': 'test-image-id-8',
            'consumption': 20.0, 
            'events': [],                       
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'size': 10.0,            
            'run_time': 2.0,
            'billing_code' : '20986.91.04'         
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_image9(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ImageBiller(start_time, finish_time, projects, "test-image-id-9")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 9,
            'instance_name': 'test_image_9',
            'openstack_id': 'test-image-id-9',
            'consumption': 10.0, 
            'events': [],                       
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'size': 10.0,            
            'run_time': 1.0,
            'billing_code' : '20986.91.04'         
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_image10(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ImageBiller(start_time, finish_time, projects, "test-image-id-10")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 10,
            'instance_name': 'test_image_10',
            'openstack_id': 'test-image-id-10',
            'consumption': 20.0, 
            'events': [],                       
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'size': 10.0,            
            'run_time': 2.0,
            'billing_code' : '20986.91.04'         
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_image11(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ImageBiller(start_time, finish_time, projects, "test-image-id-11")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 11,
            'instance_name': 'test_image_11',
            'openstack_id': 'test-image-id-11',
            'consumption': 10.0, 
            'events': [],                       
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'size': 10.0,            
            'run_time': 1.0,
            'billing_code' : '20986.91.04'         
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_image12(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ImageBiller(start_time, finish_time, projects, "test-image-id-12")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 12,
            'instance_name': 'test_image_12',
            'openstack_id': 'test-image-id-12',
            'consumption': 20.0, 
            'events': [],                       
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'size': 10.0,            
            'run_time': 2.0,
            'billing_code' : '20986.91.04'         
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_image13(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ImageBiller(start_time, finish_time, projects, "test-image-id-13")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 13,
            'instance_name': 'test_image_13',
            'openstack_id': 'test-image-id-13',
            'consumption': 10.0, 
            'events': [],                       
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'size': 10.0,            
            'run_time': 1.0,
            'billing_code' : '20986.91.04'         
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_image14(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ImageBiller(start_time, finish_time, projects, "test-image-id-14")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 14,
            'instance_name': 'test_image_14',
            'openstack_id': 'test-image-id-14',
            'consumption': 20.0, 
            'events': [],                       
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'size': 10.0,            
            'run_time': 2.0,
            'billing_code' : '20986.91.04'         
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_image15(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = ImageBiller(start_time, finish_time, projects, "test-image-id-15")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 15,
            'instance_name': 'test_image_15',
            'openstack_id': 'test-image-id-15',
            'consumption': 0.0, 
            'events': [],                       
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'size': 10.0,            
            'run_time': 0.0,
            'billing_code' : '20986.91.04'         
            }
        self.assertEqual(bill_cons[0], test_controller)
         