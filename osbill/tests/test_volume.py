from django.test import TestCase
from osbill.biller import VolumeBiller
from osbill.models import Project, Volume, VolumeEvent, VolumeType, BillCodeProjectRelation, BillCode
import datetime
import pytz

class VolumeCalcTestCase(TestCase):
     
    maxDiff = None
    def setUp(self):
        bc = BillCode.objects.create( 
            id = 1, 
            code = "20986.91.04",
            active = "True")        
        project = Project.objects.create( 
            id = 1, 
            openstack_id = "d670223972c2479ebcc5c7924e9f4a2e",
            name = "test-project", 
            description = "test project - terp 20986.91.04", 
            created = "2020-01-01 00:00:00.000000+00",
            enabled = True)
        bcpr = BillCodeProjectRelation.objects.create( 
            id = 1, 
            billcode = BillCode.objects.get(id=1),
            project = Project.objects.get(id=1), 
            activated = "2020-01-01 00:00:00+00:00")
        VolumeType.objects.bulk_create(
        [
        VolumeType(
            id = 0, 
            openstack_id = 'd670223972c2479ebcc5c7924e9f4a2r', 
            name = 'standard', 
            is_public=True),
        VolumeType(
            id = 1, 
            openstack_id = 'd670223972c2479ebcc5c7924e9f4a2q', 
            name = 'performance', 
            is_public=True),
        VolumeType(
            id = 2, 
            openstack_id = 'd670223972c2479ebcc5c7924e9f4a2w', 
            name = 'performance-20000', 
            is_public=True),
        ])
        Volume.objects.bulk_create(
        [
        Volume(
            id = 1, 
            openstack_id = "test-volume-id-1", 
            name = "test_volume_1", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 2, 
            openstack_id = "test-volume-id-2", 
            name = "test_volume_2", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 3, 
            openstack_id = "test-volume-id-3", 
            name = "test_volume_3", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 4, 
            openstack_id = "test-volume-id-4", 
            name = "test_volume_4", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 5, 
            openstack_id = "test-volume-id-5", 
            name = "test_volume_5", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 6, 
            openstack_id = "test-volume-id-6", 
            name = "test_volume_6", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 7, 
            openstack_id = "test-volume-id-7", 
            name = "test_volume_7", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 8, 
            openstack_id = "test-volume-id-8", 
            name = "test_volume_8", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 9, 
            openstack_id = "test-volume-id-9", 
            name = "test_volume_9", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 10, 
            openstack_id = "test-volume-id-10", 
            name = "test_volume_10", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 11, 
            openstack_id = "test-volume-id-11", 
            name = "test_volume_11", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 12, 
            openstack_id = "test-volume-id-12", 
            name = "test_volume_12", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 13, 
            openstack_id = "test-volume-id-13", 
            name = "test_volume_13", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 14, 
            openstack_id = "test-volume-id-14", 
            name = "test_volume_14", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 15, 
            openstack_id = "test-volume-id-15", 
            name = "test_volume_15", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 16, 
            openstack_id = "test-volume-id-16", 
            name = "test_volume_16", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 17, 
            openstack_id = "test-volume-id-17", 
            name = "test_volume_17", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 18, 
            openstack_id = "test-volume-id-18", 
            name = "test_volume_18", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 19, 
            openstack_id = "test-volume-id-19", 
            name = "test_volume_19", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 20, 
            openstack_id = "test-volume-id-20", 
            name = "test_volume_20", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 21, 
            openstack_id = "test-volume-id-21", 
            name = "test_volume_21", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 22, 
            openstack_id = "test-volume-id-22", 
            name = "test_volume_22", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 23, 
            openstack_id = "test-volume-id-23", 
            name = "test_volume_23", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 24, 
            openstack_id = "test-volume-id-24", 
            name = "test_volume_24", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 25, 
            openstack_id = "test-volume-id-25", 
            name = "test_volume_25", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 26, 
            openstack_id = "test-volume-id-26", 
            name = "test_volume_26", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 27, 
            openstack_id = "test-volume-id-27", 
            name = "test_volume_27", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 28, 
            openstack_id = "test-volume-id-28", 
            name = "test_volume_28", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 29, 
            openstack_id = "test-volume-id-29", 
            name = "test_volume_29", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 30, 
            openstack_id = "test-volume-id-30", 
            name = "test_volume_30", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 31, 
            openstack_id = "test-volume-id-31", 
            name = "test_volume_31", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 32, 
            openstack_id = "test-volume-id-32", 
            name = "test_volume_32", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 33, 
            openstack_id = "test-volume-id-33", 
            name = "test_volume_33", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 34, 
            openstack_id = "test-volume-id-34", 
            name = "test_volume_34", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 35, 
            openstack_id = "test-volume-id-35", 
            name = "test_volume_35", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 36, 
            openstack_id = "test-volume-id-36", 
            name = "test_volume_36", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 37, 
            openstack_id = "test-volume-id-37", 
            name = "test_volume_37", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 38, 
            openstack_id = "test-volume-id-38", 
            name = "test_volume_38", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 39, 
            openstack_id = "test-volume-id-39", 
            name = "test_volume_39", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 40, 
            openstack_id = "test-volume-id-40", 
            name = "test_volume_40", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 41, 
            openstack_id = "test-volume-id-41", 
            name = "test_volume_41", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 42, 
            openstack_id = "test-volume-id-42", 
            name = "test_volume_42", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 43, 
            openstack_id = "test-volume-id-43", 
            name = "test_volume_43", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 44, 
            openstack_id = "test-volume-id-44", 
            name = "test_volume_44", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 45, 
            openstack_id = "test-volume-id-45", 
            name = "test_volume_45", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 46, 
            openstack_id = "test-volume-id-46", 
            name = "test_volume_46", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 47, 
            openstack_id = "test-volume-id-47", 
            name = "test_volume_47", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 48, 
            openstack_id = "test-volume-id-48", 
            name = "test_volume_48", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 49, 
            openstack_id = "test-volume-id-49", 
            name = "test_volume_49", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 50, 
            openstack_id = "test-volume-id-50", 
            name = "test_volume_50", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 51, 
            openstack_id = "test-volume-id-51", 
            name = "test_volume_51", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 52, 
            openstack_id = "test-volume-id-52", 
            name = "test_volume_52", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 53, 
            openstack_id = "test-volume-id-53", 
            name = "test_volume_53", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 54, 
            openstack_id = "test-volume-id-54", 
            name = "test_volume_54", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 55, 
            openstack_id = "test-volume-id-55", 
            name = "test_volume_55", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 56, 
            openstack_id = "test-volume-id-56", 
            name = "test_volume_56", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 57, 
            openstack_id = "test-volume-id-57", 
            name = "test_volume_57", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 58, 
            openstack_id = "test-volume-id-58", 
            name = "test_volume_58", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 59, 
            openstack_id = "test-volume-id-59", 
            name = "test_volume_59", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 60, 
            openstack_id = "test-volume-id-60", 
            name = "test_volume_60", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 61, 
            openstack_id = "test-volume-id-61", 
            name = "test_volume_61", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 62, 
            openstack_id = "test-volume-id-62", 
            name = "test_volume_62", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 63, 
            openstack_id = "test-volume-id-63", 
            name = "test_volume_63", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 64, 
            openstack_id = "test-volume-id-64", 
            name = "test_volume_64", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 65, 
            openstack_id = "test-volume-id-65", 
            name = "test_volume_65", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 66, 
            openstack_id = "test-volume-id-66", 
            name = "test_volume_66", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 67, 
            openstack_id = "test-volume-id-67", 
            name = "test_volume_67", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 68, 
            openstack_id = "test-volume-id-68", 
            name = "test_volume_68", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 69, 
            openstack_id = "test-volume-id-69", 
            name = "test_volume_69", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 70, 
            openstack_id = "test-volume-id-70", 
            name = "test_volume_70", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 71, 
            openstack_id = "test-volume-id-71", 
            name = "test_volume_71", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 72, 
            openstack_id = "test-volume-id-72", 
            name = "test_volume_72", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 73, 
            openstack_id = "test-volume-id-73", 
            name = "test_volume_73", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 74, 
            openstack_id = "test-volume-id-74", 
            name = "test_volume_74", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 75, 
            openstack_id = "test-volume-id-75", 
            name = "test_volume_75", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 76, 
            openstack_id = "test-volume-id-76", 
            name = "test_volume_76", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 77, 
            openstack_id = "test-volume-id-77", 
            name = "test_volume_77", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 78, 
            openstack_id = "test-volume-id-78", 
            name = "test_volume_78", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 79, 
            openstack_id = "test-volume-id-79", 
            name = "test_volume_79", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 80, 
            openstack_id = "test-volume-id-80", 
            name = "test_volume_80", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 81, 
            openstack_id = "test-volume-id-81", 
            name = "test_volume_81", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 82, 
            openstack_id = "test-volume-id-82", 
            name = "test_volume_82", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 83, 
            openstack_id = "test-volume-id-83", 
            name = "test_volume_83", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 84, 
            openstack_id = "test-volume-id-84", 
            name = "test_volume_84", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 85, 
            openstack_id = "test-volume-id-85", 
            name = "test_volume_85", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 86, 
            openstack_id = "test-volume-id-86", 
            name = "test_volume_86", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 87, 
            openstack_id = "test-volume-id-87", 
            name = "test_volume_87", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 88, 
            openstack_id = "test-volume-id-88", 
            name = "test_volume_88", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 89, 
            openstack_id = "test-volume-id-89", 
            name = "test_volume_89", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 90, 
            openstack_id = "test-volume-id-90", 
            name = "test_volume_90", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 91, 
            openstack_id = "test-volume-id-91", 
            name = "test_volume_91", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 92, 
            openstack_id = "test-volume-id-92", 
            name = "test_volume_92", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 93, 
            openstack_id = "test-volume-id-93", 
            name = "test_volume_93", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 94, 
            openstack_id = "test-volume-id-94", 
            name = "test_volume_94", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 95, 
            openstack_id = "test-volume-id-95", 
            name = "test_volume_95", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 96, 
            openstack_id = "test-volume-id-96", 
            name = "test_volume_96", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 97, 
            openstack_id = "test-volume-id-97", 
            name = "test_volume_97", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 98, 
            openstack_id = "test-volume-id-98", 
            name = "test_volume_98", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 99, 
            openstack_id = "test-volume-id-99", 
            name = "test_volume_99", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 100, 
            openstack_id = "test-volume-id-100", 
            name = "test_volume_100", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 101, 
            openstack_id = "test-volume-id-101", 
            name = "test_volume_101", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 102, 
            openstack_id = "test-volume-id-102", 
            name = "test_volume_102", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 103, 
            openstack_id = "test-volume-id-103", 
            name = "test_volume_103", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 104, 
            openstack_id = "test-volume-id-104", 
            name = "test_volume_104", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 105, 
            openstack_id = "test-volume-id-105", 
            name = "test_volume_105", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 106, 
            openstack_id = "test-volume-id-106", 
            name = "test_volume_106", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 107, 
            openstack_id = "test-volume-id-107", 
            name = "test_volume_107", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 108, 
            openstack_id = "test-volume-id-108", 
            name = "test_volume_108", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 109, 
            openstack_id = "test-volume-id-109", 
            name = "test_volume_109", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 110, 
            openstack_id = "test-volume-id-110", 
            name = "test_volume_110", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 111, 
            openstack_id = "test-volume-id-111", 
            name = "test_volume_111", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 112, 
            openstack_id = "test-volume-id-112", 
            name = "test_volume_112", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 113, 
            openstack_id = "test-volume-id-113", 
            name = "test_volume_113", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 114, 
            openstack_id = "test-volume-id-114", 
            name = "test_volume_114", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 115, 
            openstack_id = "test-volume-id-115", 
            name = "test_volume_115", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 116, 
            openstack_id = "test-volume-id-116", 
            name = "test_volume_116", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 117, 
            openstack_id = "test-volume-id-117", 
            name = "test_volume_117", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 118, 
            openstack_id = "test-volume-id-118", 
            name = "test_volume_118", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 119, 
            openstack_id = "test-volume-id-119", 
            name = "test_volume_119", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 120, 
            openstack_id = "test-volume-id-120", 
            name = "test_volume_120", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 121, 
            openstack_id = "test-volume-id-121", 
            name = "test_volume_121", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 122, 
            openstack_id = "test-volume-id-122", 
            name = "test_volume_122", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 123, 
            openstack_id = "test-volume-id-123", 
            name = "test_volume_123", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 124, 
            openstack_id = "test-volume-id-124", 
            name = "test_volume_124", 
            project = Project.objects.get(id=1)),
        Volume(
            id = 125, 
            openstack_id = "test-volume-id-125", 
            name = "test_volume_125", 
            project = Project.objects.get(id=1)),
        ])
        VolumeEvent.objects.bulk_create(
        [
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=1), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=1), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=2), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=2), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=3), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=3), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=4), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=4), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=5), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=5), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=6), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=6), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=7), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=7), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=7), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=8), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=8), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=8), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=8), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=9), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=9), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=9), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=10), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=10), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=10), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=11), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=11), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=11), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=11), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=12), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=12), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=12), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=12), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=12), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=12), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=13), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=13), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=13), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=14), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=14), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=14), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=14), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=15), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=15), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=15), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=16), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=16), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=16), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=17), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=17), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=17), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=18), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=18), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=18), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=18), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=19), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=19), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=19), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=19), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=19), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=20), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=20), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=20), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=20), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=20), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=20), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=20), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=20), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=21), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=21), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=21), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=22), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=22), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=22), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=23), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=23), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=23), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=23), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=24), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=24), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=24), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=24), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=24), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=24), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=25), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=25), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=25), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=26), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=26), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=26), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=26), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=27), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=27), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=27), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=28), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=28), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=28), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=28), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=29), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=29), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=29), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=29), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=29), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=30), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=30), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=30), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=31), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=31), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=31), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=31), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=32), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=32), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=32), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=32), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=32), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=32), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=33), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=33), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=33), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=34), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=34), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=34), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=34), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=35), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=35), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=35), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=35), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=35), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=36), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=36), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=36), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=37), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=37), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=37), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=38), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=38), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=38), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=39), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=39), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=39), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=39), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=40), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=40), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=40), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=40), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=40), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=41), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=41), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=41), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=41), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=41), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=41), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=41), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=41), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=42), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=42), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=42), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=43), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=43), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=43), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=44), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=44), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=44), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=44), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=45), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=45), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=45), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=45), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=45), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=45), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=46), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=46), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=46), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=47), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=47), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=47), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=47), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=48), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=48), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=48), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=48), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=49), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=49), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=49), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=49), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=49), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=50), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=50), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=50), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=50), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=50), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=50), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=50), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=51), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=51), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=51), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=51), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=51), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=51), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=51), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=52), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=52), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=52), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=52), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=52), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=52), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=52), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=52), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 11:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=52), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 12:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=52), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=53), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=53), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=53), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=53), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=54), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=54), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=54), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=54), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=55), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=55), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=55), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=55), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=55), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=56), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=56), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=56), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=56), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=56), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=56), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=56), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=57), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=57), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=57), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=57), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=57), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=57), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=57), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=57), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 11:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=57), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=58), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=58), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=58), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=58), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=58), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=58), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=58), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=58), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=59), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=59), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=59), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=59), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=60), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=60), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=60), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=60), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=61), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=61), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=61), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=61), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=61), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=61), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=62), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=62), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=62), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=62), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=62), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=63), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=63), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=63), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=63), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=63), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=63), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=64), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=64), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=64), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=64), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=64), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=64), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=64), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=64), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 11:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=64), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 12:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=64), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 13:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=64), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=65), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=65), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=65), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=65), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=65), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=65), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=65), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=66), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=66), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=66), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=66), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=66), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=66), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=66), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=67), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=67), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=67), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=67), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=67), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=67), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=67), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=68), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=68), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=68), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=68), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=68), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=68), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=68), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=68), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 11:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=68), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 12:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=68), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 13:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=68), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 14:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=68), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 15:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=68), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 16:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=68), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=69), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=69), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=69), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=69), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=70), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=70), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=70), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=70), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=71), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=71), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=71), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=71), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=71), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=71), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=71), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=72), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=72), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=72), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=72), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=72), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=73), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=73), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=73), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=73), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=73), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=74), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=74), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=74), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=74), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=74), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=75), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=75), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=75), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=75), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=75), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=75), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=75), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=76), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=76), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=76), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=76), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=76), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=76), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=76), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=76), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=76), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 11:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=76), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=77), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=77), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=77), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=77), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=78), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=78), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=78), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=78), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=79), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=79), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=79), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=79), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=80), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=80), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=80), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=80), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=80), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=81), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=81), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=81), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=81), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=81), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=81), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=81), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=81), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 11:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=81), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=82), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=82), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=82), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=82), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=83), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=83), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=83), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=83), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=84), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=84), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=84), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=84), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=85), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=85), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=85), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=85), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=85), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=85), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=85), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=86), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=86), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=86), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=86), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=86), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=86), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=86), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=86), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 11:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=86), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 12:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=86), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 13:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=86), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=87), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=87), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=87), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=87), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=88), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=88), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=88), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=88), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=89), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=89), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=89), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=89), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=90), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=90), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=90), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=90), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=90), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=90), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=90), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=90), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 11:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=90), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 12:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=90), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 13:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=90), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=91), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=91), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=91), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=91), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=92), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=92), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=92), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=92), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=92), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=93), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=93), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=93), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=93), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=93), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=94), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=94), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=94), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=94), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=94), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=94), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=95), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=95), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=95), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=95), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=95), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=95), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=95), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=95), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=95), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 11:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=95), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 12:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=95), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 14:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=95), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 15:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=95), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=96), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=96), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=96), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=96), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=96), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=96), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=97), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=97), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=97), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=97), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=97), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=97), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=98), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=98), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=98), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=98), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=98), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=98), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=99), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=99), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=99), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=99), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=99), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=99), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=99), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=99), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=99), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 11:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=99), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 12:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=99), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 14:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=99), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 15:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=99), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 16:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=99), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 17:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=99), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 18:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=99), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 19:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=99), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=100), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=100), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=100), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=100), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=100), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=100), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=100), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=101), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=101), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=101), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=101), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=101), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=101), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=101), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=101), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 11:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=101), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 12:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=101), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 13:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=101), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=102), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=102), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=102), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=102), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=102), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=102), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=102), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=102), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=102), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 12:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=102), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 13:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=102), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 14:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=102), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 15:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=102), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 16:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=102), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=103), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=103), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=103), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=103), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=104), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=104), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=104), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=104), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=105), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=105), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=105), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=105), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=106), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=106), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=106), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=106), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=106), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=106), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=106), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=107), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=107), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=107), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=107), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=107), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=107), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=108), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=108), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=108), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=108), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=108), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=109), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=109), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=109), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=109), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=109), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=109), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=110), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=110), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=110), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=110), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=110), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=110), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=110), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=110), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=110), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=111), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=111), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=111), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=111), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=111), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=112), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=112), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=112), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=112), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=112), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=113), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=113), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=113), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=113), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=113), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=113), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=113), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=114), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=114), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=114), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=114), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=114), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=114), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=114), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=114), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=114), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 11:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=114), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 12:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=114), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 13:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=114), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=115), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=115), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=115), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=115), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=115), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=115), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=115), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=116), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=116), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=116), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=116), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=116), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=116), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=116), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=116), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=116), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 11:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=116), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 12:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=116), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 13:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=116), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=117), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=117), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=117), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=117), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=117), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=117), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=117), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=117), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=117), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=118), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=118), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=118), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=118), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=119), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=119), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=119), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=119), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=120), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=120), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=120), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=120), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=120), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=121), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=121), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=121), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=121), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=121), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=121), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=122), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=122), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=122), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=122), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=122), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=123), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=123), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=123), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=123), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=123), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=124), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=124), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=124), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=124), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=124), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 01:00:00+00:00",
            name = "create", 
            volume = Volume.objects.get(id=125), 
            bill_state = True, 
            os_state = "active",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 02:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=125), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 03:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=125), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 04:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=125), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 05:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=125), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 06:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=125), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 07:00:00+00:00",
            name = "resize", 
            volume = Volume.objects.get(id=125), 
            bill_state = True, 
            os_state = "resized",
            volume_type = VolumeType.objects.get(id=1),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 08:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=125), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=2),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 09:00:00+00:00",
            name = "retype", 
            volume = Volume.objects.get(id=125), 
            bill_state = True, 
            os_state = "retyped",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        VolumeEvent(
            created = "2023-01-11 10:00:00+00:00",
            name = "delete", 
            volume = Volume.objects.get(id=125), 
            bill_state = False, 
            os_state = "deleted",
            volume_type = VolumeType.objects.get(id=0),
            size_gb = 10.0),
        ])

    def test_calc_volume1(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-1")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 1,
            'instance_name': 'test_volume_1',
            'openstack_id': 'test-volume-id-1',
            'events': [],
            'consumption': {'volume_type_name': ['standard'], 'volume_type': [0], 'total_h_gb': {}, 'disk': [10.0], 'size_time': [0.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume2(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-2")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 2,
            'instance_name': 'test_volume_2',
            'openstack_id': 'test-volume-id-2',
            'events': [],
            'consumption': {'volume_type_name': ['standard'], 'volume_type': [0], 'total_h_gb': {'standard_volume_type': 10.0}, 'disk': [10.0], 'size_time': [1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume3(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-3")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 3,
            'instance_name': 'test_volume_3',
            'openstack_id': 'test-volume-id-3',
            'events': [],
            'consumption': {'volume_type_name': ['standard'], 'volume_type': [0], 'total_h_gb': {'standard_volume_type': 10.0}, 'disk': [10.0], 'size_time': [1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume4(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-4")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 4,
            'instance_name': 'test_volume_4',
            'openstack_id': 'test-volume-id-4',
            'events': [],
            'consumption': {'volume_type_name': ['standard'], 'volume_type': [0], 'total_h_gb': {'standard_volume_type': 10.0}, 'disk': [10.0], 'size_time': [1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume5(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-5")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 5,
            'instance_name': 'test_volume_5',
            'openstack_id': 'test-volume-id-5',
            'events': [],
            'consumption': {'volume_type_name': ['standard'], 'volume_type': [0], 'total_h_gb': {'standard_volume_type': 10.0}, 'disk': [10.0], 'size_time': [1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume6(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-6")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 6,
            'instance_name': 'test_volume_6',
            'openstack_id': 'test-volume-id-6',
            'events': [],
            'consumption': {'volume_type_name': ['standard'], 'volume_type': [0], 'total_h_gb': {}, 'disk': [10.0], 'size_time': [0.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume7(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-7")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 7,
            'instance_name': 'test_volume_7',
            'openstack_id': 'test-volume-id-7',
            'events': [],
            'consumption': {'volume_type_name': ['standard'], 'volume_type': [0], 'total_h_gb': {}, 'disk': [10.0], 'size_time': [0.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume8(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-8")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 8,
            'instance_name': 'test_volume_8',
            'openstack_id': 'test-volume-id-8',
            'events': [],
            'consumption': {'volume_type_name': ['standard'], 'volume_type': [0], 'total_h_gb': {}, 'disk': [10.0], 'size_time': [0.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume9(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-9")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 9,
            'instance_name': 'test_volume_9',
            'openstack_id': 'test-volume-id-9',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'standard'], 'volume_type': [0, 0], 'total_h_gb': {'standard_volume_type': 20.0}, 'disk': [10.0, 10.0], 'size_time': [1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume10(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-10")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 10,
            'instance_name': 'test_volume_10',
            'openstack_id': 'test-volume-id-10',
            'events': [],
            'consumption': {'volume_type_name': ['standard'], 'volume_type': [0], 'total_h_gb': {'standard_volume_type': 10.0}, 'disk': [10.0], 'size_time': [1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume11(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-11")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 11,
            'instance_name': 'test_volume_11',
            'openstack_id': 'test-volume-id-11',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'standard'], 'volume_type': [0, 0], 'total_h_gb': {'standard_volume_type': 20.0}, 'disk': [10.0, 10.0], 'size_time': [1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume12(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-12")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 12,
            'instance_name': 'test_volume_12',
            'openstack_id': 'test-volume-id-12',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'standard', 'standard'], 'volume_type': [0, 0, 0], 'total_h_gb': {'standard_volume_type': 30.0}, 'disk': [10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume13(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-13")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 13,
            'instance_name': 'test_volume_13',
            'openstack_id': 'test-volume-id-13',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'standard'], 'volume_type': [0, 0], 'total_h_gb': {'standard_volume_type': 20.0}, 'disk': [10.0, 10.0], 'size_time': [1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume14(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-14")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 14,
            'instance_name': 'test_volume_14',
            'openstack_id': 'test-volume-id-14',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'standard', 'standard'], 'volume_type': [0, 0, 0], 'total_h_gb': {'standard_volume_type': 30.0}, 'disk': [10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume15(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-15")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 15,
            'instance_name': 'test_volume_15',
            'openstack_id': 'test-volume-id-15',
            'events': [],
            'consumption': {'volume_type_name': ['standard'], 'volume_type': [0], 'total_h_gb': {'standard_volume_type': 10.0}, 'disk': [10.0], 'size_time': [1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume16(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-16")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 16,
            'instance_name': 'test_volume_16',
            'openstack_id': 'test-volume-id-16',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'standard'], 'volume_type': [0, 0], 'total_h_gb': {'standard_volume_type': 20.0}, 'disk': [10.0, 10.0], 'size_time': [1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume17(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-17")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 17,
            'instance_name': 'test_volume_17',
            'openstack_id': 'test-volume-id-17',
            'events': [],
            'consumption': {'volume_type_name': ['standard'], 'volume_type': [0], 'total_h_gb': {'standard_volume_type': 10.0}, 'disk': [10.0], 'size_time': [1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume18(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-18")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 18,
            'instance_name': 'test_volume_18',
            'openstack_id': 'test-volume-id-18',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'standard'], 'volume_type': [0, 0], 'total_h_gb': {'standard_volume_type': 20.0}, 'disk': [10.0, 10.0], 'size_time': [1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume19(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-19")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 19,
            'instance_name': 'test_volume_19',
            'openstack_id': 'test-volume-id-19',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'standard'], 'volume_type': [0, 0], 'total_h_gb': {'standard_volume_type': 20.0}, 'disk': [10.0, 10.0], 'size_time': [1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume20(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-20")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 20,
            'instance_name': 'test_volume_20',
            'openstack_id': 'test-volume-id-20',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'standard', 'standard'], 'volume_type': [0, 0, 0], 'total_h_gb': {'standard_volume_type': 30.0}, 'disk': [10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume21(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-21")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 21,
            'instance_name': 'test_volume_21',
            'openstack_id': 'test-volume-id-21',
            'events': [],
            'consumption': {'volume_type_name': ['standard'], 'volume_type': [0], 'total_h_gb': {'standard_volume_type': 10.0}, 'disk': [10.0], 'size_time': [1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume22(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-22")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 22,
            'instance_name': 'test_volume_22',
            'openstack_id': 'test-volume-id-22',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'standard'], 'volume_type': [0, 0], 'total_h_gb': {'standard_volume_type': 20.0}, 'disk': [10.0, 10.0], 'size_time': [1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume23(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-23")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 23,
            'instance_name': 'test_volume_23',
            'openstack_id': 'test-volume-id-23',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'standard'], 'volume_type': [0, 0], 'total_h_gb': {'standard_volume_type': 20.0}, 'disk': [10.0, 10.0], 'size_time': [1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume24(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 08:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-24")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 24,
            'instance_name': 'test_volume_24',
            'openstack_id': 'test-volume-id-24',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'standard', 'standard'], 'volume_type': [0, 0, 0], 'total_h_gb': {'standard_volume_type': 30.0}, 'disk': [10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume25(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-25")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 25,
            'instance_name': 'test_volume_25',
            'openstack_id': 'test-volume-id-25',
            'events': [],
            'consumption': {'volume_type_name': ['standard'], 'volume_type': [0], 'total_h_gb': {}, 'disk': [10.0], 'size_time': [0.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume26(self):
        start_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-26")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 26,
            'instance_name': 'test_volume_26',
            'openstack_id': 'test-volume-id-26',
            'events': [],
            'consumption': {'volume_type_name': ['standard'], 'volume_type': [0], 'total_h_gb': {}, 'disk': [10.0], 'size_time': [0.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume27(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-27")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 27,
            'instance_name': 'test_volume_27',
            'openstack_id': 'test-volume-id-27',
            'events': [],
            'consumption': {'volume_type_name': ['standard'], 'volume_type': [0], 'total_h_gb': {}, 'disk': [10.0], 'size_time': [0.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume28(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-28")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 28,
            'instance_name': 'test_volume_28',
            'openstack_id': 'test-volume-id-28',
            'events': [],
            'consumption': {'volume_type_name': ['standard'], 'volume_type': [0], 'total_h_gb': {}, 'disk': [10.0], 'size_time': [0.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume29(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-29")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 29,
            'instance_name': 'test_volume_29',
            'openstack_id': 'test-volume-id-29',
            'events': [],
            'consumption': {'volume_type_name': ['standard'], 'volume_type': [0], 'total_h_gb': {}, 'disk': [10.0], 'size_time': [0.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume30(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-30")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 30,
            'instance_name': 'test_volume_30',
            'openstack_id': 'test-volume-id-30',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance'], 'volume_type': [0, 1], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 10.0}, 'disk': [10.0, 10.0], 'size_time': [1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume31(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-31")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 31,
            'instance_name': 'test_volume_31',
            'openstack_id': 'test-volume-id-31',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance'], 'volume_type': [0, 1], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 10.0}, 'disk': [10.0, 10.0], 'size_time': [1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume32(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-32")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 32,
            'instance_name': 'test_volume_32',
            'openstack_id': 'test-volume-id-32',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance', 'performance-20000'], 'volume_type': [0, 1, 2], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 10.0, 'performance-20000_volume_type': 10.0}, 'disk': [10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume33(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-33")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 33,
            'instance_name': 'test_volume_33',
            'openstack_id': 'test-volume-id-33',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance'], 'volume_type': [0, 1], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 10.0}, 'disk': [10.0, 10.0], 'size_time': [1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume34(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-34")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 34,
            'instance_name': 'test_volume_34',
            'openstack_id': 'test-volume-id-34',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance', 'performance-20000'], 'volume_type': [0, 1, 2], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 10.0, 'performance-20000_volume_type': 10.0}, 'disk': [10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume35(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-35")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 35,
            'instance_name': 'test_volume_35',
            'openstack_id': 'test-volume-id-35',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance', 'performance-20000', 'standard'], 'volume_type': [0, 1, 2, 0], 'total_h_gb': {'standard_volume_type': 20.0, 'performance_volume_type': 10.0, 'performance-20000_volume_type': 10.0}, 'disk': [10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume36(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-36")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 36,
            'instance_name': 'test_volume_36',
            'openstack_id': 'test-volume-id-36',
            'events': [],
            'consumption': {'volume_type_name': ['performance'], 'volume_type': [1], 'total_h_gb': {'performance_volume_type': 10.0}, 'disk': [10.0], 'size_time': [1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume37(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-37")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 37,
            'instance_name': 'test_volume_37',
            'openstack_id': 'test-volume-id-37',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance'], 'volume_type': [0, 1], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 10.0}, 'disk': [10.0, 10.0], 'size_time': [1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume38(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-38")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 38,
            'instance_name': 'test_volume_38',
            'openstack_id': 'test-volume-id-38',
            'events': [],
            'consumption': {'volume_type_name': ['standard'], 'volume_type': [0], 'total_h_gb': {'standard_volume_type': 10.0}, 'disk': [10.0], 'size_time': [1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume39(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-39")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 39,
            'instance_name': 'test_volume_39',
            'openstack_id': 'test-volume-id-39',
            'events': [],
            'consumption': {'volume_type_name': ['performance', 'performance-20000'], 'volume_type': [1, 2], 'total_h_gb': {'performance_volume_type': 10.0, 'performance-20000_volume_type': 10.0}, 'disk': [10.0, 10.0], 'size_time': [1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume40(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-40")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 40,
            'instance_name': 'test_volume_40',
            'openstack_id': 'test-volume-id-40',
            'events': [],
            'consumption': {'volume_type_name': ['performance', 'performance-20000'], 'volume_type': [1, 2], 'total_h_gb': {'performance_volume_type': 10.0, 'performance-20000_volume_type': 10.0}, 'disk': [10.0, 10.0], 'size_time': [1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume41(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-41")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 41,
            'instance_name': 'test_volume_41',
            'openstack_id': 'test-volume-id-41',
            'events': [],
            'consumption': {'volume_type_name': ['performance-20000', 'standard', 'performance'], 'volume_type': [2, 0, 1], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 10.0, 'performance-20000_volume_type': 10.0}, 'disk': [10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume42(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-42")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 42,
            'instance_name': 'test_volume_42',
            'openstack_id': 'test-volume-id-42',
            'events': [],
            'consumption': {'volume_type_name': ['performance'], 'volume_type': [1], 'total_h_gb': {'performance_volume_type': 10.0}, 'disk': [10.0], 'size_time': [1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume43(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-43")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 43,
            'instance_name': 'test_volume_43',
            'openstack_id': 'test-volume-id-43',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance'], 'volume_type': [0, 1], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 10.0}, 'disk': [10.0, 10.0], 'size_time': [1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume44(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-44")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 44,
            'instance_name': 'test_volume_44',
            'openstack_id': 'test-volume-id-44',
            'events': [],
            'consumption': {'volume_type_name': ['performance', 'performance-20000'], 'volume_type': [1, 2], 'total_h_gb': {'performance_volume_type': 10.0, 'performance-20000_volume_type': 10.0}, 'disk': [10.0, 10.0], 'size_time': [1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume45(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 08:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-45")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 45,
            'instance_name': 'test_volume_45',
            'openstack_id': 'test-volume-id-45',
            'events': [],
            'consumption': {'volume_type_name': ['performance-20000', 'standard', 'performance'], 'volume_type': [2, 0, 1], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 10.0, 'performance-20000_volume_type': 10.0}, 'disk': [10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume46(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-46")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 46,
            'instance_name': 'test_volume_46',
            'openstack_id': 'test-volume-id-46',
            'events': [],
            'consumption': {'volume_type_name': ['performance'], 'volume_type': [1], 'total_h_gb': {}, 'disk': [10.0], 'size_time': [0.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume47(self):
        start_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-47")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 47,
            'instance_name': 'test_volume_47',
            'openstack_id': 'test-volume-id-47',
            'events': [],
            'consumption': {'volume_type_name': ['performance-20000'], 'volume_type': [2], 'total_h_gb': {}, 'disk': [10.0], 'size_time': [0.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume48(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-48")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 48,
            'instance_name': 'test_volume_48',
            'openstack_id': 'test-volume-id-48',
            'events': [],
            'consumption': {'volume_type_name': ['standard'], 'volume_type': [0], 'total_h_gb': {}, 'disk': [10.0], 'size_time': [0.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume49(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-49")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 49,
            'instance_name': 'test_volume_49',
            'openstack_id': 'test-volume-id-49',
            'events': [],
            'consumption': {'volume_type_name': ['standard'], 'volume_type': [0], 'total_h_gb': {}, 'disk': [10.0], 'size_time': [0.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume50(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-50")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 50,
            'instance_name': 'test_volume_50',
            'openstack_id': 'test-volume-id-50',
            'events': [],
            'consumption': {'volume_type_name': ['standard'], 'volume_type': [0], 'total_h_gb': {}, 'disk': [10.0], 'size_time': [0.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume51(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-51")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 51,
            'instance_name': 'test_volume_51',
            'openstack_id': 'test-volume-id-51',
            'events': [],
            'consumption': {'volume_type_name': ['standard'], 'volume_type': [0], 'total_h_gb': {}, 'disk': [10.0], 'size_time': [0.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume52(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-52")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 52,
            'instance_name': 'test_volume_52',
            'openstack_id': 'test-volume-id-52',
            'events': [],
            'consumption': {'volume_type_name': ['standard'], 'volume_type': [0], 'total_h_gb': {}, 'disk': [10.0], 'size_time': [0.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume53(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-53")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 53,
            'instance_name': 'test_volume_53',
            'openstack_id': 'test-volume-id-53',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance', 'performance'], 'volume_type': [0, 1, 1], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 20.0}, 'disk': [10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume54(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-54")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 54,
            'instance_name': 'test_volume_54',
            'openstack_id': 'test-volume-id-54',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'standard', 'performance'], 'volume_type': [0, 0, 1], 'total_h_gb': {'standard_volume_type': 20.0, 'performance_volume_type': 10.0}, 'disk': [10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume55(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-55")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 55,
            'instance_name': 'test_volume_55',
            'openstack_id': 'test-volume-id-55',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance', 'performance', 'performance-20000'], 'volume_type': [0, 1, 1, 2], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 20.0, 'performance-20000_volume_type': 10.0}, 'disk': [10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume56(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 08:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-56")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 56,
            'instance_name': 'test_volume_56',
            'openstack_id': 'test-volume-id-56',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance', 'performance-20000', 'performance-20000', 'standard', 'performance'], 'volume_type': [0, 1, 2, 2, 0, 1], 'total_h_gb': {'standard_volume_type': 20.0, 'performance_volume_type': 20.0, 'performance-20000_volume_type': 20.0}, 'disk': [10.0, 10.0, 10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume57(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 08:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-57")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 57,
            'instance_name': 'test_volume_57',
            'openstack_id': 'test-volume-id-57',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance', 'performance-20000', 'performance-20000', 'standard', 'performance'], 'volume_type': [0, 1, 2, 2, 0, 1], 'total_h_gb': {'standard_volume_type': 20.0, 'performance_volume_type': 20.0, 'performance-20000_volume_type': 20.0}, 'disk': [10.0, 10.0, 10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume58(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 08:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-58")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 58,
            'instance_name': 'test_volume_58',
            'openstack_id': 'test-volume-id-58',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance', 'performance-20000', 'performance-20000', 'standard', 'performance'], 'volume_type': [0, 1, 2, 2, 0, 1], 'total_h_gb': {'standard_volume_type': 20.0, 'performance_volume_type': 20.0, 'performance-20000_volume_type': 20.0}, 'disk': [10.0, 10.0, 10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume59(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-59")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 59,
            'instance_name': 'test_volume_59',
            'openstack_id': 'test-volume-id-59',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance'], 'volume_type': [0, 1], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 10.0}, 'disk': [10.0, 10.0], 'size_time': [1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume60(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-60")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 60,
            'instance_name': 'test_volume_60',
            'openstack_id': 'test-volume-id-60',
            'events': [],
            'consumption': {'volume_type_name': ['standard'], 'volume_type': [0], 'total_h_gb': {'standard_volume_type': 10.0}, 'disk': [10.0], 'size_time': [1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume61(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-61")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 61,
            'instance_name': 'test_volume_61',
            'openstack_id': 'test-volume-id-61',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance', 'performance-20000', 'standard'], 'volume_type': [0, 1, 2, 0], 'total_h_gb': {'standard_volume_type': 20.0, 'performance_volume_type': 10.0, 'performance-20000_volume_type': 10.0}, 'disk': [10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume62(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-62")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 62,
            'instance_name': 'test_volume_62',
            'openstack_id': 'test-volume-id-62',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance', 'performance'], 'volume_type': [0, 1, 1], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 20.0}, 'disk': [10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume63(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-63")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 63,
            'instance_name': 'test_volume_63',
            'openstack_id': 'test-volume-id-63',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance', 'performance', 'performance-20000'], 'volume_type': [0, 1, 1, 2], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 20.0, 'performance-20000_volume_type': 10.0}, 'disk': [10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume64(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 08:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-64")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 64,
            'instance_name': 'test_volume_64',
            'openstack_id': 'test-volume-id-64',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance', 'performance-20000', 'performance-20000', 'standard', 'performance'], 'volume_type': [0, 1, 2, 2, 0, 1], 'total_h_gb': {'standard_volume_type': 20.0, 'performance_volume_type': 20.0, 'performance-20000_volume_type': 20.0}, 'disk': [10.0, 10.0, 10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume65(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-65")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 65,
            'instance_name': 'test_volume_65',
            'openstack_id': 'test-volume-id-65',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance', 'performance', 'performance'], 'volume_type': [0, 1, 1, 1], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 30.0}, 'disk': [10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume66(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-66")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 66,
            'instance_name': 'test_volume_66',
            'openstack_id': 'test-volume-id-66',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'standard', 'standard', 'performance'], 'volume_type': [0, 0, 0, 1], 'total_h_gb': {'standard_volume_type': 30.0, 'performance_volume_type': 10.0}, 'disk': [10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume67(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-67")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 67,
            'instance_name': 'test_volume_67',
            'openstack_id': 'test-volume-id-67',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'standard', 'standard'], 'volume_type': [0, 0, 0], 'total_h_gb': {'standard_volume_type': 30.0}, 'disk': [10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume68(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 09:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-68")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 68,
            'instance_name': 'test_volume_68',
            'openstack_id': 'test-volume-id-68',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance', 'performance-20000', 'performance-20000', 'standard', 'standard', 'performance'], 'volume_type': [0, 1, 2, 2, 0, 0, 1], 'total_h_gb': {'standard_volume_type': 30.0, 'performance_volume_type': 20.0, 'performance-20000_volume_type': 20.0}, 'disk': [10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume69(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-69")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 69,
            'instance_name': 'test_volume_69',
            'openstack_id': 'test-volume-id-69',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance', 'performance'], 'volume_type': [0, 1, 1], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 20.0}, 'disk': [10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume70(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-70")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 70,
            'instance_name': 'test_volume_70',
            'openstack_id': 'test-volume-id-70',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'standard', 'performance'], 'volume_type': [0, 0, 1], 'total_h_gb': {'standard_volume_type': 20.0, 'performance_volume_type': 10.0}, 'disk': [10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume71(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 09:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-71")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 71,
            'instance_name': 'test_volume_71',
            'openstack_id': 'test-volume-id-71',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance', 'performance-20000', 'performance-20000', 'standard', 'performance'], 'volume_type': [0, 1, 2, 2, 0, 1], 'total_h_gb': {'standard_volume_type': 20.0, 'performance_volume_type': 20.0, 'performance-20000_volume_type': 20.0}, 'disk': [10.0, 10.0, 10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume72(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-72")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 72,
            'instance_name': 'test_volume_72',
            'openstack_id': 'test-volume-id-72',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance', 'performance', 'performance'], 'volume_type': [0, 1, 1, 1], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 30.0}, 'disk': [10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume73(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-73")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 73,
            'instance_name': 'test_volume_73',
            'openstack_id': 'test-volume-id-73',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'standard', 'performance', 'performance'], 'volume_type': [0, 0, 1, 1], 'total_h_gb': {'standard_volume_type': 20.0, 'performance_volume_type': 20.0}, 'disk': [10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume74(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-74")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 74,
            'instance_name': 'test_volume_74',
            'openstack_id': 'test-volume-id-74',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'standard', 'standard', 'performance'], 'volume_type': [0, 0, 0, 1], 'total_h_gb': {'standard_volume_type': 30.0, 'performance_volume_type': 10.0}, 'disk': [10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume75(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 09:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-75")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 75,
            'instance_name': 'test_volume_75',
            'openstack_id': 'test-volume-id-75',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance', 'performance', 'performance-20000', 'performance-20000', 'standard'], 'volume_type': [0, 1, 1, 2, 2, 0], 'total_h_gb': {'standard_volume_type': 20.0, 'performance_volume_type': 20.0, 'performance-20000_volume_type': 20.0}, 'disk': [10.0, 10.0, 10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume76(self):
        start_time = datetime.datetime.strptime("2023-01-11 01:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 12:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-76")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 76,
            'instance_name': 'test_volume_76',
            'openstack_id': 'test-volume-id-76',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance', 'performance-20000', 'performance-20000', 'standard', 'performance', 'performance', 'performance-20000', 'standard'], 'volume_type': [0, 1, 2, 2, 0, 1, 1, 2, 0], 'total_h_gb': {'standard_volume_type': 30.0, 'performance_volume_type': 30.0, 'performance-20000_volume_type': 30.0}, 'disk': [10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume77(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-77")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 77,
            'instance_name': 'test_volume_77',
            'openstack_id': 'test-volume-id-77',
            'events': [],
            'consumption': {'volume_type_name': ['performance'], 'volume_type': [1], 'total_h_gb': {'performance_volume_type': 10.0}, 'disk': [10.0], 'size_time': [1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume78(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-78")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 78,
            'instance_name': 'test_volume_78',
            'openstack_id': 'test-volume-id-78',
            'events': [],
            'consumption': {'volume_type_name': ['performance'], 'volume_type': [1], 'total_h_gb': {'performance_volume_type': 10.0}, 'disk': [10.0], 'size_time': [1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume79(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-79")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 79,
            'instance_name': 'test_volume_79',
            'openstack_id': 'test-volume-id-79',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance'], 'volume_type': [0, 1], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 10.0}, 'disk': [10.0, 10.0], 'size_time': [1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume80(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-80")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 80,
            'instance_name': 'test_volume_80',
            'openstack_id': 'test-volume-id-80',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance', 'performance-20000'], 'volume_type': [0, 1, 2], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 10.0, 'performance-20000_volume_type': 10.0}, 'disk': [10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume81(self):
        start_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 08:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-81")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 81,
            'instance_name': 'test_volume_81',
            'openstack_id': 'test-volume-id-81',
            'events': [],
            'consumption': {'volume_type_name': ['performance-20000', 'standard', 'performance'], 'volume_type': [2, 0, 1], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 10.0, 'performance-20000_volume_type': 10.0}, 'disk': [10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume82(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-82")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 82,
            'instance_name': 'test_volume_82',
            'openstack_id': 'test-volume-id-82',
            'events': [],
            'consumption': {'volume_type_name': ['performance', 'performance'], 'volume_type': [1, 1], 'total_h_gb': {'performance_volume_type': 20.0}, 'disk': [10.0, 10.0], 'size_time': [1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume83(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-83")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 83,
            'instance_name': 'test_volume_83',
            'openstack_id': 'test-volume-id-83',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'standard', 'performance'], 'volume_type': [0, 0, 1], 'total_h_gb': {'standard_volume_type': 20.0, 'performance_volume_type': 10.0}, 'disk': [10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume84(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-84")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 84,
            'instance_name': 'test_volume_84',
            'openstack_id': 'test-volume-id-84',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance', 'performance'], 'volume_type': [0, 1, 1], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 20.0}, 'disk': [10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume85(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-85")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 85,
            'instance_name': 'test_volume_85',
            'openstack_id': 'test-volume-id-85',
            'events': [],
            'consumption': {'volume_type_name': ['performance', 'performance-20000', 'performance-20000', 'standard'], 'volume_type': [1, 2, 2, 0], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 10.0, 'performance-20000_volume_type': 20.0}, 'disk': [10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume86(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 10:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-86")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 86,
            'instance_name': 'test_volume_86',
            'openstack_id': 'test-volume-id-86',
            'events': [],
            'consumption': {'volume_type_name': ['performance-20000', 'standard', 'performance', 'performance', 'performance-20000', 'standard'], 'volume_type': [2, 0, 1, 1, 2, 0], 'total_h_gb': {'standard_volume_type': 20.0, 'performance_volume_type': 20.0, 'performance-20000_volume_type': 20.0}, 'disk': [10.0, 10.0, 10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume87(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-87")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 87,
            'instance_name': 'test_volume_87',
            'openstack_id': 'test-volume-id-87',
            'events': [],
            'consumption': {'volume_type_name': ['performance'], 'volume_type': [1], 'total_h_gb': {'performance_volume_type': 10.0}, 'disk': [10.0], 'size_time': [1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume88(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-88")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 88,
            'instance_name': 'test_volume_88',
            'openstack_id': 'test-volume-id-88',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance'], 'volume_type': [0, 1], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 10.0}, 'disk': [10.0, 10.0], 'size_time': [1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume89(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-89")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 89,
            'instance_name': 'test_volume_89',
            'openstack_id': 'test-volume-id-89',
            'events': [],
            'consumption': {'volume_type_name': ['standard'], 'volume_type': [0], 'total_h_gb': {'standard_volume_type': 10.0}, 'disk': [10.0], 'size_time': [1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume90(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-90")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 90,
            'instance_name': 'test_volume_90',
            'openstack_id': 'test-volume-id-90',
            'events': [],
            'consumption': {'volume_type_name': ['performance-20000', 'standard', 'performance'], 'volume_type': [2, 0, 1], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 10.0, 'performance-20000_volume_type': 10.0}, 'disk': [10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume91(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-91")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 91,
            'instance_name': 'test_volume_91',
            'openstack_id': 'test-volume-id-91',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'standard'], 'volume_type': [0, 0], 'total_h_gb': {'standard_volume_type': 20.0}, 'disk': [10.0, 10.0], 'size_time': [1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume92(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-92")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 92,
            'instance_name': 'test_volume_92',
            'openstack_id': 'test-volume-id-92',
            'events': [],
            'consumption': {'volume_type_name': ['performance', 'performance'], 'volume_type': [1, 1], 'total_h_gb': {'performance_volume_type': 20.0}, 'disk': [10.0, 10.0], 'size_time': [1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume93(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-93")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 93,
            'instance_name': 'test_volume_93',
            'openstack_id': 'test-volume-id-93',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance', 'performance'], 'volume_type': [0, 1, 1], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 20.0}, 'disk': [10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume94(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-94")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 94,
            'instance_name': 'test_volume_94',
            'openstack_id': 'test-volume-id-94',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance', 'performance', 'performance-20000'], 'volume_type': [0, 1, 1, 2], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 20.0, 'performance-20000_volume_type': 10.0}, 'disk': [10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume95(self):
        start_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 13:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-95")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 95,
            'instance_name': 'test_volume_95',
            'openstack_id': 'test-volume-id-95',
            'events': [],
            'consumption': {'volume_type_name': ['performance', 'performance-20000', 'standard', 'standard', 'performance', 'performance-20000'], 'volume_type': [1, 2, 0, 0, 1, 2], 'total_h_gb': {'standard_volume_type': 20.0, 'performance_volume_type': 20.0, 'performance-20000_volume_type': 20.0}, 'disk': [10.0, 10.0, 10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume96(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-96")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 96,
            'instance_name': 'test_volume_96',
            'openstack_id': 'test-volume-id-96',
            'events': [],
            'consumption': {'volume_type_name': ['performance', 'performance'], 'volume_type': [1, 1], 'total_h_gb': {'performance_volume_type': 20.0}, 'disk': [10.0, 10.0], 'size_time': [1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume97(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-97")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 97,
            'instance_name': 'test_volume_97',
            'openstack_id': 'test-volume-id-97',
            'events': [],
            'consumption': {'volume_type_name': ['performance', 'performance'], 'volume_type': [1, 1], 'total_h_gb': {'performance_volume_type': 20.0}, 'disk': [10.0, 10.0], 'size_time': [1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume98(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-98")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 98,
            'instance_name': 'test_volume_98',
            'openstack_id': 'test-volume-id-98',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance', 'performance'], 'volume_type': [0, 1, 1], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 20.0}, 'disk': [10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume99(self):
        start_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 13:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-99")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 99,
            'instance_name': 'test_volume_99',
            'openstack_id': 'test-volume-id-99',
            'events': [],
            'consumption': {'volume_type_name': ['performance', 'performance-20000', 'standard', 'standard', 'performance', 'performance-20000'], 'volume_type': [1, 2, 0, 0, 1, 2], 'total_h_gb': {'standard_volume_type': 20.0, 'performance_volume_type': 20.0, 'performance-20000_volume_type': 20.0}, 'disk': [10.0, 10.0, 10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume100(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-100")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 100,
            'instance_name': 'test_volume_100',
            'openstack_id': 'test-volume-id-100',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'standard', 'performance', 'performance-20000'], 'volume_type': [0, 0, 1, 2], 'total_h_gb': {'standard_volume_type': 20.0, 'performance_volume_type': 10.0, 'performance-20000_volume_type': 10.0}, 'disk': [10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume101(self):
        start_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 09:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-101")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 101,
            'instance_name': 'test_volume_101',
            'openstack_id': 'test-volume-id-101',
            'events': [],
            'consumption': {'volume_type_name': ['performance', 'performance', 'performance-20000', 'performance-20000'], 'volume_type': [1, 1, 2, 2], 'total_h_gb': {'performance_volume_type': 20.0, 'performance-20000_volume_type': 20.0}, 'disk': [10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume102(self):
        start_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 11:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-102")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 102,
            'instance_name': 'test_volume_102',
            'openstack_id': 'test-volume-id-102',
            'events': [],
            'consumption': {'volume_type_name': ['performance-20000', 'standard', 'standard', 'standard', 'performance'], 'volume_type': [2, 0, 0, 0, 1], 'total_h_gb': {'standard_volume_type': 30.0, 'performance_volume_type': 10.0, 'performance-20000_volume_type': 10.0}, 'disk': [10.0, 10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume103(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-103")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 103,
            'instance_name': 'test_volume_103',
            'openstack_id': 'test-volume-id-103',
            'events': [],
            'consumption': {'volume_type_name': ['performance'], 'volume_type': [1], 'total_h_gb': {'performance_volume_type': 10.0}, 'disk': [10.0], 'size_time': [1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume104(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-104")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 104,
            'instance_name': 'test_volume_104',
            'openstack_id': 'test-volume-id-104',
            'events': [],
            'consumption': {'volume_type_name': ['performance'], 'volume_type': [1], 'total_h_gb': {'performance_volume_type': 10.0}, 'disk': [10.0], 'size_time': [1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume105(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-105")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 105,
            'instance_name': 'test_volume_105',
            'openstack_id': 'test-volume-id-105',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance'], 'volume_type': [0, 1], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 10.0}, 'disk': [10.0, 10.0], 'size_time': [1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume106(self):
        start_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 09:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-106")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 106,
            'instance_name': 'test_volume_106',
            'openstack_id': 'test-volume-id-106',
            'events': [],
            'consumption': {'volume_type_name': ['performance-20000', 'standard', 'performance'], 'volume_type': [2, 0, 1], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 10.0, 'performance-20000_volume_type': 10.0}, 'disk': [10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume107(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 08:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-107")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 107,
            'instance_name': 'test_volume_107',
            'openstack_id': 'test-volume-id-107',
            'events': [],
            'consumption': {'volume_type_name': ['performance', 'performance-20000', 'standard'], 'volume_type': [1, 2, 0], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 10.0, 'performance-20000_volume_type': 10.0}, 'disk': [10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume108(self):
        start_time = datetime.datetime.strptime("2023-01-11 02:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-108")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 108,
            'instance_name': 'test_volume_108',
            'openstack_id': 'test-volume-id-108',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance', 'performance', 'performance-20000'], 'volume_type': [0, 1, 1, 2], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 20.0, 'performance-20000_volume_type': 10.0}, 'disk': [10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume109(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 08:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-109")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 109,
            'instance_name': 'test_volume_109',
            'openstack_id': 'test-volume-id-109',
            'events': [],
            'consumption': {'volume_type_name': ['performance', 'performance-20000', 'performance-20000', 'standard'], 'volume_type': [1, 2, 2, 0], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 10.0, 'performance-20000_volume_type': 20.0}, 'disk': [10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume110(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 11:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-110")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 110,
            'instance_name': 'test_volume_110',
            'openstack_id': 'test-volume-id-110',
            'events': [],
            'consumption': {'volume_type_name': ['performance-20000', 'standard', 'performance', 'performance', 'performance-20000', 'standard'], 'volume_type': [2, 0, 1, 1, 2, 0], 'total_h_gb': {'standard_volume_type': 20.0, 'performance_volume_type': 20.0, 'performance-20000_volume_type': 20.0}, 'disk': [10.0, 10.0, 10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume111(self):
        start_time = datetime.datetime.strptime("2023-01-11 03:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-111")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 111,
            'instance_name': 'test_volume_111',
            'openstack_id': 'test-volume-id-111',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance', 'performance'], 'volume_type': [0, 1, 1], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 20.0}, 'disk': [10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume112(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-112")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 112,
            'instance_name': 'test_volume_112',
            'openstack_id': 'test-volume-id-112',
            'events': [],
            'consumption': {'volume_type_name': ['performance', 'performance'], 'volume_type': [1, 1], 'total_h_gb': {'performance_volume_type': 20.0}, 'disk': [10.0, 10.0], 'size_time': [1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume113(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 09:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-113")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 113,
            'instance_name': 'test_volume_113',
            'openstack_id': 'test-volume-id-113',
            'events': [],
            'consumption': {'volume_type_name': ['performance', 'performance-20000', 'performance-20000', 'standard'], 'volume_type': [1, 2, 2, 0], 'total_h_gb': {'standard_volume_type': 10.0, 'performance_volume_type': 10.0, 'performance-20000_volume_type': 20.0}, 'disk': [10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume114(self):
        start_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 14:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-114")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 114,
            'instance_name': 'test_volume_114',
            'openstack_id': 'test-volume-id-114',
            'events': [],
            'consumption': {'volume_type_name': ['performance', 'performance-20000', 'standard', 'standard', 'performance', 'performance-20000'], 'volume_type': [1, 2, 0, 0, 1, 2], 'total_h_gb': {'standard_volume_type': 20.0, 'performance_volume_type': 20.0, 'performance-20000_volume_type': 20.0}, 'disk': [10.0, 10.0, 10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume115(self):
        start_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 09:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-115")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 115,
            'instance_name': 'test_volume_115',
            'openstack_id': 'test-volume-id-115',
            'events': [],
            'consumption': {'volume_type_name': ['performance', 'performance', 'performance'], 'volume_type': [1, 1, 1], 'total_h_gb': {'performance_volume_type': 30.0}, 'disk': [10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume116(self):
        start_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 14:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-116")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 116,
            'instance_name': 'test_volume_116',
            'openstack_id': 'test-volume-id-116',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'performance', 'performance', 'performance-20000', 'performance-20000', 'standard'], 'volume_type': [0, 1, 1, 2, 2, 0], 'total_h_gb': {'standard_volume_type': 20.0, 'performance_volume_type': 20.0, 'performance-20000_volume_type': 20.0}, 'disk': [10.0, 10.0, 10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume117(self):
        start_time = datetime.datetime.strptime("2023-01-11 04:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 11:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-117")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 117,
            'instance_name': 'test_volume_117',
            'openstack_id': 'test-volume-id-117',
            'events': [],
            'consumption': {'volume_type_name': ['standard', 'standard', 'performance', 'performance-20000', 'standard', 'standard'], 'volume_type': [0, 0, 1, 2, 0, 0], 'total_h_gb': {'standard_volume_type': 40.0, 'performance_volume_type': 10.0, 'performance-20000_volume_type': 10.0}, 'disk': [10.0, 10.0, 10.0, 10.0, 10.0, 10.0], 'size_time': [1.0, 1.0, 1.0, 1.0, 1.0, 1.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume118(self):
        start_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-118")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 118,
            'instance_name': 'test_volume_118',
            'openstack_id': 'test-volume-id-118',
            'events': [],
            'consumption': {'volume_type_name': ['performance'], 'volume_type': [1], 'total_h_gb': {}, 'disk': [10.0], 'size_time': [0.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume119(self):
        start_time = datetime.datetime.strptime("2023-01-11 05:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-119")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 119,
            'instance_name': 'test_volume_119',
            'openstack_id': 'test-volume-id-119',
            'events': [],
            'consumption': {'volume_type_name': ['performance'], 'volume_type': [1], 'total_h_gb': {}, 'disk': [10.0], 'size_time': [0.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume120(self):
        start_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-120")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 120,
            'instance_name': 'test_volume_120',
            'openstack_id': 'test-volume-id-120',
            'events': [],
            'consumption': {'volume_type_name': ['performance-20000'], 'volume_type': [2], 'total_h_gb': {}, 'disk': [10.0], 'size_time': [0.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume121(self):
        start_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 08:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-121")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 121,
            'instance_name': 'test_volume_121',
            'openstack_id': 'test-volume-id-121',
            'events': [],
            'consumption': {'volume_type_name': ['standard'], 'volume_type': [0], 'total_h_gb': {}, 'disk': [10.0], 'size_time': [0.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume122(self):
        start_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-122")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 122,
            'instance_name': 'test_volume_122',
            'openstack_id': 'test-volume-id-122',
            'events': [],
            'consumption': {'volume_type_name': ['performance'], 'volume_type': [1], 'total_h_gb': {}, 'disk': [10.0], 'size_time': [0.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume123(self):
        start_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-123")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 123,
            'instance_name': 'test_volume_123',
            'openstack_id': 'test-volume-id-123',
            'events': [],
            'consumption': {'volume_type_name': ['performance'], 'volume_type': [1], 'total_h_gb': {}, 'disk': [10.0], 'size_time': [0.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume124(self):
        start_time = datetime.datetime.strptime("2023-01-11 06:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 07:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-124")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 124,
            'instance_name': 'test_volume_124',
            'openstack_id': 'test-volume-id-124',
            'events': [],
            'consumption': {'volume_type_name': ['performance'], 'volume_type': [1], 'total_h_gb': {}, 'disk': [10.0], 'size_time': [0.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         

    def test_calc_volume125(self):
        start_time = datetime.datetime.strptime("2023-01-11 11:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        finish_time = datetime.datetime.strptime("2023-01-11 12:00:00", '%Y-%m-%d %H:%M:%S').replace(tzinfo=pytz.utc)
        projects = Project.objects.filter(id=1)
        bill_run = VolumeBiller(start_time, finish_time, projects, "test-volume-id-125")
        bill_cons = bill_run.instance_calc()
        bill_cons[0]['events']=[]
        test_controller = {            
            'instance_id': 125,
            'instance_name': 'test_volume_125',
            'openstack_id': 'test-volume-id-125',
            'events': [],
            'consumption': {'volume_type_name': ['standard'], 'volume_type': [0], 'total_h_gb': {}, 'disk': [10.0], 'size_time': [0.0]},     
            'attached_to': '',                   
            'project': 1,
            'project_name': 'test-project',
            'project_description': 'test project - terp 20986.91.04',
            'project_openstack_id': 'd670223972c2479ebcc5c7924e9f4a2e',
            'billing_code' : '20986.91.04'     
            }
        self.assertEqual(bill_cons[0], test_controller)
         