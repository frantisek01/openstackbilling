#!/usr/bin/env python3
import time
import socket
import os


def wait_for_port(port=int(os.environ['SQL_PORT']), host=os.environ['SQL_HOST'], timeout=15.0):
    """Wait until a port starts accepting TCP connections.
    Args:
        port (int): Port number.
        host (str): Host address on which the port should exist.
        timeout (float): In seconds. How long to wait before raising errors.
    Raises:
        TimeoutError: The port isn't accepting connection after time specified in `timeout`.
    """
    start_time = time.perf_counter()
    while True:
        try:
            with socket.create_connection((host, port), timeout=timeout):
                break
        except OSError as ex:
            time.sleep(0.01)
            if time.perf_counter() - start_time >= timeout:
                raise TimeoutError('Waited too long for the port {} on host {} to start accepting '
                                   'connections.'.format(port, host)) from ex

if __name__ == "__main__":
    wait_for_port()
    # os.system('sleep 15s')
    os.system('pipenv shell')
    os.system('pipenv run python3 manage.py makemigrations')
    os.system('pipenv run python3 manage.py migrate')  
    os.system('pipenv run python3 manage.py collectstatic --noinput')  
    os.system('pipenv run python3 manage.py runserver 0.0.0.0:8000')
    os.system('tail -f /dev/null')