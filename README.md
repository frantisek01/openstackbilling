# OpenStack Billing

Billing services for OpenStack.  

## Description

App periodically loads billing data via OpenStack API, calculates usage in hours and gigabyte hours.

* Images, volumes, servers
* Billing codes
* Dedicated hosts
* Volume types
* Local disks
* Project sum
* Billing code sum
* OS licenses

## Getting Started

### Dependencies

Runs with postgres db.


## Authors

František Doležel
f.dolezel01@gmail.com

