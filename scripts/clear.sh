#!/usr/bin/env bash

docker compose -f /srv/docker-compose.yml down
docker rm -f 639444363233.dkr.ecr.eu-west-1.amazonaws.com/openstackbilling:db || true
docker rm -f 639444363233.dkr.ecr.eu-west-1.amazonaws.com/openstackbilling:app || true