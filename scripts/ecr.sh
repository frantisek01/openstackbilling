#!/bin/bash

aws ecr get-login-password --region eu-west-1 | docker login --username AWS --password-stdin 639444363233.dkr.ecr.eu-west-1.amazonaws.com

docker pull 639444363233.dkr.ecr.eu-west-1.amazonaws.com/openstackbilling:db

docker pull 639444363233.dkr.ecr.eu-west-1.amazonaws.com/openstackbilling:app